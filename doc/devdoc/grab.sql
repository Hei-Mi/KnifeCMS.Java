--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.3
-- Dumped by pg_dump version 9.1.3
-- Started on 2012-10-09 11:15:01

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 215 (class 1259 OID 17935)
-- Dependencies: 2018 2019 2020 2021 5
-- Name: k_grab; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE k_grab (
    id character varying(32) DEFAULT ''::character varying NOT NULL,
    k_title character varying(200) DEFAULT NULL::character varying,
    k_url character varying(255) DEFAULT NULL::character varying,
    k_date timestamp without time zone DEFAULT now(),
    k_listdna_head text,
    k_content_dna text,
    k_listdna_foot text,
    k_url2 character varying,
    k_pagedna_head text,
    k_pagedna_foot text
);


ALTER TABLE public.k_grab OWNER TO postgres;

--
-- TOC entry 2023 (class 2606 OID 17943)
-- Dependencies: 215 215
-- Name: k_grab_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY k_grab
    ADD CONSTRAINT k_grab_pkey PRIMARY KEY (id);


-- Completed on 2012-10-09 11:15:02

--
-- PostgreSQL database dump complete
--

