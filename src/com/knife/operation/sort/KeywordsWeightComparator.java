package com.knife.operation.sort;

import java.util.Comparator;

import com.knife.news.object.Keywords;

public class KeywordsWeightComparator implements Comparator<Keywords> {
	public int compare(Keywords firstKey, Keywords secondKey) {
		int firstWeight = firstKey.getWeight();
		int secondWeight = secondKey.getWeight();
		return signum(firstWeight - secondWeight);
	}

	public int signum(int diff) {
		if (diff > 0)
			return 1;
		if (diff < 0)
			return -1;
		else
			return 0;
	}
}
