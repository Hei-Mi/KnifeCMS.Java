package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.Flow;

/**
 * 工作流接口
 * @author Knife
 *
 */
public interface FlowService {
	
	/**
	 * 保存日志
	 * @param flow
	 * @return true保存成功,false保存失败
	 */
	boolean saveFlow(Flow flow);
	
	/**
	 * 更新日志
	 * @param flow
	 * @return true更新成功,false更新失败
	 */
	boolean updateFlow(Flow flow);
	
	/**
	 * 按编号取得日志
	 * @param id 编号
	 * @return 日志对象
	 */
	Flow getFlowById(String id);
	
	/**
	 * 删除日志
	 * @param flow
	 * @return 日志对象
	 */
	boolean delFlow(Flow flow);
	
	/**
	 * 取得所有日志
	 * @return 日志列表
	 */
	List<Flow> getAllFlow();
	
	/**
	 * 按查询条件取得日志
	 * @param sql 查询语句
	 * @param paras 查询参数
	 * @param begin 起始地址
	 * @param end 结束地址
	 * @return 日志列表
	 */
	List<Flow> getFlowBySql(String sql, Collection<Object> paras, int begin, int end);
}
