package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.ThemeService;
import com.knife.news.object.Theme;

public class ThemeServiceImpl extends DAOSupportService implements ThemeService {
	private static ThemeServiceImpl themeDao = new ThemeServiceImpl();

	public static ThemeServiceImpl getInstance() {
		return themeDao;
	}

	public boolean saveTheme(Theme theme) {
		com.knife.news.model.Theme atheme=new com.knife.news.model.Theme();
		atheme.setTheme(theme);
		return this.dao.save(atheme);
	}
	
	public boolean updateTheme(Theme theme) {
		com.knife.news.model.Theme atheme=new com.knife.news.model.Theme();
		atheme.setTheme(theme);
		return this.dao.update(atheme);
	}
	
	public Theme getThemeById(String id) {
		try{
			com.knife.news.model.Theme atheme = (com.knife.news.model.Theme) this.dao.get(com.knife.news.model.Theme.class, id);
			Theme theme = new Theme(atheme);
			return theme;
		}catch(Exception e){
			return null;
		}
	}
	
	public boolean delTheme(Theme theme) {
		com.knife.news.model.Theme atheme=new com.knife.news.model.Theme();
		atheme.setTheme(theme);
		return dao.del(atheme);
	}
	
	public List<Theme> getAllTheme() {
		return changeListType(this.dao.query(com.knife.news.model.Theme.class, "id!=''"));
		//return this.dao.query(News.class, "k_type=?", paras);
	}
	
	public List<Theme> getThemeBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListType(this.dao.query(com.knife.news.model.Theme.class, sql, paras, begin, end));
	}
	
	public List<Theme> changeListType(List<Object> objs){
		List<Theme> results = new ArrayList<Theme>();
		if(objs!=null){
			for (Object newobj : objs) {
				com.knife.news.model.Theme robj = (com.knife.news.model.Theme) newobj;
				Theme tobj =  new Theme(robj);
				results.add(tobj);
			}
		}
		return results;	
	}
}
