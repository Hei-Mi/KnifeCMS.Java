package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.NewsVersionService;
import com.knife.news.object.Flow;
import com.knife.news.object.NewsVersion;

public class NewsVersionServiceImpl extends DAOSupportService implements NewsVersionService{

	private static NewsVersionServiceImpl newsversionDao=new NewsVersionServiceImpl();
	
	public static NewsVersionServiceImpl getInstance(){
		return newsversionDao;
	}
	
	@Override
	public String addNewsVersion(NewsVersion newsv) {
		// TODO Auto-generated method stub
		com.knife.news.model.NewsVersion anewsv = new com.knife.news.model.NewsVersion();
		newsv.setId(anewsv.getId());
		anewsv.setNewsVersion(newsv);
		if(dao.save(anewsv)){
			return anewsv.getId();
		}else{
			return "";
		}
	}

	@Override
	public boolean saveNewsVersion(NewsVersion newsv) {
		// TODO Auto-generated method stub
		com.knife.news.model.NewsVersion anewsv = new com.knife.news.model.NewsVersion();
		anewsv.setNewsVersion(newsv);
		return dao.save(anewsv);
	}

	@Override
	public boolean updateNewsVersion(NewsVersion newsv) {
		// TODO Auto-generated method stub
		com.knife.news.model.NewsVersion anewsv = new com.knife.news.model.NewsVersion();
		anewsv.setNewsVersion(newsv);
		return dao.update(anewsv);
	}

	@Override
	public boolean delNewsVersion(NewsVersion newsv) {
		// TODO Auto-generated method stub
		com.knife.news.model.NewsVersion anewsv = new com.knife.news.model.NewsVersion();
		anewsv.setNewsVersion(newsv);
		return dao.del(anewsv);
	}

	@Override
	public List<NewsVersion> getNewsVersionByNewsid(String newsid) {
		// TODO Auto-generated method stub
		try{
			Collection<Object> paras = new ArrayList<Object>();
			if(newsid.length()>0){
				paras.add(newsid);
				return	this.getNewsVersionBySql("k_newsid=? order by k_versionnumber asc",paras,0,-1);
			}
			return null;
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public NewsVersion getNewsVersionById(String id) {
		// TODO Auto-generated method stub
		try{
			com.knife.news.model.NewsVersion anewsv = (com.knife.news.model.NewsVersion) this.dao.get(com.knife.news.model.NewsVersion.class, id);
			NewsVersion newsv = new NewsVersion(anewsv);
			return newsv;
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public List<NewsVersion> getNewsVersionBySql(String sql,
			Collection<Object> paras, int begin, int end) {
		// TODO Auto-generated method stub
		return changeListType(this.dao.query(com.knife.news.model.NewsVersion.class, sql, paras, begin, end));
	}
	
	public List<NewsVersion> changeListType(List<Object> objs){
		List<NewsVersion> results = new ArrayList<NewsVersion>();
		if(objs!=null){
			for (Object newobj : objs) {
				com.knife.news.model.NewsVersion robj = (com.knife.news.model.NewsVersion) newobj;
				NewsVersion tobj = new NewsVersion(robj);
				results.add(tobj);
			}
		}
		return results;	
	}
}
