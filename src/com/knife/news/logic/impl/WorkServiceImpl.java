package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.NewsService;
import com.knife.news.logic.PicService;
import com.knife.news.logic.PicTypeService;
import com.knife.news.logic.VideoService;
import com.knife.news.logic.WorkService;
import com.knife.news.object.News;
import com.knife.news.object.Pic;
import com.knife.news.object.PicType;
import com.knife.news.object.Video;
import com.knife.news.object.Work;

public class WorkServiceImpl extends DAOSupportService implements WorkService {
	private static WorkService workDao = new WorkServiceImpl();
	private NewsService newsDAO=NewsServiceImpl.getInstance();
	private PicService picDAO=PicServiceImpl.getInstance();
	private PicTypeService ptDAO=PicTypeServiceImpl.getInstance();
	private VideoService videoDAO=VideoServiceImpl.getInstance();

	public static WorkService getInstance() {
		return workDao;
	}
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.WorkService#getWorkById(java.lang.String)
	 */
	//@Override
	public Work getWorkById(String id,int tid) {
		try{
			Work work = new Work();
			switch(tid){
			case 1:
				News anews=newsDAO.getNewsById(id);
				work=news2work(anews);
				break;
			case 2:
				Pic apic=picDAO.getPicById(id);
				work=pic2work(apic);
				break;
			case 3:
				Video avideo=videoDAO.getVideoById(id);
				work=video2work(avideo);
				break;
			case 4:
				PicType aptype=ptDAO.getPicTypeById(id);
				work=pictype2work(aptype);
				break;
			}
			return work;
		}catch(Exception e){
			return null;
		}
	}
	
	public void auditWork(String id,int typeid){
		switch(typeid){
		case 1:
			News anews=newsDAO.getNewsById(id);
			anews.setDisplay(1);
			newsDAO.updateNews(anews);
			break;
		case 2:
			Pic apic=picDAO.getPicById(id);
			apic.setDisplay(1);
			picDAO.updatePic(apic);
			break;
		case 3:
			Video avideo=videoDAO.getVideoById(id);
			avideo.setDisplay(1);
			videoDAO.updateVideo(avideo);
			break;
		}
	}
	
	public void deleteWork(String id,int typeid){
		switch(typeid){
		case 1:
			News anews=newsDAO.getNewsById(id);
			newsDAO.delNews(anews);
			break;
		case 2:
			Pic apic=picDAO.getPicById(id);
			picDAO.delPic(apic);
			break;
		case 3:
			Video avideo=videoDAO.getVideoById(id);
			videoDAO.delVideo(avideo);
			break;
		}
	}
	
	public List<Work> getWorksByType(int typeid){
		List<Work> todolist=new ArrayList<Work>();
		switch(typeid){
		case 1:
			List<News> todoNews=newsDAO.getNews("k_display='0'");
			todolist.addAll(newsToWork(todoNews));
			break;
		case 2:
			List<Pic> todoPics=picDAO.getPicsBySql("k_display='0' and k_userid>0");
			todolist.addAll(picToWork(todoPics));
			break;
		case 3:
			List<Video> todoVideo=videoDAO.getVideosBySql("k_display='0' and k_userid>0");
			todolist.addAll(videoToWork(todoVideo));
			break;
		case 4:
			List<PicType> todoPicType=ptDAO.getPicTypesBySql("k_display='0' and k_userid>0");
			todolist.addAll(pictypeToWork(todoPicType));
			break;
		}
		
		return todolist;
	}
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.WorkService#getAllWork()
	 */
	//@Override
	public List<Work> getAllWorks() {
		List<Work> todolist=new ArrayList<Work>();
		List<News> todoNews=newsDAO.getNews("k_display='0'");
		todolist.addAll(newsToWork(todoNews));
		List<Pic> todoPics=picDAO.getPicsBySql("k_display='0' and k_userid>0");
		todolist.addAll(picToWork(todoPics));
		List<Video> todoVideo=videoDAO.getVideosBySql("k_display='0' and k_userid>0");
		todolist.addAll(videoToWork(todoVideo));
		List<PicType> todoPicType=ptDAO.getPicTypesBySql("k_display='0' and k_userid>0");
		todolist.addAll(pictypeToWork(todoPicType));
		return todolist;
	}
	
	//将文章转换为待办事项
	private List<Work> newsToWork(List<News> list){
		List<Work> worklist=new ArrayList<Work>();
		for(News anews:list){
			worklist.add(news2work(anews));
		}
		return worklist;
	}
	
	private Work news2work(News anews){
		Work awork=new Work();
		awork.setId(anews.getId());
		awork.setName(anews.getTitle());
		awork.setAuthor(anews.getAuthor());
		awork.setDate(anews.getDate());
		awork.setUrl("/manage_news.ad?parameter=edit&id="+anews.getId());
		return awork;
	}
	
	//将图片转换为待办事项
	private List<Work> picToWork(List<Pic> list){
		List<Work> worklist=new ArrayList<Work>();
		for(Pic apic:list){
			worklist.add(pic2work(apic));
		}
		return worklist;
	}
	
	private Work pic2work(Pic apic){
		Work awork=new Work();
		awork.setId(apic.getId());
		awork.setName(apic.getName());
		awork.setAuthor(apic.getAuthor());
		awork.setDate(apic.getDate());
		awork.setUrl("/manage_pic.ad?parameter=preview&id="+apic.getId());
		awork.setPath(apic.getUrl());
		return awork;
	}
	
	//将视频转换为待办事项
	private List<Work> videoToWork(List<Video> list){
		List<Work> worklist=new ArrayList<Work>();
		for(Video avideo:list){
			worklist.add(video2work(avideo));
		}
		return worklist;
	}
	
	private Work video2work(Video avideo){
		Work awork=new Work();
		awork.setId(avideo.getId());
		awork.setName(avideo.getName());
		awork.setAuthor(avideo.getAuthor());
		awork.setDate(avideo.getDate());
		awork.setUrl("/manage_video.ad?parameter=preview&id="+avideo.getId());
		awork.setPath(avideo.getUrl());
		return awork;
	}
	
	//将视频转换为待办事项
	private List<Work> pictypeToWork(List<PicType> list){
			List<Work> worklist=new ArrayList<Work>();
			for(PicType aptype:list){
				worklist.add(pictype2work(aptype));
			}
			return worklist;
	}
	
	private Work pictype2work(PicType aptype){
		Work awork=new Work();
		awork.setId(aptype.getId());
		awork.setName(aptype.getName());
		awork.setAuthor(aptype.getAuthor());
		awork.setDate(aptype.getDate());
		awork.setUrl("/manage_pic_type.ad?parameter=edit&id="+aptype.getId());
		return awork;
	}
}
