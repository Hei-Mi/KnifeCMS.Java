package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.BillService;
import com.knife.news.object.Bill;

public class BillServiceImpl extends DAOSupportService implements BillService {
	private static BillServiceImpl billDAO = new BillServiceImpl();

	public static BillServiceImpl getInstance() {
		return billDAO;
	}

	public List<Bill> getAllBill(){
		return changeListType(this.dao.query(com.knife.news.model.Bill.class, "id!=''"));
	}
	
	public List<Bill> getBillBySuser(String suser){
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(suser);
		return changeListType(this.dao.query(com.knife.news.model.Bill.class, "k_suser=? order by length(k_order) desc,k_order desc",paras));
	}

	public List<Bill> getBillBySql(String sql) {
		return changeListType(this.dao.query(com.knife.news.model.Bill.class, sql));
	}

	public List<Bill> getBillBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListType(this.dao.query(com.knife.news.model.Bill.class, sql, paras, begin, end));
	}

	public Bill getBillById(String id) {
		com.knife.news.model.Bill abill=(com.knife.news.model.Bill)this.dao.get(com.knife.news.model.Bill.class, id);
		return new Bill(abill);
	}
	
	public List<Bill> getBillByUrl(String url){
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(url);
		return changeListType(this.dao.query(com.knife.news.model.Bill.class, "k_url=?",paras));
	}

	public boolean saveBill(Bill bill) {
		com.knife.news.model.Bill bbill=new com.knife.news.model.Bill();
		bill.setId(bbill.getId());
		bbill.setBill(bill);
		return this.dao.save(bbill);
	}

	public boolean updateBill(Bill bill) {
		com.knife.news.model.Bill bbill=new com.knife.news.model.Bill();
		bbill.setBill(bill);
		return this.dao.update(bbill);
	}

	public boolean delBill(Bill bill) {
		com.knife.news.model.Bill bbill=new com.knife.news.model.Bill();
		bbill.setBill(bill);
		return this.dao.del(bbill);
	}
	
	public List<com.knife.news.object.Bill> changeListType(List<Object> news){
		List<Bill> results = new ArrayList<Bill>();
		if(news!=null){
			for (Object newobj : news) {
				com.knife.news.model.Bill bnews = (com.knife.news.model.Bill) newobj;
				Bill abill=new Bill(bnews);
				results.add(abill);
			}
		}
		return results;	
	}
}