package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.ThumbnailService;
import com.knife.news.model.Thumbnail;

public class ThumbServiceImpl extends DAOSupportService implements ThumbnailService  {

	private static ThumbServiceImpl userDAO = new ThumbServiceImpl();

	public static ThumbServiceImpl getInstance() {
		return userDAO;
	}

	public boolean deleteThumb(Thumbnail user) {
		// TODO Auto-generated method stub
		return false;
	}

	public Thumbnail getThumbById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public Thumbnail getThumbByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Thumbnail> getThumbsBySql(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Thumbnail> getThumbsBySql(String sql, Collection<Object> paras, int begin, int end) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Thumbnail> queryUserThumb() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean saveThumb(Thumbnail thumb) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean updateThumb(Thumbnail user) {
		// TODO Auto-generated method stub
		return false;
	}

	public List<Thumbnail> changeListType(List<Object> news){
		List<Thumbnail> results = new ArrayList<Thumbnail>();
		if(news!=null){
			for (Object newobj : news) {
				Thumbnail bnews = (Thumbnail) newobj;
				results.add(bnews);
			}
		}
		return results;	
	}
}
