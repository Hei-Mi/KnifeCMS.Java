package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.Theme;

/**
 * 日志接口
 * @author Knife
 *
 */
public interface ThemeService {
	
	/**
	 * 保存日志
	 * @param theme
	 * @return true保存成功,false保存失败
	 */
	boolean saveTheme(Theme theme);
	
	/**
	 * 更新日志
	 * @param theme
	 * @return true更新成功,false更新失败
	 */
	boolean updateTheme(Theme theme);
	
	/**
	 * 按编号取得日志
	 * @param id 编号
	 * @return 日志对象
	 */
	Theme getThemeById(String id);
	
	/**
	 * 删除日志
	 * @param theme
	 * @return 日志对象
	 */
	boolean delTheme(Theme theme);
	
	/**
	 * 取得所有日志
	 * @return 日志列表
	 */
	List<Theme> getAllTheme();
	
	/**
	 * 按查询条件取得日志
	 * @param sql 查询语句
	 * @param paras 查询参数
	 * @param begin 起始地址
	 * @param end 结束地址
	 * @return 日志列表
	 */
	List<Theme> getThemeBySql(String sql, Collection<Object> paras, int begin, int end);
}
