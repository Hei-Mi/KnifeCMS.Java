package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.Bill;

/**
 * 账单接口
 * @author Knife
 *
 */
public interface BillService {
	
	/**
	 * 取得所有账单列表
	 * @return 账单列表
	 */
	List<Bill> getAllBill();
	
	/**
	 * 取得供应商下的账单列表
	 * @param suser 供应商编号
	 * @return 账单列表
	 */
	List<Bill> getBillBySuser(String suser);
	
	/**
	 * 按条件取得账单列表
	 * @param sql 查询条件
	 * @return 账单列表
	 */
	List<Bill> getBillBySql(String sql);
	
	/**
	 * 按条件取得指定范围的账单列表
	 * @param sql 查询条件
	 * @param paras 查询参数
	 * @param begin 起始编号
	 * @param end 结束编号
	 * @return 账单列表
	 */
	List<Bill> getBillBySql(String sql, Collection<Object> paras, int begin, int end);
	
	/**
	 * 按编号取得账单
	 * @param id 账单编号
	 * @return 账单对象
	 */
	Bill getBillById(String id);
	
	/**
	 * 保存账单
	 * @param bill 账单对象
	 * @return ture保存成功，false保存失败
	 */
	boolean saveBill(Bill bill);
	
	/**
	 * 更新账单
	 * @param bill 账单对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateBill(Bill bill);
	
	/**
	 * 删除账单
	 * @param bill 账单对象
	 * @return true删除成功，false删除失败
	 */
	boolean delBill(Bill bill);
}
