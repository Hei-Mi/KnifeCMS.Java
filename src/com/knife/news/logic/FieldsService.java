package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.model.Fields;
import com.knife.news.object.FormField;

/**
 * 自定义字段接口
 * @author Knife
 *
 */
public interface FieldsService {

	/**
	 * 获取字段
	 * @return 字段对象
	 */
	Fields getFields();
	
	/**
	 * 取得所有字段
	 * @return 字段列表
	 */
	List<Fields> getAllFields();
	
	/**
	 * 按文章取得字段
	 * @param newsid 文章编号
	 * @param order 排序
	 * @return 字段对象
	 */
	Fields getFieldsByNews(String newsid,String order);
	
	List<Fields> getFieldsBySql(String sql);
	
	List<Fields> getFieldsBySql(String sql, Collection<Object> paras, int begin, int end);
	
	Fields getFieldsById(String id);
	
	boolean saveFields(Fields fields);
	
	boolean updateFields(Fields fields);
	
	boolean delFields(Fields fields);
	
	List<FormField> jsonStringToList(String newsid,String json);
}
