package com.knife.news.logic;

import com.knife.web.ActionContext;
import com.knife.web.tools.ActiveUser;
import com.knife.web.tools.IActiveUser;
import com.knife.web.tools.ICurrentUser;

/**
 * 当前用户
 * @author Knife
 *
 */
public class CurrentUser implements ICurrentUser {
	public IActiveUser getCurrentUser() {
		ActiveUser user=new ActiveUser();
		user.setUserName("admin");
		user=(ActiveUser)ActionContext.getContext().getSession().getAttribute("bloguser");
		return user;		
	}
	public static IActiveUser  getActiveUser()
	{	
		CurrentUser cu=new CurrentUser();
		IActiveUser user=cu.getCurrentUser();
		return user;
	}
}
