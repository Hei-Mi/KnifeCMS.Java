package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.Content;

public interface ContentService {

	public abstract boolean addContent(Content news);

	public abstract boolean saveContent(Content content);

	public abstract boolean updateContent(Content content);

	public abstract boolean delContent(Content content);

	public abstract List<Content> getContent();

	public abstract List<Content> getContentByNewsid(String newsid);

	public abstract List<Content> getContent(String sql,
			Collection<Object> paras);

	public abstract Content getContentById(String id);

	public abstract List<Content> getCommBySql(String sql,
			Collection<Object> paras, int begin, int end);

}