package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.Video;

/**
 * 视频接口
 * @author Knife
 *
 */
public interface VideoService {
	
	/**
	 * 取得所有视频列表
	 * @return 视频列表
	 */
	List<Video> getAllVideos();
	
	/**
	 * 取得分类下的视频列表
	 * @param typeid 分类编号
	 * @return 视频列表
	 */
	List<Video> getVideosByType(String typeid);
	
	/**
	 * 取得分类树下的视频列表
	 * @param treeid 分类树编号
	 * @return 视频列表
	 */
	List<Video> getVideosByTree(String treeid);
	
	/**
	 * 取得文章下的视频列表
	 * @param newsid 文章编号
	 * @return 视频列表
	 */
	List<Video> getVideosByNewsId(String newsid);
	
	/**
	 * 取得文章下的视频列表
	 * @param newsid 文章编号
	 * @return 视频列表
	 */
	List<Video> getVideosByNewsId(String newsid,String orderby);
	
	/**
	 * 按条件取得视频列表
	 * @param sql 查询条件
	 * @return 视频列表
	 */
	List<Video> getVideosBySql(String sql);
	
	/**
	 * 按条件取得指定范围的视频列表
	 * @param sql 查询条件
	 * @param paras 查询参数
	 * @param begin 起始编号
	 * @param end 结束编号
	 * @return 视频列表
	 */
	List<Video> getVideosBySql(String sql, Collection<Object> paras, int begin, int end);
	
	/**
	 * 按路径取得视频列表
	 * @param url 视频路径
	 * @return 视频列表
	 */
	List<Video> getVideosByUrl(String url);
	
	/**
	 * 按编号取得视频
	 * @param id 视频编号
	 * @return 视频对象
	 */
	Video getVideoById(String id);
	
	/**
	 * 保存视频
	 * @param pic 视频对象
	 * @return ture保存成功，false保存失败
	 */
	boolean saveVideo(Video pic);
	
	/**
	 * 更新视频
	 * @param pic 视频对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateVideo(Video pic);
	
	/**
	 * 删除视频
	 * @param pic 视频对象
	 * @return true删除成功，false删除失败
	 */
	boolean delVideo(Video pic);
}
