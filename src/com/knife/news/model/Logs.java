package com.knife.news.model;

import java.util.Date;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_logs", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Logs {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_content")
	private String content;

	@TableField(name = "k_date")
	private Date date;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setLogs(com.knife.news.object.Logs log){
		this.id=log.getId();
		this.content=log.getContent();
		this.date=log.getDate();
	}
}
