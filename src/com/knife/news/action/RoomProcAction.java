package com.knife.news.action;

import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class RoomProcAction extends BaseCmdAction {

	@Override
	public Page doInit(WebForm form, Module module) {
		return doSummary(form,module);
	}

	public Page doSummary(WebForm form, Module module){
		
		return new Page("score", "user_score.htm","html","/web/" + thisSite.getTemplate() + "/");
	}
	
	public Page doList(WebForm form, Module module){
		return null;
	}
}
