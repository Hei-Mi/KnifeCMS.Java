package com.knife.news.manage.action;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.knife.news.logic.impl.FlowNodeServiceImpl;
import com.knife.news.logic.impl.FlowServiceImpl;
import com.knife.news.logic.impl.SiteServiceImpl;
import com.knife.news.object.Flow;
import com.knife.news.object.FlowNode;
import com.knife.news.object.Site;
import com.knife.tools.ZipUtil;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class FlowManageAction extends ManageAction {
	private FlowServiceImpl flowDAO = FlowServiceImpl.getInstance();
	private SiteServiceImpl siteDAO = SiteServiceImpl.getInstance();
	private FlowNodeServiceImpl flownodeDao = FlowNodeServiceImpl.getInstance();
	private ZipUtil z=new ZipUtil();
	private String sid="";
	
	public Page doInit(WebForm form, Module module) {
		return module.findPage("index");
	}
	
	public Page doList(WebForm form, Module module){
		if(sid.equals("")){
			sid = CommUtil.null2String(form.get("sid"));
		}
		List<Flow> allFlows = flowDAO.getAllFlow();
		int rows = allFlows.size();// 总数
		int pageSize = 18;// 每页18条
		int currentPage = 1;
		int frontPage = 0;
		int nextPage = currentPage+1;
		List<Flow> firstFlows = new ArrayList<Flow>();
		int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));// 计算页数
		Collection<Object> paras = new ArrayList<Object>();
		if(sid.length()>0){
			paras.add(sid);
			form.addResult("sid", sid);
			firstFlows = flowDAO.getFlowBySql("k_site=?",paras,0,pageSize);
		}else{
			firstFlows = flowDAO.getFlowBySql("1=1",paras,0,pageSize);
		}
		Site thisSite = siteDAO.getSiteById(sid);
		form.addResult("thisSite", thisSite);
		form.addResult("rows", rows);
		form.addResult("currentPage", currentPage);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("flowList", firstFlows);
		return module.findPage("list");
	}
	
	public Page doPage(WebForm form, Module module) {
		if(sid.equals("")){
			sid = CommUtil.null2String(form.get("sid"));
		}
		List<Flow> allFlows = flowDAO.getAllFlow();
		int rows = allFlows.size();
		int pageSize = 18;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		Collection<Object> paras = new ArrayList<Object>();
		List<Flow> flowList = new ArrayList<Flow>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		String sql="";
		if(sid.length()>0){
			paras.add(sid);
			form.addResult("sid", sid);
			sql="k_site=?";
		}else{
			sql="1=1";
		}
		if (end < pageSize) {
			flowList = flowDAO.getFlowBySql(sql, paras, begin - 1, end);
		} else {
			flowList = flowDAO.getFlowBySql(sql, paras, begin - 1, pageSize);
		}
		Site thisSite = siteDAO.getSiteById(sid);
		form.addResult("thisSite", thisSite);
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("flowList", flowList);
		return module.findPage("list");
	}
	
	public Page doDelete(WebForm form, Module module) {
		boolean ret=false;
		String id = CommUtil.null2String(form.get("id"));
		if(!id.equals("")){
			Flow aflow =null;
			if(id.indexOf(",")>0){
				String[] ids = id.split(",");
				for (int i = 0; i < ids.length; i++) {
					if(!ids[i].isEmpty()){
						aflow = flowDAO.getFlowById(ids[i]);
//						if(aflow.getFile()!=null){
//							//删除解压的文件夹
//							String dirName=aflow.getFile().getName();
//							dirName=dirName.substring(0,dirName.indexOf("."));
//							File unzipDir=new File(aflow.getFile().getParentFile().getAbsolutePath()+File.separator+dirName);
//							if(unzipDir.exists()){
//								z.deleteFile(unzipDir);
//							}
//							aflow.getFile().delete();
//							File pubFile=new File(aflow.getFile().getAbsolutePath().replace("html", "wwwroot"));
//							if(pubFile.exists()){
//								File unzippubDir=new File(pubFile.getParentFile().getAbsolutePath()+File.separator+dirName);
//								if(unzippubDir.exists()){
//									z.deleteFile(unzippubDir);
//								}
//								pubFile.delete();
//							}
//						}
						//级联删除流节点
						List<FlowNode> nodelist = new ArrayList<FlowNode>();
						nodelist = flownodeDao.getFlowNodeByFlowId(ids[i]);
						for(int j=0;nodelist!=null && j<nodelist.size();j++){
							flownodeDao.delFlowNode(nodelist.get(j));
						}
						//end
						ret=flowDAO.delFlow(aflow);
					}
				}
			}else{
				aflow = flowDAO.getFlowById(id);
//				if(aflow.getFile()!=null){
//					String dirName=aflow.getFile().getName();
//					dirName=dirName.substring(0,dirName.indexOf("."));
//					File unzipDir=new File(aflow.getFile().getParentFile().getAbsolutePath()+File.separator+dirName);
//					if(unzipDir.exists()){
//						z.deleteFile(unzipDir);
//					}
//					aflow.getFile().delete();
//					File pubFile=new File(aflow.getFile().getAbsolutePath().replace("html", "wwwroot"));
//					if(pubFile.exists()){
//						File unzippubDir=new File(pubFile.getParentFile().getAbsolutePath()+File.separator+dirName);
//						if(unzippubDir.exists()){
//							z.deleteFile(unzippubDir);
//						}
//						pubFile.delete();
//					}
//				}
				//级联删除流节点
				List<FlowNode> nodelist = new ArrayList<FlowNode>();
				nodelist = flownodeDao.getFlowNodeByFlowId(id);
				for(int j=0;nodelist!=null && j<nodelist.size();j++){
					flownodeDao.delFlowNode(nodelist.get(j));
				}
				//end
				//System.out.println("删除图片:"+filepath);
				ret=flowDAO.delFlow(aflow);
			}
		}
		if(ret){
			form.addResult("msg", "删除成功！");
		}else{
			form.addResult("msg", "删除失败！");
		}
		return doList(form, module);
	}
	
	public Page doAdd(WebForm form, Module module) {
		String sid = CommUtil.null2String(form.get("sid"));
		form.addResult("sid", sid);
		form.addResult("action", "save");
		return module.findPage("add");
	}
	
	public Page doEdit(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		Flow flow = flowDAO.getFlowById(id);
		form.addResult("sid", flow.getSite());
		form.addResult("flow", flow);
		form.addResult("action", "update");
		return module.findPage("add");
	}
	
	public Page doDesign(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		Flow flow = flowDAO.getFlowById(id);
		form.addResult("sid", flow.getSite());
		form.addResult("flow", flow);
		form.addResult("action", "update");
		return module.findPage("edit");
	}
	
	public Page doSave(WebForm form, Module module) {
		Flow flow = (Flow) form.toPo(Flow.class);
		flow.setDate(new Date());
		if (flowDAO.saveFlow(flow)) {
			form.addResult("msg", "添加成功！");
			return doList(form, module);
		} else {
			return doList(form, module);
		}
	}

	public Page doUpdate(WebForm form, Module module) {
		Flow flow = (Flow) form.toPo(Flow.class);
		if(flow.getDate()==null){
			flow.setDate(new Date());
		}
		if (flowDAO.updateFlow(flow)) {
			form.addResult("msg", "编辑成功！");
			return doList(form, module);
		} else {
			return doList(form, module);
		}
	}
}