package com.knife.news.manage.action;

import com.knife.news.logic.LogsService;
import com.knife.news.logic.UserService;
import com.knife.news.logic.impl.LogsServiceImpl;
import com.knife.news.logic.impl.UserServiceImpl;
import com.knife.news.model.Constants;
import com.knife.news.object.User;
import com.knife.tools.WebXML;
import com.knife.web.ActionContext;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;
import com.knife.web.tools.AbstractCmdAction;

public class ManageAction extends AbstractCmdAction {
	public UserService userDAO = UserServiceImpl.getInstance();
	public LogsService logDAO = LogsServiceImpl.getInstance();
	
	User user = (User) ActionContext.getContext().getSession().getAttribute(
			Constants.SESSION_USER);
	Integer popedom=0;
	String copyType =(String)ActionContext.getContext().getSession().getAttribute("copyType");

    @Override
    public Object doBefore(WebForm form, Module module) {
		if (user != null) {
			popedom=user.getPopedom();
			form.addResult("pop", popedom);
			form.addResult("username", user.getUsername());
			//jlm
			form.addResult("user", user);
			if(copyType!=null){
				form.addResult("copyType", copyType);
			}
			return super.doBefore(form, module);
		}else{
			//判断用户是否统一认证
			WebXML webxml=new WebXML();
			String casUrl=webxml.readParamValue("casServerLoginUrl");
			if(casUrl.length()>0){
				//return new Page("login",casUrl,"url");
				form.addResult("casLogin", casUrl);
			}
			return new Page("login", "/manage_login.html");
		}
    }
    
    public Page doInit(WebForm form, Module module) {
    	return null;
    }
}