package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.impl.FiltServiceImpl;
import com.knife.news.logic.impl.ThumbServiceImpl;
import com.knife.news.model.Filter;
import com.knife.news.model.Thumbnail;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class ThumbnailManageAction extends ManageAction {

	private ThumbServiceImpl thumbDAO = ThumbServiceImpl.getInstance();
	
	private FiltServiceImpl filtDAO = FiltServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return module.findPage("list");
	}

	public Page doShowUsers(WebForm form, Module module) {
		//String tid = CommUtil.null2String(form.get("tid"));
		String addCondition="";
		
		//List userList = thumbDAO.getUsersBySql("id!=''"+addCondition);
		List<Thumbnail> userList = thumbDAO.getThumbsBySql("id!=''"+addCondition);
		int rows = userList.size();// 分页开始
		int pageSize = 15;
		int currentPage = 1;
		int frontPage = 0;
		int nextPage = 2;
		List<Thumbnail> firstUser = new ArrayList<Thumbnail>();
		int totalPage = rows==0?1:((int) Math.ceil((float) (rows) / (float) (pageSize)));
		firstUser = thumbDAO.getThumbsBySql("id!=''"+addCondition+" order by k_username desc",null, 0, pageSize);
		form.addResult("rows", rows);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("currentPage", currentPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("userList", firstUser);
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		String tid = CommUtil.null2String(form.get("tid"));
		String addCondition="";
		if(tid.length()>0){
			addCondition = " and k_popedom='"+tid+"'";
			form.addResult("tid", tid);
		}
		List<Thumbnail> allUser = thumbDAO.getThumbsBySql("id!=''"+addCondition);
		int rows = allUser.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = rows==0?1:((int) Math.ceil((float) (rows) / (float) (pageSize)));
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<Thumbnail> userList = new ArrayList<Thumbnail>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		if (end < pageSize) {
			userList = thumbDAO.getThumbsBySql("id!=''"+addCondition, null, begin - 1, end);
		} else {
			userList = thumbDAO.getThumbsBySql("id!=''"+addCondition, null, begin - 1,pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("userList", userList);
		return module.findPage("list");
	}

	public Page doDelete(WebForm form, Module module) {
		String id = (String) form.get("id");
		boolean ret=false;
		if(id.indexOf(",")>0){
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				Thumbnail u = thumbDAO.getThumbById(ids[i]);
				ret = thumbDAO.deleteThumb(u);//deleteUser(u);
			}
		}else{
			Thumbnail u = thumbDAO.getThumbById(id);
			ret=thumbDAO.deleteThumb(u);
		}
		
		if (ret) {
			form.addResult("msg", "删除成功！");
		} else {
			form.addResult("msg", "删除失败，用户不存在！");			
		}
		return doShowUsers(form, module);
	}
	
	public Page doAdd(WebForm form,Module module){
		form.addResult("action", "save");
		return module.findPage("edit");
	}
	
	public Page doEdit(WebForm form,Module module){
		Thumbnail aUser=null;
		String id = CommUtil.null2String(form.get("id"));
		if(id.equals("")){id="0";}
		if(!id.equals("0")){
			aUser = thumbDAO.getThumbById(id);
		}
		//String tid =aUser.getPopedom()+"";
		form.addResult("action", "update");
		form.addResult("user", aUser);
		return module.findPage("edit");
	}
	
	public Page doSave(WebForm form, Module module) {
		String username = (String) form.get("username");
		Filter huser = filtDAO.getFilterByName(username);
		if (huser != null) {
			form.addResult("msg", "您使用的用户名系统不允许！");
			return doShowUsers(form, module);
		} else {
			Thumbnail puser = thumbDAO.getThumbByName(username);
			if (puser == null) {
				if (username.equals("admin")) {
					form.set("popedom", "3");
				}
				Thumbnail user = (Thumbnail) form.toPo(Thumbnail.class);
				if (thumbDAO.saveThumb(user)) {
					//ActionContext.getContext().getSession().setAttribute(Constants.SESSION_USER, user);
					//form.addResult("user", user);
					form.addResult("msg", "添加成功！");
					return doShowUsers(form, module);
				} else {
					form.addResult("msg", "添加失败！");
					return doShowUsers(form, module);
				}
			} else {
				form.addResult("msg", "用户名已经存在！");
				return doShowUsers(form, module);
			}
		}
	}

	public Page doUpdate(WebForm form, Module module) {
		Thumbnail user = (Thumbnail) form.toPo(Thumbnail.class);
		if (thumbDAO.deleteThumb(user)) {
			form.addResult("msg", "编辑成功！");
			form.addResult("id", user.getId());
			//return doEdit(form, module);
			return doShowUsers(form,module);
		} else {
			form.addResult("msg", "编辑失败！");
			form.addResult("id", user.getId());
			return doEdit(form, module);
			
		}
	}

}
