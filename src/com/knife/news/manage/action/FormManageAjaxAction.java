package com.knife.news.manage.action;

import java.util.List;

import com.knife.news.logic.impl.FormServiceImpl;
import com.knife.news.model.Form;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class FormManageAjaxAction extends ManageAction {
	private FormServiceImpl formdao = FormServiceImpl.getInstance();
	
	public Page doInit(WebForm form,Module module){
		return doChoose(form, module);
	}
	
	public Page doAdd(WebForm form,Module module){
		return module.findPage("ajax");
	}
	
	public Page doChoose(WebForm form,Module module){
		List<Form> allForms=formdao.getAllForms();
		form.addResult("formList", allForms);
		return module.findPage("dialog");
	}
}
