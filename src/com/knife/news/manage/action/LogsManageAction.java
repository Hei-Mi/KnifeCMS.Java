package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.LogsService;
import com.knife.news.logic.impl.LogsServiceImpl;
import com.knife.news.object.Logs;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class LogsManageAction extends ManageAction {

	LogsService logDAO = LogsServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return doList(form, module);
	}

	public Page doList(WebForm form, Module module) {
		List<Logs> allfilts = logDAO.getAllLogs();
		if (allfilts != null) {
			int rows = allfilts.size();// 分页开始
			int pageSize = 15;
			int currentPage = 1;
			int frontPage = 0;
			int nextPage = 2;
			int totalPage = (int) Math
					.ceil((float) (rows) / (float) (pageSize));
			List<Logs> firstfilts = logDAO.getLogsBySql("id!='' order by k_date desc", null, 0,
					pageSize);
			form.addResult("rows", rows);
			form.addResult("pageSize", pageSize);
			form.addResult("frontPage", frontPage);
			form.addResult("currentPage", currentPage);
			form.addResult("nextPage", nextPage);
			form.addResult("totalPage", totalPage);
			form.addResult("logList", firstfilts);
		}
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		String logkey = CommUtil.null2String(form.get("logkey"));
		Collection<Object> paras = new ArrayList<Object>();
		String sql="1=1";
		
		if(logkey.length()>0){
			logkey="%"+logkey+"%";
			paras.add(logkey);
			sql+=" and k_content like ?";
			form.addResult("logkey", logkey);
			List<Logs> allfilts =logDAO.getLogsBySql(sql+" order by k_date", paras, 0, -1);
			if (allfilts != null) {
				int rows = allfilts.size();
				int pageSize = 15;
				int paraPage = CommUtil.null2Int(form.get("page"));
				int frontPage = paraPage - 1;
				int nextPage = paraPage + 1;
				int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
				int begin = (paraPage - 1) * pageSize + 1;
				int end = rows - begin + 1;
				List<Logs> logList = new ArrayList<Logs>();
				form.addResult("frontPage", frontPage);
				form.addResult("nextPage", nextPage);
				if (end < pageSize) {
					logList = logDAO
							.getLogsBySql(sql+" order by k_date", paras, begin - 1, end);
				} else {
					logList = logDAO.getLogsBySql(sql+" order by k_date desc", paras, begin - 1,
							pageSize);
				}
				form.addResult("currentPage", paraPage);
				form.addResult("totalPage", totalPage);
				form.addResult("rows", rows);
				form.addResult("logList", logList);
				return module.findPage("list");
			}else{
				return doLogSearch(form,module);
			}
		}else{
			List<Logs> allfilts = logDAO.getAllLogs();
			int rows = allfilts.size();
			int pageSize = 15;
			int paraPage = CommUtil.null2Int(form.get("page"));
			int frontPage = paraPage - 1;
			int nextPage = paraPage + 1;
			int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
			int begin = (paraPage - 1) * pageSize + 1;
			int end = rows - begin + 1;
			List<Logs> logList = new ArrayList<Logs>();
			form.addResult("frontPage", frontPage);
			form.addResult("nextPage", nextPage);
			if (end < pageSize) {
				logList = logDAO
						.getLogsBySql("id!='' order by k_date", null, begin - 1, end);
			} else {
				logList = logDAO.getLogsBySql("id!='' order by k_date desc", null, begin - 1,
						pageSize);
			}
			form.addResult("currentPage", paraPage);
			form.addResult("totalPage", totalPage);
			form.addResult("rows", rows);
			form.addResult("logList", logList);
			return module.findPage("list");
		}
	}

	public Page doAdd(WebForm form, Module module) {
		form.addResult("action", "save");
		return module.findPage("edit");
	}

	public Page doEdit(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		Logs log = logDAO.getLogsById(id);
		form.addResult("log", log);
		form.addResult("action", "update");
		return module.findPage("edit");
	}

	public Page doDelete(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		boolean result = false;
		//System.out.println("获取到的ID是:" + id);
		if (id.indexOf(",") > 0) {
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				Logs log = logDAO.getLogsById(ids[i]);
				result = logDAO.delLogs(log);
			}
		} else {
			Logs log = logDAO.getLogsById(id);
			result = logDAO.delLogs(log);
		}
		if (result) {
			form.addResult("msg", "删除成功！");
			return doList(form, module);
		} else {
			form.addResult("msg", "删除失败！");
			return doList(form, module);
		}
	}
	
	public Page doClean(WebForm form, Module module) {
		boolean result = false;
		//System.out.println("获取到的ID是:" + id);
		result = logDAO.cleanLogs();
		if (result) {
			form.addResult("msg", "清空成功！");
			return doList(form, module);
		} else {
			form.addResult("msg", "清空失败！");
			return doList(form, module);
		}
	}

	public Page doSave(WebForm form, Module module) {
		Logs log = (Logs) form.toPo(Logs.class);
		if (logDAO.saveLogs(log)) {
			form.addResult("msg", "添加成功！");
			return doList(form, module);
		} else {
			return doList(form, module);
		}
	}

	public Page doUpdate(WebForm form, Module module) {
		Logs log = (Logs) form.toPo(Logs.class);
		if (logDAO.updateLogs(log)) {
			form.addResult("msg", "编辑成功！");
			return doList(form, module);
		} else {
			return doList(form, module);
		}
	}
	public Page doLogSearch(WebForm form, Module module) {
		String logkey = CommUtil.null2String(form.get("logkey"));
		Collection<Object> paras = new ArrayList<Object>();
		String sql="1=1";
		
		if(logkey.length()>0){
			logkey="%"+logkey+"%";
			paras.add(logkey);
			sql+=" and k_content like ?";
			form.addResult("logkey", logkey);
			List<Logs> allfilts =logDAO.getLogsBySql(sql+" order by k_date", paras, 0, -1);
			if (allfilts != null) {
				int rows = allfilts.size();// 分页开始
				int pageSize = 15;
				int currentPage = 1;
				int frontPage = 0;
				int nextPage = 2;
				int totalPage = (int) Math
						.ceil((float) (rows) / (float) (pageSize));
				List<Logs> firstfilts = logDAO.getLogsBySql(sql+" order by k_date desc", paras, 0,
						pageSize);
				form.addResult("rows", rows);
				form.addResult("pageSize", pageSize);
				form.addResult("frontPage", frontPage);
				form.addResult("currentPage", currentPage);
				form.addResult("nextPage", nextPage);
				form.addResult("totalPage", totalPage);
				form.addResult("logList", firstfilts);
			}
			return module.findPage("list");
		}else{
			form.addResult("logkey", logkey);
			return doList(form, module);
		}
		
	}
	
}
