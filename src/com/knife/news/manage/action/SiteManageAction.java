package com.knife.news.manage.action;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.impl.SiteServiceImpl;
import com.knife.news.object.Logs;
import com.knife.news.object.Site;
import com.knife.util.CommUtil;
import com.knife.web.Globals;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class SiteManageAction extends ManageAction{
	private SiteServiceImpl siteDAO = SiteServiceImpl.getInstance();
	
	public Page doInit(WebForm form, Module module) {
		return module.findPage("index");
	}
	
	public Page doList(WebForm form, Module module) {
		if(popedom==0){
			return new Page("noright","/manage/user_index.html");
		}
		List<Site> typeList = siteDAO.getAllSites();
		int rows = typeList.size();// 分页开始
		int pageSize = 15;
		int currentPage = 1;
		int frontPage = 0;
		int nextPage = 2;
		List<Site> firstTypes = new ArrayList<Site>();
		int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));
		String sql="id!=''";
		sql+=" order by length(k_order),k_order";
		firstTypes = siteDAO.getSitesBySql(sql, null,0, pageSize);
		form.addResult("rows", rows);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("currentPage", currentPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("siteList", firstTypes);
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		if(popedom==0){
			return new Page("noright","/manage/user_index.html");
		}
		List<Site> typeList = siteDAO.getAllSites();
		int rows = typeList.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<Site> firstTypes = new ArrayList<Site>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		String sql="id!=''";
		sql+=" order by length(k_order),k_order";
		if (end < pageSize) {
			firstTypes = siteDAO.getSitesBySql(sql, null, begin - 1, end);
		} else {
			firstTypes = siteDAO.getSitesBySql(sql, null, begin - 1, pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("siteList", firstTypes);
		return module.findPage("list");
	}
	
	public Page doAdd(WebForm form,Module module){
		form.addResult("action", "save");
		return module.findPage("edit");
	}
	
	public Page doEdit(WebForm form, Module module){
		String id = CommUtil.null2String(form.get("id"));
		Site mysite = new Site();
		if(id.length()>0){
			mysite=siteDAO.getSiteById(id);
		}else{
			mysite=siteDAO.getSite();
		}
		form.addResult("action", "update");
		form.addResult("site", mysite);
		return module.findPage("edit");
	}
	
	public Page doSave(WebForm form, Module module){
		Site mySite = (Site) form.toPo(Site.class);
		String sid = siteDAO.saveSite(mySite);
		if(sid.length()>0){
			//创建相关文件夹
			Site site=siteDAO.getSiteById(sid);
			if(site.getTemplate()!=null){
				if(site.getTemplate().length()>0){
					File templateFile = new File(Globals.APP_BASE_DIR + Globals.DEFAULT_TEMPLATE_PATH + "/web/" +site.getTemplate());
					if(!templateFile.exists()){
						templateFile.mkdirs();
					}
				}
			}
			Logs log=new Logs(user.getUsername()+"新建站点:"+site.getName());
			logDAO.saveLogs(log);
		}
		form.addResult("msg", "保存成功！");
		return doEdit(form, module);
	}

	public Page doUpdate(WebForm form, Module module) {
		Site site = (Site) form.toPo(Site.class);
		Site mySite = siteDAO.getSiteById(site.getId());
		File templateFile = new File(Globals.APP_BASE_DIR + Globals.DEFAULT_TEMPLATE_PATH + "/web/" +site.getTemplate());
		//System.out.println(Globals.APP_BASE_DIR + Globals.DEFAULT_TEMPLATE_PATH + "/web/" +site.getTemplate());
		if(mySite.getTemplate()!=null){
			if(mySite.getTemplate().length()>0 && !mySite.getTemplate().equals(site.getTemplate())){
				//修改相关文件夹
				File oldTemplate = new File(Globals.APP_BASE_DIR + Globals.DEFAULT_TEMPLATE_PATH + "/web/" +mySite.getTemplate());
				if(!oldTemplate.exists()){
					oldTemplate.mkdirs();
				}
			    String[] files=oldTemplate.list();  
			    File f=null;  
			    String filename=""; 
			    for(String file:files)  
			    {  
			     f=new File(oldTemplate,file);// 注意,这里一定要写成File(fl,file)如果写成File(file)是行不通的,一定要全路径  
			     filename=f.getName();
			     f.renameTo(new File(templateFile.getAbsolutePath()+File.separator+filename));// 这里可以反复使用replace替换,当然也可以使用正则表达式来替换了   
			    }
				oldTemplate.renameTo(templateFile);
			}else{
				templateFile.mkdirs();
			}
		}else{
			templateFile.mkdirs();
		}
		if (siteDAO.updateSite(site)) {
			Logs log=new Logs(user.getUsername()+"编辑站点:"+site.getName());
			logDAO.saveLogs(log);
			form.addResult("msg", "编辑成功！");
		} else {
			form.addResult("msg", "编辑失败！");
		}
		return doEdit(form, module);
	}
	
	public Page doDelete(WebForm form,Module module){
		String id = (String) form.get("id");
		boolean result = false;
		if(id.indexOf(",")>0){
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				Site site = siteDAO.getSiteById(ids[i]);
				//删除相关文件夹
				File templateFile = new File(Globals.APP_BASE_DIR + Globals.DEFAULT_TEMPLATE_PATH + "/web/" +site.getTemplate());
				if(templateFile.exists()){
					templateFile.delete();
				}
				//记录日志
				Logs log=new Logs(user.getUsername()+"删除站点:"+site.getName());
				logDAO.saveLogs(log);
				result = siteDAO.delSite(site);
			}
		}else{
			Site site = siteDAO.getSiteById(id);
			//删除相关文件夹
			File templateFile = new File(Globals.APP_BASE_DIR + Globals.DEFAULT_TEMPLATE_PATH + "/web/" +site.getTemplate());
			if(templateFile.exists()){
				templateFile.delete();
			}
			//记录日志
			Logs log=new Logs(user.getUsername()+"删除站点:"+site.getName());
			logDAO.saveLogs(log);
			result = siteDAO.delSite(site);
		}
		if (result) {
			form.addResult("msg", "删除成功！");
		} else {
			form.addResult("msg", "删除失败！");
		}
		return doList(form, module);
	}
}
