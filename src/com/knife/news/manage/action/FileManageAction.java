package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.impl.TypeServiceImpl;
import com.knife.news.object.Type;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class FileManageAction extends ManageAction {
	private TypeServiceImpl typedao = TypeServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return module.findPage("index");
	}
	
	public Page doType(WebForm form, Module module) {
		List<Type> typeList = typedao.getAllTypes(-1);
		int rows = typeList.size();// 分页开始
		int pageSize = 15;
		int currentPage = 1;
		int frontPage = 1;
		int nextPage = 2;
		List<Type> firstTypes = new ArrayList<Type>();
		int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));
		if(nextPage>totalPage){
			nextPage=totalPage;
		}
		firstTypes = typedao.getTypesBySql("id!='' order by k_order", null,0, pageSize);
		form.addResult("rows", rows);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("currentPage", currentPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("typeList", firstTypes);
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		List<Type> typeList = typedao.getAllTypes(-1);
		int rows = typeList.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<Type> firstTypes = new ArrayList<Type>();

		if (frontPage <= 0) {
			form.addResult("msg", "这是第一页了！");
			form.addResult("frontPage", 1);

		} else {
			form.addResult("frontPage", frontPage);
		}
		if (nextPage > totalPage) {
			nextPage=totalPage;
			form.addResult("msg", "这是最后一页了！");
			form.addResult("nextPage", totalPage);
		} else {
			form.addResult("nextPage", nextPage);
		}
		if (end < pageSize) {

			firstTypes = typedao.getTypesBySql("id!=''", null, begin - 1, end);
		} else {
			firstTypes = typedao.getTypesBySql("id!=''", null, begin - 1, pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("typeList", firstTypes);
		return module.findPage("list");
	}
	
	public Page doAdd(WebForm form,Module module){
		String pid = CommUtil.null2String(form.get("pid"));
		if(pid.equals("")){pid="0";}
		if(!pid.equals("0")){
			Type aType = typedao.getTypeById(pid);
			pid = aType.getParent();
		}
		List<Type> typeList = typedao.getSubTypesById(pid);
		form.addResult("action", "save");
		form.addResult("typeList", typeList);
		return module.findPage("edit");
	}
	
	public Page doEdit(WebForm form,Module module){
		Type aType=null;
		List<Type> typeList =null;
		String id = CommUtil.null2String(form.get("id"));
		String pid = "0";
		if(id.equals("")){id="0";}
		if(!id.equals("0")){
			aType = typedao.getTypeById(id);
			pid = aType.getParent();
		}
		if(!pid.equals("0")){
			Type pType = typedao.getTypeById(pid);
			typeList = typedao.getSubTypesById(pType.getParent());
		}
		form.addResult("action", "update");
		form.addResult("typeList", typeList);
		form.addResult("type", aType);
		return module.findPage("edit");
	}
	
	public Page doSave(WebForm form, Module module) {
		//com.knife.news.model.Type type = (com.knife.news.model.Type) form.toPo(com.knife.news.model.Type.class);
		Type type = (Type) form.toPo(Type.class);
		String tid=typedao.saveType(type);
		if (tid.length()>0) {
			form.addResult("msg", "添加成功！");
			return doType(form, module);
		} else {
			form.addResult("msg", "添加失败！");
			return doType(form, module);
		}
	}
	
	public Page doUpdate(WebForm form, Module module) {
		//com.knife.news.model.Type type = (com.knife.news.model.Type) form.toPo(com.knife.news.model.Type.class);
		Type type = (Type) form.toPo(Type.class);
		if (typedao.updateType(type)) {
			form.addResult("msg", "编辑成功！");
			form.addResult("id", type.getId());
			return doEdit(form, module);
		} else {
			form.addResult("msg", "编辑失败！");
			form.addResult("id", type.getId());
			return doEdit(form, module);
		}
	}

	public Page doDelete(WebForm form, Module module) {
		String id = (String) form.get("id");
		//System.out.print("要删除的ID是:"+id);
		boolean result = false;
		//com.knife.news.model.Type ntype = new com.knife.news.model.Type();
		if(id.indexOf(",")>0){
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				Type type = typedao.getTypeById(ids[i]);
				typedao.reOrderTypes(type);
				//ntype.setType(type);
				result = typedao.delType(type);
			}
		}else{
			Type type = typedao.getTypeById(id);
			typedao.reOrderTypes(type);
			//ntype.setType(type);
			result = typedao.delType(type);
		}
		if (result) {
			form.addResult("msg", "删除成功！");
			return doType(form, module);
		} else {
			form.addResult("msg", "删除失败！");
			return doType(form, module);
		}
	}
}
