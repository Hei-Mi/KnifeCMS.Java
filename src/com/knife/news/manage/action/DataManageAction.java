package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.impl.CommentServiceImpl;
import com.knife.news.object.Comment;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class DataManageAction extends ManageAction {

	CommentServiceImpl commDAO = CommentServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		// TODO Auto-generated method stub
		return doList(form, module);
	}

	public Page doList(WebForm form, Module module) {
		List<Comment> commList = commDAO.getComment();
		int rows = commList.size();// 分页开始
		int pageSize = 15;
		int currentPage = 1;
		int frontPage = 0;
		int nextPage = 2;
		List<Comment> firstComm = new ArrayList<Comment>();
		int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));
		firstComm = commDAO.getCommBySql("id!='' order by k_date desc", null,
				0, pageSize);
		form.addResult("rows", rows);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("currentPage", currentPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("commList", firstComm);
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		List<Comment> allNews = commDAO.getComment();
		int rows = allNews.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<Comment> commList = new ArrayList<Comment>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		if (end < pageSize) {

			commList = commDAO.getCommBySql("id!=''", null, begin - 1, end);
		} else {
			commList = commDAO.getCommBySql("id!=''", null, begin - 1, pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("commList", commList);
		return module.findPage("list");
	}

	public boolean deleteComm(Comment comm) {
		return commDAO.delComment(comm);
	}
	
	public Page doDelete(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		boolean result = false;
		if(id.indexOf(",")>0){
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				Comment comm = commDAO.getCommentById(ids[i]);
				result = deleteComm(comm);
			}
		}else{
			//News news = newsdao.getNewsById(id);
			Comment comm = commDAO.getCommentById(id);
			result = deleteComm(comm);
		}
		//News news = newsdao.getNewsById(id);
		if (result){
			form.addResult("msg", "删除新闻成功！");
			return doList(form, module);
		} else {
			form.addResult("msg", "删除失败！");
			return doList(form, module);
		}
	}
}
