package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.impl.TreeServiceImpl;
import com.knife.news.model.Tree;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class TreeManageAjaxAction extends ManageAction {
	private TreeServiceImpl treeDAO = TreeServiceImpl.getInstance();
	
	public Page doAdd(WebForm form,Module module){
		Tree tree = (Tree) form.toPo(Tree.class);
		//type.setName(name);
		tree.setTree_type("1");
		tree.setShow(1);
		if (treeDAO.saveTree(tree)) {
			form.addResult("jvalue", tree.getId());
		} else {
			form.addResult("jvalue", "0");
		}
		return module.findPage("ajax");
	}
	
	public Page doShowList(WebForm form,Module module) {
		List<Tree> types=new ArrayList<Tree>();
		String checkStyle=CommUtil.null2String(form.get("checkStyle"));
		if(checkStyle.equals("")){checkStyle="checkbox";}
		types = treeDAO.getAllTree(1);
		form.addResult("treeList", types);
		form.addResult("checkStyle", checkStyle);
		return module.findPage("dialog");
	}
	
	public Page doRename(WebForm form,Module module){
		String id = (String) form.get("id");
		String name = CommUtil.null2String(form.get("name"));
		//System.out.print("获取到的名称:"+name);
		Tree tree = treeDAO.getTreeById(id);
		tree.setName(name);
		treeDAO.updateTree(tree);
		return null;
	}
	
	public Page doDelete(WebForm form,Module module){
		String id = (String) form.get("id");
		Tree tree = treeDAO.getTreeById(id);
		treeDAO.delTree(tree);
		return null;
	}
}
