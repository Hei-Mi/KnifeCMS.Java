package com.knife.news.manage.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.knife.news.logic.GrabService;
import com.knife.news.logic.SiteService;
import com.knife.news.logic.impl.GrabServiceImpl;
import com.knife.news.logic.impl.SiteServiceImpl;
import com.knife.news.object.Grab;
import com.knife.news.object.Logs;
import com.knife.news.object.Site;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class GrabManageAction extends ManageAction {
	private SiteService siteDAO = SiteServiceImpl.getInstance();
	private GrabService grabDAO = GrabServiceImpl.getInstance();
	//private NewsService newsDAO = NewsServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return module.findPage("index");
	}
	
	public Page doList(WebForm form, Module module) {
		if(popedom==0){
			return new Page("noright","/manage/user_index.html");
		}
		String site_id = CommUtil.null2String(form.get("sid"));
		String tree_id = CommUtil.null2String(form.get("tree_id"));
		List<Grab> typeList = grabDAO.getGrabs();
		int rows = typeList.size();// 分页开始
		int pageSize = 15;
		int currentPage = 1;
		int frontPage = 0;
		int nextPage = 2;
		List<Grab> firstTypes = new ArrayList<Grab>();
		int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));
		String sql="id!=''";
		Collection<Object> paras=new ArrayList<Object>();
		if(site_id.length()>0){
			sql+=" and k_site=?";
			paras.add(site_id);
			form.addResult("sid", site_id);
		}
		if(tree_id.length()>0){
			sql+=" and k_tree_id=?";
			paras.add(tree_id);
			form.addResult("tree_id", tree_id);
		}
		sql+=" order by id";
		firstTypes = grabDAO.getGrabsBySql(sql, paras,0, pageSize);
		form.addResult("rows", rows);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("currentPage", currentPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("grabList", firstTypes);
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		if(popedom==0){
			return new Page("noright","/manage/user_index.html");
		}
		String site_id = CommUtil.null2String(form.get("sid"));
		String tree_id = CommUtil.null2String(form.get("tree_id"));
		List<Grab> typeList = grabDAO.getGrabs();
		int rows = typeList.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<Grab> firstTypes = new ArrayList<Grab>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		String sql="id!=''";
		Collection<Object> paras=new ArrayList<Object>();
		if(site_id.length()>0){
			sql+=" and k_site=?";
			paras.add(site_id);
			form.addResult("sid", site_id);
		}
		if(tree_id.length()>0){
			sql+=" and k_tree_id=?";
			paras.add(tree_id);
			form.addResult("tree_id", tree_id);
		}
		sql+=" order by id";
		if (end < pageSize) {
			firstTypes = grabDAO.getGrabsBySql(sql, paras, begin - 1, end);
		} else {
			firstTypes = grabDAO.getGrabsBySql(sql, paras, begin - 1, pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("grabList", firstTypes);
		return module.findPage("list");
	}
	
	public Page doAdd(WebForm form,Module module){
		String site_id = CommUtil.null2String(form.get("sid"));
		if(site_id.length()>0){
			form.addResult("sid", site_id);
			Site site=siteDAO.getSiteById(site_id);
			form.addResult("site", site);
		}
		String tree_id = CommUtil.null2String(form.get("tree_id"));
		if(tree_id.length()>0){
			form.addResult("tree_id", tree_id);
		}
		form.addResult("action", "save");
		return module.findPage("edit");
	}
	
	public Page doEdit(WebForm form,Module module){
		String nid = CommUtil.null2String(form.get("id"));
		Grab grab=grabDAO.getGrabById(nid);
		form.addResult("action", "update");
		form.addResult("thisGrab", grab);
		return module.findPage("edit");
	}
	
	public Page doSave(WebForm form, Module module) {
		//com.knife.news.model.Type type = (com.knife.news.model.Type) form.toPo(com.knife.news.model.Type.class);
		com.knife.news.model.Grab type = (com.knife.news.model.Grab) form.toPo(com.knife.news.model.Grab.class);
		Grab grab=new Grab(type);
		String retid=grabDAO.saveGrab(grab);
		if (!"".equals(retid)) {
			//记录日志
			Logs log=new Logs(user.getUsername()+"添加采集:"+type.getTitle());
			logDAO.saveLogs(log);
			form.addResult("msg", "添加成功！");
			return doList(form, module);
		} else {
			form.addResult("msg", "添加失败！");
			return doList(form, module);
		}
	}
	
	public Page doUpdate(WebForm form, Module module) {
		com.knife.news.model.Grab type = (com.knife.news.model.Grab) form.toPo(com.knife.news.model.Grab.class);
		Grab grab=new Grab(type);
		if (grabDAO.updateGrab(grab)) {
			//记录日志
			Logs log=new Logs(user.getUsername()+"编辑采集:"+type.getTitle());
			logDAO.saveLogs(log);
			form.addResult("msg", "编辑成功！");
		} else {
			form.addResult("msg", "编辑失败！");
		}
		return doEdit(form, module);
	}

	public Page doDelete(WebForm form, Module module) {
		String id = (String) form.get("id");
		//System.out.print("要删除的ID是:"+id);
		boolean result = false;
		//com.knife.news.model.Type atype = new com.knife.news.model.Type();
		if(id.indexOf(",")>0){
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				Grab type = grabDAO.getGrabById(ids[i]);
				//记录日志
				Logs log=new Logs(user.getUsername()+"删除采集:"+type.getTitle());
				logDAO.saveLogs(log);
				result = grabDAO.delGrab(type);
			}
		}else{
			Grab type = grabDAO.getGrabById(id);
			//记录日志
			Logs log=new Logs(user.getUsername()+"删除频道:"+type.getTitle());
			logDAO.saveLogs(log);
			result = grabDAO.delGrab(type);
		}
		if (result) {
			form.addResult("msg", "删除成功！");
		} else {
			form.addResult("msg", "删除失败！");
		}
		return doList(form, module);
	}
	
	public Page doConfigList(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		form.addResult("id", id);
		return module.findPage("config_list");
	}
}
