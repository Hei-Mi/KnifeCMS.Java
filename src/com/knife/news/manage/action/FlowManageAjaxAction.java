package com.knife.news.manage.action;



import java.util.List;

import com.knife.news.logic.FlowService;
import com.knife.news.logic.impl.FlowServiceImpl;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;
import com.knife.news.object.Flow;

public class FlowManageAjaxAction extends ManageAction {

	private FlowServiceImpl flowDao = FlowServiceImpl.getInstance();
	
	public Page doInit(WebForm form,Module module){
		return doChoose(form, module);
	}
	public Page doList(){
		String id = CommUtil.null2String("sid");
		
		return null ; 
	}
	
	public Page doChoose(WebForm form,Module module){
		String sid = CommUtil.null2String(form.get("sid"));
		//只得到发文流程
		List<Flow> flows= flowDao.getFlowBySql("k_site="+sid+" and k_type=1");
		form.addResult("flowList", flows);
		return module.findPage("dialog");
	}
}
