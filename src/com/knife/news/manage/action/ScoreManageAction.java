package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.UserService;
import com.knife.news.logic.impl.UserServiceImpl;
import com.knife.news.model.Filter;
import com.knife.news.object.Type;
import com.knife.news.object.User;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class ScoreManageAction extends ManageAction {

	private UserService userDAO = UserServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return module.findPage("index");
	}

	public Page doList(WebForm form, Module module) {
		String tid = CommUtil.null2String(form.get("tid"));
		String addCondition = "";
		if (tid.length() > 0) {
			addCondition = " and k_popedom='" + tid + "'";
			form.addResult("tid", tid);
		}
		String key_words = CommUtil.null2String(form.get("key_words"));
		if (key_words.length() > 0) {
			addCondition = " and (k_username like '%" + key_words
					+ "%' or k_email like '%" + key_words + "%')";
		}
		List<User> userList = userDAO.getUsersBySql("id!=''" + addCondition);
		int rows = userList.size();// 分页开始
		int pageSize = 15;
		int currentPage = 1;
		int frontPage = 0;
		int nextPage = 2;
		List<User> firstUser = new ArrayList<User>();
		int totalPage = rows == 0 ? 1 : ((int) Math.ceil((float) (rows)
				/ (float) (pageSize)));
		firstUser = userDAO.getUsersBySql("id!=''" + addCondition
				+ " order by k_username desc", null, 0, pageSize);
		form.addResult("rows", rows);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("currentPage", currentPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("userList", firstUser);
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		String tid = CommUtil.null2String(form.get("tid"));
		String addCondition = "";
		if (tid.length() > 0) {
			addCondition = " and k_popedom='" + tid + "'";
			form.addResult("tid", tid);
		}
		String key_words = CommUtil.null2String(form.get("key_words"));
		if (key_words.length() > 0) {
			addCondition = " and (k_username like '%" + key_words
					+ "%' or k_email like '%" + key_words + "%')";
		}
		List<User> allUser = userDAO.getUsersBySql("id!=''" + addCondition);
		int rows = allUser.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = rows == 0 ? 1 : ((int) Math.ceil((float) (rows)
				/ (float) (pageSize)));
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<User> userList = new ArrayList<User>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		if (end < pageSize) {
			userList = userDAO.getUsersBySql("id!=''" + addCondition, null,
					begin - 1, end);
		} else {
			userList = userDAO.getUsersBySql("id!=''" + addCondition, null,
					begin - 1, pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("userList", userList);
		return module.findPage("list");
	}

	public Page doDelete(WebForm form, Module module) {
		String id = (String) form.get("id");
		boolean ret = false;
		if (id.indexOf(",") > 0) {
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				User u = userDAO.getUserById(ids[i]);
				ret = userDAO.deleteUser(u);
			}
		} else {
			User u = userDAO.getUserById(id);
			ret = userDAO.deleteUser(u);
		}

		if (ret) {
			form.addResult("msg", "删除成功！");
		} else {
			form.addResult("msg", "删除失败，用户不存在！");
		}
		return doList(form, module);
	}

	public Page doAdd(WebForm form, Module module) {
		String tid = CommUtil.null2String(form.get("tid"));
		if (tid.equals("")) {
			tid = "0";
		}
		form.addResult("action", "save");
		form.addResult("tid", tid);
		return module.findPage("edit");
	}

	public Page doEdit(WebForm form, Module module) {
		User aUser = null;
		String id = CommUtil.null2String(form.get("id"));
		if (id.equals("")) {
			id = "0";
		}
		if (!id.equals("0")) {
			aUser = userDAO.getUserById(id);
		}
		if(aUser!=null){
			String tid = aUser.getPopedom() + "";
			form.addResult("action", "update");
			form.addResult("tid", tid);
			form.addResult("user", aUser);
		}
		return module.findPage("edit");
	}

	public Page doSave(WebForm form, Module module) {
		String username = CommUtil.null2String(form.get("username"));
			User puser = userDAO.getUserByName(username);
			if (puser == null) {
				if (username.equals("admin")) {
					form.set("popedom", "3");
				}
				User user = (User) form.toPo(User.class);
				if (userDAO.saveUser(user)) {
					// ActionContext.getContext().getSession().setAttribute(Constants.SESSION_USER,
					// user);
					// form.addResult("user", user);
					form.addResult("msg", "添加成功！");
					return doList(form, module);
				} else {
					form.addResult("msg", "添加失败！");
					return doList(form, module);
				}
			} else {
				form.addResult("msg", "用户名已经存在！");
				return doList(form, module);
			}
	}

	public Page doUpdate(WebForm form, Module module) {
		User user = (User) form.toPo(User.class);
		//System.out.println("user:"+user.getId());
		if (userDAO.updateUser(user)) {
			form.addResult("msg", "编辑成功！");
		} else {
			form.addResult("msg", "编辑失败！");
		}
		form.addResult("id", user.getId());
		return doEdit(form, module);
	}

	public Page doNoSetSuper(WebForm form, Module module) {
		String id = (String) form.get("id");
		User user = userDAO.getUserById(id);
		user.setPopedom(0);
		if (userDAO.updateUser(user)) {
			form.addResult("msg", "设置成功！");
			return doList(form, module);
		} else {
			form.addResult("msg", "设置失败！");
			return doList(form, module);
		}
	}
}
