package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.knife.news.logic.SurveyDefineService;
import com.knife.news.logic.SurveyService;
import com.knife.news.logic.impl.SurveyDefineServiceImpl;
import com.knife.news.logic.impl.SurveyServiceImpl;
import com.knife.news.object.Logs;
import com.knife.news.object.Survey;
import com.knife.news.object.SurveyDefine;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class SurveyManageAction extends ManageAction{
	private SurveyDefineService surveyDefineDAO = SurveyDefineServiceImpl.getInstance();
	private SurveyService surveyDAO = SurveyServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return doList(form, module);
	}
	
	public Page doList(WebForm form, Module module) {
		List<SurveyDefine> typeList = surveyDefineDAO.getAllSurveyDefines();
		int rows = typeList.size();// 分页开始
		int pageSize = 15;
		int currentPage = 1;
		int frontPage = 0;
		int nextPage = 2;
		List<SurveyDefine> firstTypes = new ArrayList<SurveyDefine>();
		int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));
		firstTypes = surveyDefineDAO.getSurveyDefinesBySql("id!=''", null,0, pageSize);
		form.addResult("rows", rows);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("currentPage", currentPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("surveyList", firstTypes);
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		List<SurveyDefine> typeList = surveyDefineDAO.getAllSurveyDefines();
		int rows = typeList.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<SurveyDefine> firstTypes = new ArrayList<SurveyDefine>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		if (end < pageSize) {
			firstTypes = surveyDefineDAO.getSurveyDefinesBySql("id!=''", null, begin - 1, end);
		} else {
			firstTypes = surveyDefineDAO.getSurveyDefinesBySql("id!=''", null, begin - 1, pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("surveyList", firstTypes);
		return module.findPage("list");
	}
	
	public Page doAdd(WebForm form, Module module) {
		form.addResult("action", "save");
		return module.findPage("edit");
	}
	
	public Page doSave(WebForm form, Module module) {
		SurveyDefine myform = (SurveyDefine) form.toPo(SurveyDefine.class);
		if (surveyDefineDAO.saveSurveyDefine(myform)) {
			form.addResult("msg", "添加成功！");
			return doList(form, module);
		} else {
			form.addResult("msg", "添加失败！");
			return doList(form, module);
		}
	}
	
	public Page doAjaxSave(WebForm form, Module module){
		String id = CommUtil.null2String(form.get("id"));
		SurveyDefine myform = (SurveyDefine) form.toPo(SurveyDefine.class);
		if(myform.getXml()!=null){
			JSONObject surveyXml = JSONObject.fromObject(myform.getXml());
			String surveyFields = surveyXml.getString("fields");
			myform.setXml(surveyFields.substring(1, surveyFields.length()-1));
		}
		if("".equals(id)){
			if(surveyDefineDAO.saveSurveyDefine(myform)){
				Logs log=new Logs(user.getUsername()+"添加调查:"+myform.getName());
				logDAO.saveLogs(log);
				form.addResult("jvalue", myform.getId());
			}else{
				form.addResult("jvalue", "");
			}
		}else{
			if(surveyDefineDAO.updateSurveyDefine(myform)){
				Logs log=new Logs(user.getUsername()+"编辑调查:"+myform.getName());
				logDAO.saveLogs(log);
				form.addResult("jvalue", myform.getId());
			}else{
				form.addResult("jvalue", "");
			}
		}
		return module.findPage("ajax");
	}
	
	public Page doEdit(WebForm form,Module module){
		String id = CommUtil.null2String(form.get("id"));
		if(id.equals("")){id="0";}
		SurveyDefine aform = surveyDefineDAO.getSurveyDefineById(id);
		form.addResult("action", "update");
		form.addResult("surveyDefine", aform);
		return module.findPage("edit");
	}
	
	public Page doUpdate(WebForm form, Module module) {
		SurveyDefine aform = (SurveyDefine) form.toPo(SurveyDefine.class);
		if (surveyDefineDAO.updateSurveyDefine(aform)) {
			form.addResult("msg", "编辑成功！");
			form.addResult("id", aform.getId());
			return doEdit(form, module);
		} else {
			form.addResult("msg", "编辑失败！");
			form.addResult("id", aform.getId());
			return doEdit(form, module);
		}
	}
	
	public Page doDelete(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		boolean result = false;
		if(id.indexOf(",")>0){
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				SurveyDefine aform = surveyDefineDAO.getSurveyDefineById(ids[i]);
				result = surveyDefineDAO.delSurveyDefine(aform);
			}
		}else{
			SurveyDefine aform = surveyDefineDAO.getSurveyDefineById(id);
			result = surveyDefineDAO.delSurveyDefine(aform);
		}
		if (result) {
			form.addResult("msg", "删除成功！");
			return doList(form, module);
		} else {
			form.addResult("msg", "删除失败！");
			return doList(form, module);
		}
	}
	
	public Page doResult(WebForm form, Module module){
		String id = CommUtil.null2String(form.get("id"));
		SurveyDefine surveyDefine=surveyDefineDAO.getSurveyDefineById(id);
		JSONArray surveyXml = JSONArray.fromObject("["+surveyDefine.getXml()+"]");
		form.addResult("fields", surveyXml);
		List<Survey> typeList = surveyDAO.getSurveyBySid(id);
		int rows = typeList.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		if(begin<0){begin=1;}
		int end = rows - begin + 1;
		List<Survey> firstTypes = new ArrayList<Survey>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(id);
		if (end < pageSize) {
			firstTypes = surveyDAO.getSurveyBySql("k_sid=?", paras, begin - 1, end);
		} else {
			firstTypes = surveyDAO.getSurveyBySql("k_sid=?", paras, begin - 1, pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("surveyList", firstTypes);
		return module.findPage("result");
	}
	
	public Page doReport(WebForm form, Module module){
		String id = CommUtil.null2String(form.get("id"));
		SurveyDefine surveyDefine=surveyDefineDAO.getSurveyDefineById(id);
		JSONArray surveyXml = JSONArray.fromObject("["+surveyDefine.getXml()+"]");
		form.addResult("fields", surveyXml);
		return module.findPage("report");
	}
	
	public Page doChart(WebForm form, Module module){
		String field="";
		
		return module.findPage("chart");
	}
}