package com.knife.news.manage.thread;

import com.knife.service.GenerateSiteTask;

public class SiteThreads {
	private static Thread gthread;
	
	public static Thread getThread(GenerateSiteTask task){
		gthread=new Thread(task);
		return gthread;
	}
}
