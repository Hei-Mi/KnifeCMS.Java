package com.knife.news.object;

import java.util.Date;
import java.util.List;

import com.knife.news.logic.TypeService;
import com.knife.news.logic.impl.TypeServiceImpl;

/**
 * 后台用户对象
 * @author Knife
 *
 */
public class User {
	private String id;
	private String username;
	private String password;
	private String email;
	private String qq;
	private String tel;
	private Date birthday;
	private Date sysj;
	private String fax;
	private String addr;
	private String intro;
	private int popedom;
	private int document_show;
	
	private TypeService typeDAO = TypeServiceImpl.getInstance();
	
	public User(){}
	
    public User(com.knife.news.model.User user){
			this.id=user.getId();
			this.username=user.getUsername();
			this.password=user.getPassword();
			this.email=user.getEmail();
			this.qq=user.getQq();
			this.tel=user.getTel();
			this.birthday=user.getBirthday();
			this.sysj=user.getSysj();
			this.fax=user.getFax();
			this.addr=user.getAddr();
			this.intro=user.getIntro();
			this.popedom=user.getPopedom();
			this.document_show=user.getDocument_show();
    }

    /**
     * 取得任意文件权限
     * @return 权限值
     */
	public int getDocument_show() {
		return document_show;
	}

	/**
	 * 设置任意文件权限
	 * @param document_show 权限值
	 */
	public void setDocument_show(int document_show) {
		this.document_show = document_show;
	}
    
    /**
     * 取得权限
     * @return 权限值
     */
	public int getPopedom() {
		return popedom;
	}

	/**
	 * 设置权限
	 * @param popedom 权限值
	 */
	public void setPopedom(int popedom) {
		this.popedom = popedom;
	}

	
	/**
	 * 取得地址
	 * @return 地址
	 */
	public String getAddr() {
		return addr;
	}

	/**
	 * 设置地址
	 * @param addr 地址
	 */
	public void setAddr(String addr) {
		this.addr = addr;
	}

	/**
	 * 取得邮件地址
	 * @return 邮件地址
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * 设置邮件地址
	 * @param email 邮件地址
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 取得传真地址
	 * @return 传真地址
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * 设置传真地址
	 * @param fax 传真地址
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * 取得自我介绍
	 * @return 自我介绍
	 */
	public String getIntro() {
		return intro;
	}

	/**
	 * 设置自我介绍
	 * @param intro 自我介绍
	 */
	public void setIntro(String intro) {
		this.intro = intro;
	}

	/**
	 * 取得用户密码
	 * @return 用户密码
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 设置用户密码
	 * @param password 用户密码
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * 取得QQ信息
	 * @return QQ信息
	 */
	public String getQq() {
		return qq;
	}

	/**
	 * 设置QQ信息
	 * @param qq QQ信息
	 */
	public void setQq(String qq) {
		this.qq = qq;
	}

	/**
	 * 取得电话信息
	 * @return 电话信息
	 */
	public String getTel() {
		return tel;
	}

	/**
	 * 设置电话信息
	 * @param tel 电话信息
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}

	/**
	 * 取得用户名
	 * @return 用户名
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * 设置用户名
	 * @param username 用户名
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * 取得用户编号
	 * @return 用户编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 设置用户编号
	 * @param id 用户编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 取得用户生日
	 * @return 用户生日
	 */
	public Date getBirthday() {
		return birthday;
	}

	/**
	 * 设置用户生日
	 * @param birthday 用户生日
	 */
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public Date getSysj() {
		return sysj;
	}
	public void setSysj(Date sysj) {
		this.sysj = sysj;
	}
	/**
	 * 取得用户拥有权限的频道
	 * @return 频道列表
	 */
	public List<Type> getTypes(){
		String sql="1=1";
		if(this.popedom==0){
			sql+=" and k_admin like '%"+this.getId()+",%'";
		}else{
			sql+=" and k_audit like '%"+this.getId()+",%'";
		}
		List<Type> types=typeDAO.getTypesBySql(sql, null, 0, -1);
		return types;
	}
	
	/**
	 * 取得用户拥有权限的频道编号
	 * @return 频道名称，多个用","隔开
	 */
	public String getTypeID(){
		String typeid="";
		List<Type> types=getTypes();
		for(Type atype:types){
			typeid += atype.getId()+",";
		}
		return typeid;
	}
	
	/**
	 * 取得用户拥有权限的频道名称
	 * @return 频道名称，多个用","隔开
	 */
	public String getTypename(){
		String typename="";
		List<Type> types=getTypes();
		for(Type atype:types){
			typename += atype.getName()+",";
		}
		return typename;
	}
}
