package com.knife.news.object;

import java.io.File;
import java.util.Date;

import net.coobird.thumbnailator.Thumbnails;

import com.knife.member.Userinfo;
import com.knife.member.UserinfoDAO;
import com.knife.web.Globals;

/**
 * 图片对象
 * @author Knife
 *
 */
public class Pic {
	private String id;
	private String title;
	private String pictype;
	@SuppressWarnings("unused")
	private String name;
	@SuppressWarnings("unused")
	private String path;
	private String url;
	private String newsid;
	private int userid;
	private Date date;
	private int order;
	private int display;
	private File file;
	private File sfile;
	
    public Pic(com.knife.news.model.Pic p){
		this.id=p.getId();
		this.title=p.getTitle();
		this.pictype=p.getPictype();
    	this.url=p.getUrl();
    	this.name=p.getUrl().substring(url.lastIndexOf("/")+1);
    	this.path=p.getUrl().substring(0,url.lastIndexOf("/"));
    	this.newsid=p.getNewsid();
    	this.userid=p.getUserid();
    	this.date=p.getDate();
    	this.order=p.getOrder();
    	this.display=p.getDisplay();
    }
	
    /**
     * 取得图片编号
     * @return 图片编号
     */
	public String getId() {
		return id;
	}
	
	/**
	 * 设置图片编号
	 * @param id 图片编号
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * 取得图片标题
	 * @return 图片标题
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * 设置图片标题
	 * @param title 图片标题
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * 取得图片分类
	 * @return 图片分类编号
	 */
	public String getPictype() {
		return pictype;
	}
	
	/**
	 * 设置图片分类
	 * @param title 图片分类编号
	 */
	public void setPictype(String pictype) {
		this.pictype = pictype;
	}
	
	/**
	 * 图片名称
	 * @return 图片名
	 */
	public String getName(){
		return this.getUrl().substring(url.lastIndexOf("/")+1);
	}
	
	/**
	 * 图片路径(不包含名称)
	 * @return 图片路径
	 */
	public String getPath(){
		return this.getUrl().substring(0,url.lastIndexOf("/"));
	}
	
	/**
	 * 取得图片地址
	 * @return 图片地址
	 */
	public String getUrl() {
		return url;
	}
	
	/**
	 * 设置图片地址
	 * @param url 图片地址
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	/**
	 * 取得图片所属新闻编号
	 * @return 新闻编号
	 */
	public String getNewsid() {
		return newsid;
	}
	
	/**
	 * 设置图片所属新闻编号
	 * @param newsid 新闻编号
	 */
	public void setNewsid(String newsid) {
		this.newsid = newsid;
	}
	
	public int getUserid(){
		return userid;
	}
	
	public void setUserid(int userid){
		this.userid=userid;
	}
	
	public Date getDate(){
		return date;
	}
	
	public void setDate(Date date){
		this.date=date;
	}
	
	/**
	 * 取得排序编号
	 * @return 排序编号
	 */
	public int getOrder() {
		return order;
	}
	
	/**
	 * 设置排序编号
	 * @param order 排序编号
	 */
	public void setOrder(int order) {
		this.order = order;
	}
	
	public int getDisplay() {
		return display;
	}

	public void setDisplay(int display) {
		this.display = display;
	}

	/**
	 * 取得图片文件
	 * @return 文件对象
	 */
	public File getFile(){
		String filepath=Globals.APP_BASE_DIR + this.getUrl();
		filepath=filepath.replace("/", File.separator);
		filepath=filepath.replace(File.separator+File.separator, File.separator);
		file=new File(filepath);
		if(file.exists()){
			return file;
		}else{
			return null;
		}
	}
	
	/**
	 * 取得缩略图文件
	 * @return 文件对象
	 */
	public File getSfile(){
		String sfilepath=Globals.APP_BASE_DIR + this.getPath() + "/s_"+this.getName();
		sfilepath=sfilepath.replace("/", File.separator);
		sfilepath=sfilepath.replace(File.separator+File.separator, File.separator);
		sfile=new File(sfilepath);
		if(sfile.exists()){
			return sfile;
		}else{
			return null;
		}
	}
	
	/**
	 * 取得缩略图
	 * @return 缩略图web路径
	 */
	public String getSpic(){
		try{
		File spic=this.getSfile();
		if(spic.exists()){
			return this.getPath()+"/s_"+this.getName();
		}else{
			return "";
		}
		}catch(Exception e){
			return "";
		}
	}
	
	/**
	 * 设置缩略图
	 * @param width 缩略图宽
	 * @param height 缩略图高
	 */
	public void setSpic(int width,int height){
		//如果不存在,则先生成
		File spic=this.getSfile();
		if(spic.exists()){
			spic.delete();
		}
		try{
		Thumbnails.of(this.getFile()).size(width, height).keepAspectRatio(false)
		.outputQuality(1.0f).toFile(spic);
		}catch(Exception e){}
	}
	
	/**
	 * 取得图片作者
	 * @return
	 */
	public String getAuthor(){
		String author="";
		if(this.getUserid()>0){
			UserinfoDAO uDAO=new UserinfoDAO();
			Userinfo auser=uDAO.findById((long)this.getUserid());
			if(auser!=null){
				author=auser.getAcount(); 
			}
		}
		return author;
	}
}
