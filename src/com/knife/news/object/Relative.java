package com.knife.news.object;

/**
 * 相关文章对象
 * @author Knife
 *
 */
public class Relative {
	private String id;
	private String news_id;
	private String rels;
	
	public Relative(){}
	
	/**
	 * 构造方法，将数据模型转换为对象
	 * @param relative 相关文章的数据模型对象
	 */
	public Relative(com.knife.news.model.Relative relative){
		this.id		= relative.getId();
		this.news_id= relative.getNews_id();
		this.rels	= relative.getRels();
	}
	
	/**
	 * 取得相关文章对象编号
	 * @return 相关文章对象编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 设置相关文章对象编号
	 * @param id 相关文章对象编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 取得主文章编号
	 * @return 主文章编号
	 */
	public String getNews_id() {
		return news_id;
	}

	/**
	 * 设置主文章编号
	 * @param news_id 主文章编号
	 */
	public void setNews_id(String news_id) {
		this.news_id = news_id;
	}
	
	/**
	 * 取得相关文章编号，用逗号(,)隔开
	 * @return 相关文章编号
	 */
	public String getRels() {
		return rels;
	}

	/**
	 * 设置相关文章编号，用逗号(,)隔开
	 * @param rels 相关文章编号
	 */
	public void setRels(String rels) {
		this.rels = rels;
	}
}