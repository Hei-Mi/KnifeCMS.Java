package com.knife.news.object;


/**
 * 主题对象
 * @author Knife
 *
 */
public class Theme {
	private String id;
	private String name;
	private String description;
	private String css;
	private int status;
	
	public Theme(){
	}
	
	public Theme(com.knife.news.model.Theme theme){
		this.id			= theme.getId();
		this.name		= theme.getName();
		this.description= theme.getDescription();
		this.css		= theme.getCss();
		this.status		= theme.getStatus();
	}
	
	/**
	 * 取得主题编号
	 * @return 主题编号
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * 设置主题编号
	 * @param id 主题编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 取得主题名称
	 * @return 主题名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置主题名称
	 * @param name 主题名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 取得主题描述
	 * @return 主题描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 设置主题描述
	 * @param description 主题描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 取得主题样式
	 * @return 主题样式
	 */
	public String getCss() {
		return css;
	}

	/**
	 * 设置主题样式
	 * @param css 主题样式
	 */
	public void setCss(String css) {
		this.css = css;
	}

	/**
	 * 取得主题状态
	 * @return 主题状态：0未使用,1已使用
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * 设置主题状态
	 * @param status 主题状态：0未使用,1已使用
	 */
	public void setStatus(int status) {
		this.status = status;
	}
}