package com.knife.news.object;

/**
 * 商品对象
 * @author Knife
 *
 */
public class Commodity {
	private String id;
	private String name;
	private String title;
	private int money;
	private int score;
	private int num;
	
	/**
	 * 取得商品编号
	 * @return 商品编号
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * 设置商品编号
	 * @param id 商品编号
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * 取得商品名称
	 * @return 商品名称
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 设置商品名称
	 * @param name 商品名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 取得商品标题
	 * @return 商品标题
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * 设置商品标题
	 * @param title 商品标题
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * 取得商品价格
	 * @return 商品价格
	 */
	public int getMoney() {
		return money;
	}
	
	/**
	 * 设置商品价格
	 * @param money 商品价格
	 */
	public void setMoney(int money) {
		this.money = money;
	}
	
	/**
	 * 取得商品积分
	 * @return 商品积分
	 */
	public int getScore() {
		return score;
	}
	
	/**
	 * 设置商品积分
	 * @param score
	 */
	public void setScore(int score) {
		this.score = score;
	}
	
	/**
	 * 取得商品数量
	 * @return 商品数量
	 */
	public int getNum() {
		return num;
	}
	
	/**
	 * 设置商品数量
	 * @param num 商品数量
	 */
	public void setNum(int num) {
		this.num = num;
	}
}
