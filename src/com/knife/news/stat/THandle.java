package com.knife.news.stat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
//日期与时间的处理类
public class THandle {
    /**
     * 获取日期date所在的月份的实际天数
     */
    public static int getMonthDays(Date date) {
        GregorianCalendar currtime = new GregorianCalendar();
        currtime.setTime(date);
        int year = currtime.get(Calendar.YEAR);
        boolean leap = currtime.isLeapYear(year);
        int month = currtime.get(GregorianCalendar.MONTH) + 1;
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12: {
                return 31;
            }
            case 2: {
                if (leap) {
                    return 29;
                }
                else {
                    return 28;
                }
            }
            case 4:
            case 6:
            case 9:
            case 11: {
                return 30;
            }
        }
        return 0;
    }

    /**
     * 获取日期date所在月份前n个月的第一天的字符串表示
     * 比如date是“2006-3-5”，n = 4
     * 那么就返回“2005-11-01”
     */
    public static String getStartDateofNMonth(Date date, int n) {
        GregorianCalendar currtime = new GregorianCalendar();
        currtime.setTime(date);
        currtime.add(GregorianCalendar.MONTH, -n);
        int year = currtime.get(Calendar.YEAR);
        int month = currtime.get(GregorianCalendar.MONTH) + 1;
        return year + "-" + month + "-01";
    }

    /**
     * 获取日期date所在月份前n个月的最后一天的字符串表示
     * 比如date是“2006-3-5”，n = 4
     * 那么就返回“2005-11-30”
     */
    public static String getEndDateofNMonth(Date date, int n) {
        GregorianCalendar currtime = new GregorianCalendar();
        currtime.setTime(date);
        currtime.add(GregorianCalendar.MONTH, -n);
        int year = currtime.get(Calendar.YEAR);
        int month = currtime.get(GregorianCalendar.MONTH) + 1;
        int days = getMonthDays(currtime.getTime());
        return year + "-" + month + "-" + days;
    }
    /**
     * 获取日期date所在月份前n个月的月份字符串表示
     * 比如date是“2006-3-5”，n = 4
     * 那么就返回“2005年11月”
     */
    public static String getNMonth(Date date, int n) {
        GregorianCalendar currtime = new GregorianCalendar();
        currtime.setTime(date);
        currtime.add(GregorianCalendar.MONTH, -n);
        int year = currtime.get(Calendar.YEAR);
        int month = currtime.get(GregorianCalendar.MONTH) + 1;
        return year + "年" + month + "月";
    }

    public static String getLastDate(Date date, int n){
        GregorianCalendar currtime = new GregorianCalendar();
        currtime.setTime(date);
        currtime.add(GregorianCalendar.DAY_OF_MONTH, -n);
        int year = currtime.get(Calendar.YEAR);
        int month = currtime.get(GregorianCalendar.MONTH) + 1;
        int days = currtime.get(GregorianCalendar.DAY_OF_MONTH);
        return year + "-" + month + "-" + days;
    }

    public static String getNextHour(int n){
        return n + ":00:00";
    }
}
