package com.knife.member;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Score entity. @author MyEclipse Persistence Tools
 */

public class Point implements java.io.Serializable {

	// Fields

	private Integer id;
	private String memguid;
	private String pointitem;
	private double point;
	private Timestamp effectdate;
	private Timestamp pointdate;
	private String pointtype;
	private double sjpoint;
	private Integer points;

	// Constructors

	/** default constructor */
	public Point() {
	}

	/** minimal constructor */
	public Point(String memguid) {
		this.memguid = memguid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMemguid() {
		return memguid;
	}

	public void setMemguid(String memguid) {
		this.memguid = memguid;
	}

	public String getPointitem() {
		return pointitem;
	}

	public void setPointitem(String pointitem) {
		this.pointitem = pointitem;
	}

	public double getPoint() {
		return point;
	}

	public void setPoint(double point) {
		this.point = point;
	}

	public Timestamp getEffectdate() {
		return effectdate;
	}

	public void setEffectdate(Timestamp effectdate) {
		this.effectdate = effectdate;
	}

	public Timestamp getPointdate() {
		return pointdate;
	}

	public void setPointdate(Timestamp pointdate) {
		this.pointdate = pointdate;
	}

	public String getPointtype() {
		return pointtype;
	}

	public void setPointtype(String pointtype) {
		this.pointtype = pointtype;
	}

	public double getSjpoint() {
		return sjpoint;
	}

	public void setSjpoint(double sjpoint) {
		this.sjpoint = sjpoint;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}
	
}