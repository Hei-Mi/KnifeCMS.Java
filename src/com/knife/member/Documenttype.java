package com.knife.member;

/**
 * Documenttype entity. @author MyEclipse Persistence Tools
 */

public class Documenttype implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1253246765595553668L;
	private Integer id;
	private String type;

	// Constructors

	/** default constructor */
	public Documenttype() {
	}

	/** full constructor */
	public Documenttype(String type) {
		this.type = type;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

}