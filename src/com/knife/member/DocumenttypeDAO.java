package com.knife.member;

import java.util.List;

import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Example;

/**
 * A data access object (DAO) providing persistence and search support for
 * Documenttype entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.knife.member.Documenttype
 * @author MyEclipse Persistence Tools
 */

public class DocumenttypeDAO extends BaseHibernateDAO {

	// property constants
	public static final String TYPE = "type";

	public void save(Documenttype transientInstance) {
		try {
			getSession().getTransaction().begin();
			getSession().save(transientInstance);
			getSession().getTransaction().commit();

		} catch (RuntimeException re) {

			throw re;
		}
	}

	public void delete(Documenttype persistentInstance) {

		try {
			getSession().getTransaction().begin();
			getSession().delete(persistentInstance);
			getSession().getTransaction().commit();

		} catch (RuntimeException re) {

			throw re;
		}
	}

	public Documenttype findById(java.lang.Integer id) {

		try {
			Documenttype instance = (Documenttype) getSession().get(
					"com.knife.member.Documenttype", id);
			return instance;
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public List findByExample(Documenttype instance) {

		try {
			List results = getSession().createCriteria("com.knife.member.Documenttype").add(
					Example.create(instance)).list();

			return results;
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {

		try {
			String queryString = "from Documenttype as model where model."
					+ propertyName + "= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public List findByType(Object type) {
		return findByProperty(TYPE, type);
	}

	public List findAll() {

		try {
			String queryString = "from Documenttype";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public int findAllSize() {

		return findAll().size();
	}

	public int getTotalPage(int size) {
		return (int) Math.ceil(findAllSize() / (size * 1.0));
	}

	public List findPagedAll(int currentPage, int pageSize) throws Exception {

		try {
			if (currentPage < 1) {
				return null;
			}
			String queryString = "from Documenttype";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setFirstResult((currentPage - 1) * pageSize);
			queryObject.setMaxResults(pageSize);
			return queryObject.list();
		} catch (Exception re) {

			throw re;
		}
	}

	public Documenttype merge(Documenttype detachedInstance) {

		try {
			Documenttype result = (Documenttype) getSession().merge(
					detachedInstance);

			return result;
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public void attachDirty(Documenttype instance) {

		try {
			getSession().saveOrUpdate(instance);

		} catch (RuntimeException re) {

			throw re;
		}
	}

	public void attachClean(Documenttype instance) {

		try {
			getSession().lock(instance, LockMode.NONE);

		} catch (RuntimeException re) {

			throw re;
		}
	}
}