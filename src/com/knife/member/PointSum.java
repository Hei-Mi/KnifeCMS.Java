package com.knife.member;

import java.io.Serializable;

public class PointSum implements Serializable{
	private long id ;
	private String pointtype;
	private double ljjlpoint;
	private double ljsjpoint;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPointtype() {
		return pointtype;
	}
	public void setPointtype(String pointtype) {
		this.pointtype = pointtype;
	}
	public double getLjjlpoint() {
		return ljjlpoint;
	}
	public void setLjjlpoint(double ljjlpoint) {
		this.ljjlpoint = ljjlpoint;
	}
	public double getLjsjpoint() {
		return ljsjpoint;
	}
	public void setLjsjpoint(double ljsjpoint) {
		this.ljsjpoint = ljsjpoint;
	}
	
}
