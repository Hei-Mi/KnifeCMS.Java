package com.knife.tools;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.knife.web.Globals;

public class WebXML {
	private File inputXml;
	private String path;
	private String name="web.xml";
	private Document document;

	public WebXML(){
		init();
	}
	
	public WebXML(String xmlpath) {
		path=xmlpath;
		init();
	}
	
	public WebXML(String xmlpath,String xmlname) {
		path=xmlpath;
		name=xmlname;
		init();
	}
	
	private void init(){
		if(path!=null){
			if(path.length()>0){
				inputXml =new File(path + File.separator+ "WEB-INF" + File.separator + name);
			}
		}else{
			if(Globals.APP_BASE_DIR!=null){
				if(Globals.APP_BASE_DIR.length()>0){
					inputXml =new File(Globals.APP_BASE_DIR + File.separator
						+ "WEB-INF" + File.separator + name);
				}else{
					inputXml =new File(WebXML.class.getClassLoader().getResource("/") + File.separator
							+ "WEB-INF" + File.separator + name);
				}
			}
		}
	}
	
	public String readParamValue(String paramName){
		String ret = "";
		try {
			SAXReader saxReader = new SAXReader();
			Document document = saxReader.read(inputXml);
			List list = document.selectNodes("//web-app/filter");
			for(Iterator iter = list.iterator();iter.hasNext();){
				Element filterElement = (Element) iter.next();
				List list1 = filterElement.selectNodes("//init-param");
				if(list1.size()>0){
					for(Iterator iter1 = list1.iterator();iter1.hasNext();){
						Element paramElement = (Element) iter1.next();
						Node pNameNode = paramElement.selectSingleNode("param-name");
						if(pNameNode!=null){
							if(paramName.equals(pNameNode.getText())){
								Node pValueNode = paramElement.selectSingleNode("param-value");
								ret = pValueNode.getText();
							}
						}
						//System.out.println(paramElement.getName() + ":" +paramElement.asXML() + "\n\n\n");
					}
				}
			}
		} catch (DocumentException e) {
			System.out.println(e.getMessage());
		}
		return ret;
	}

	public String read(String pname) {
		String ret = "";
		try {
			SAXReader saxReader = new SAXReader();
			Document document = saxReader.read(inputXml);
			List list = document.selectNodes("//filter-mapping");
			Iterator iter = list.iterator();
			while (iter.hasNext()) {
				Element paramElement = (Element) iter.next();
				Node titleNode = paramElement.selectSingleNode("filter-name");
				// Attribute attribute = (Attribute) iter.next();
				if (titleNode.getText().equals(pname)) {
					// attribute.setValue("Introductory");
					Node valueNode = paramElement
							.selectSingleNode("url-pattern");
					ret = valueNode.getText();
					// valueNode.setText(pvalue);
					break;
				}
			}
		} catch (DocumentException e) {
			System.out.println(e.getMessage());
		}
		return ret;
	}

	@SuppressWarnings("unchecked")
	public void modify(String pname, String pvalue) {
		try {
			List list = document.selectNodes("//init-param");
			Iterator iter = list.iterator();
			while (iter.hasNext()) {
				Element paramElement = (Element) iter.next();
				Node titleNode = paramElement.selectSingleNode("param-name");
				// Attribute attribute = (Attribute) iter.next();
				if (titleNode.getText().equals(pname)) {
					// attribute.setValue("Introductory");
					Node valueNode = paramElement
							.selectSingleNode("param-value");
					valueNode.setText(pvalue);
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public void saveSSO(String loginUrl, String serverName) {
		try {
			SAXReader saxReader = new SAXReader();
			document = saxReader.read(inputXml);
			modify("casServerLoginUrl", loginUrl);
			modify("serverName", serverName);
			XMLWriter output = new XMLWriter(new FileWriter(inputXml));
			output.write(document);
			output.close();
		} catch (DocumentException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] argv) {
		WebXML dom4jParser = new WebXML("E:\\Knife\\CMS\\KnifeCMS.Java\\KnifeCMS\\web","web_sso_cqrbank.xml");
		//dom4jParser.modifyDocument("","");
		System.out.println("Read value:"+dom4jParser.readParamValue("casServerUrlPrefix"));
	}
}
