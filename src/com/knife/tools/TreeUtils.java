package com.knife.tools;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import com.knife.tools.tree.xml.DataNodeRegister;
import com.knife.tools.tree.xml.TreeNode;
import com.knife.web.Globals;

public class TreeUtils {
	//protected static XmlNodeRegister register = XmlNodeRegister.getInstance();
	protected static DataNodeRegister register = DataNodeRegister.getInstance();
	
	//取得站点树
	public static String getSitesJson(){
		StringBuffer buf = new StringBuffer();
		boolean isParent=false;
		buf.append("[");
		List<TreeNode> lst = register.getSiteNodes();
		for(int i=0,count=lst.size();i<count;i++){
			if(i != 0){
				buf.append(",");
			}
			TreeNode node = lst.get(i);
			buf.append("{\"id\":\""+node.getSiteId()+"\"");
			buf.append(",\"name\":\"" + node.getText() + "\"");
			buf.append(",\"icon\":\"/include/manage/zTreeStyle/img/folder_Site.gif\"");
			buf.append(",\"isParent\":"+ isParent +"}");
			//buf.append(",isParent:"+ !node.isLeaf() +",parentId:'" + node.getParentId() + "'}");
		}
		buf.append("]");
		//System.out.println(buf.toString());
		return buf.toString();
	}
	
	public static String getSiteTreeJson(){
		return getSiteTreeJson("");
	}

	public static String getSiteTreeJson(String uid){
		StringBuffer buf = new StringBuffer();
		boolean isParent=false;
		buf.append("[");
		List<TreeNode> lst = register.getSiteNodes();
		for(int i=0,count=lst.size();i<count;i++){
			if(i != 0){
				buf.append(",");
			}
			TreeNode node = lst.get(i);
			buf.append("{\"id\":\""+node.getId()+"\"");
			buf.append(",\"name\":\"" + node.getText() + "\"");
			buf.append(",\"icon\":\"/include/manage/zTreeStyle/img/folder_Site.gif\"");
			if(node.isLeaf()){
				isParent=false;
			}else{
				isParent=true;
			}
			buf.append(",\"isParent\":"+ isParent);
			if(uid.length()>0){
				buf.append(",\"open\":"+true);
				buf.append(",\"nodes\":"+getSubTreeNodesJson(node.getSiteId(),node.getId(),"","",uid));
			}
			buf.append(",\"siteId\":\""+ node.getSiteId() +"\"}");
			//buf.append(",isParent:"+ !node.isLeaf() +",parentId:'" + node.getParentId() + "'}");
		}
		buf.append(",{\"id\":\"0\",\"name\":\"未归类\",\"icon\":\"/include/manage/zTreeStyle/img/folder_None.gif\",\"isParent\":false,\"siteId\":\"\"}");
		buf.append("]");
		//System.out.println(buf.toString());
		return buf.toString();
	}
	
	public static String getSubTreeNodesJson(String parentId){
		return getSubTreeNodesJson("1",parentId,"","","");
	}
	
	public static String getSubTreeNodesJson(String parentId,String userId){
		return getSubTreeNodesJson("1",parentId,userId,"","");
	}
	
	public static String getSubTreeNodesJson(String parentId,String userId,String tree_id){
		return getSubTreeNodesJson("1",parentId,userId,tree_id,"");
	}
	
	/**根据id返回对应的子分类字符串
	 * @param parentId
	 * @return
	 */
	public static String getSubTreeNodesJson(String siteId,String parentId,String userId,String tree_id,String uid){
		StringBuffer buf = new StringBuffer();
		boolean isParent=false;
		buf.append("[");
		List<TreeNode> lst = register.getChildNodes(siteId,parentId,userId,tree_id,uid);
		if(userId.length()>0 && lst.size()==0 && parentId.equals("0")){
			lst = register.getChildNodes(siteId,"",userId,tree_id,uid);
		}
		for(int i=0,count=lst.size();i<count;i++){
			if(i != 0){
				buf.append(",");
			}
			TreeNode node = lst.get(i);
			buf.append("{\"id\":\"" + node.getId() + "\"");
			buf.append(",\"name\":\"" + node.getText() + "\"");
			if(node.IsTree()){
				buf.append(",\"icon\":\"/include/manage/zTreeStyle/img/folder_Tree.gif\"");
			}
			if(node.isLeaf()){
				isParent=false;
			}else{
				isParent=true;
			}
			buf.append(",\"isParent\":"+ isParent);
			if(node.isCheck()){
				buf.append(",\"checked\":"+ node.isCheck());
			}
			if(node.isOpen()){
				buf.append(",\"open\":"+node.isOpen());
				buf.append(",\"nodes\":"+getSubTreeNodesJson(siteId,node.getId(),userId,tree_id,uid));
			}
			buf.append(",\"siteId\":\""+ node.getSiteId() +"\"}");
			//buf.append(",isParent:"+ !node.isLeaf() +",parentId:'" + node.getParentId() + "'}");
		}
		buf.append("]");
		//System.out.println(buf.toString());
		return buf.toString();
	}
	
	
	//jlm
	/**返回任务频道节点
	 * @param parentId
	 * @return
	 */
	public static String getSubTaskTreeNodesJson(String siteId,String parentId,String userId,String tree_id,String uid){
		StringBuffer buf = new StringBuffer();
		boolean isParent=false;
		buf.append("[");
		List<TreeNode> lst = register.getTaskNodes(siteId,parentId,userId,tree_id,uid);
		if(userId.length()>0 && lst.size()==0 && parentId.equals("0")){
			lst = register.getChildNodes(siteId,"",userId,tree_id,uid);
		}
		for(int i=0,count=lst.size();i<count;i++){
			if(i != 0){
				buf.append(",");
			}
			TreeNode node = lst.get(i);
			buf.append("{\"id\":\"" + node.getId() + "\"");
			buf.append(",\"name\":\"" + node.getText() + "\"");
			if(node.IsTree()){
				buf.append(",\"icon\":\"/include/manage/zTreeStyle/img/folder_Tree.gif\"");
			}
			if(node.isLeaf()){
				isParent=false;
			}else{
				isParent=false;
			}
			buf.append(",\"isParent\":"+ isParent);
			if(node.isCheck()){
				buf.append(",\"checked\":"+ node.isCheck());
			}
			if(node.isOpen()){
				buf.append(",\"open\":"+node.isOpen());
				buf.append(",\"nodes\":"+getSubTreeNodesJson(siteId,node.getId(),userId,tree_id,uid));
			}
			buf.append(",\"siteId\":\""+ node.getSiteId() +"\"}");
			//buf.append(",isParent:"+ !node.isLeaf() +",parentId:'" + node.getParentId() + "'}");
		}
		buf.append("]");
		//System.out.println(buf.toString());
		return buf.toString();
	}
	
	public static String getTemplatesJson(){
		return getTemplatesJson("","");
	}
	
	//返回所有的模板列表
	public static String getTemplatesJson(String sfilename,String filename){
		StringBuffer buf = new StringBuffer();
		String isParent="false";
		if(sfilename.length()>0){
			filename = sfilename.replace("_spt_", File.separator) + File.separator +filename;
		}
		String templateFile = Globals.APP_BASE_DIR + File.separator +"WEB-INF"+File.separator+"templates"+File.separator+"web"+File.separator;
		if(filename.length()>0){
			templateFile = templateFile + filename + File.separator;
		}
		File templatePath = new File(templateFile);
		buf.append("[");
		if(templatePath.exists()){
			int i=0;
			File[] tFiles = templatePath.listFiles();
			Arrays.sort(tFiles, new Comparator<File>(){
				public int compare(File f1, File f2){
					return f1.getName().compareTo(f2.getName());
				}
			});
			for(File afile:tFiles){
			if(i != 0){
				buf.append(",");
			}
			buf.append("{\"id\":\"" + afile.getName() + "\"");
			buf.append(",\"name\":\"" + afile.getName() + "\"");
			if(afile.isDirectory()){
				isParent="true";
			}else{
				isParent="false";
			}
			buf.append(",\"isParent\":"+isParent);
			buf.append(",\"siteId\":\""+ filename.replace(File.separator, "_spt_") +"\"}");
			i++;
		}
		}
		buf.append("]");
		//System.out.println(buf.toString());
		return buf.toString();
	}
	
	//返回所有html列表
	public static String getGTemplatesJson(String sfilename,String filename){
		StringBuffer buf = new StringBuffer();
		String isParent="false";
		if(sfilename.length()>0){
			filename = sfilename.replace("_spt_", File.separator) + File.separator +filename;
		}
		String templateFile = Globals.APP_BASE_DIR + File.separator +"WEB-INF"+File.separator+"templates"+File.separator+"htm"+File.separator;
		if(filename.length()>0){
			templateFile = templateFile + filename + File.separator;
		}
		File templatePath = new File(templateFile);
		buf.append("[");
		if(templatePath.exists()){
			int i=0;
			File[] tFiles = templatePath.listFiles();
			Arrays.sort(tFiles, new Comparator<File>(){
				public int compare(File f1, File f2){
					return f1.getName().compareTo(f2.getName());
				}
			});
			for(File afile:tFiles){
			if(i != 0){
				buf.append(",");
			}
			buf.append("{\"id\":\"" + afile.getName() + "\"");
			buf.append(",\"name\":\"" + afile.getName() + "\"");
			if(afile.isDirectory()){
				isParent="true";
			}else{
				isParent="false";
			}
			buf.append(",\"isParent\":"+isParent);
			buf.append(",\"siteId\":\""+ filename.replace(File.separator, "_spt_") +"\"}");
			i++;
		}
		}
		buf.append("]");
		//System.out.println(buf.toString());
		return buf.toString();
	}
	
	//返回树列表
	public static String getTreeJson(){
		StringBuffer buf = new StringBuffer();
		boolean isParent=false;
		buf.append("[");
		List<TreeNode> lst = register.getTrees();
		for(int i=0,count=lst.size();i<count;i++){
			if(i != 0){
				buf.append(",");
			}
			TreeNode node = lst.get(i);
			buf.append("{\"id\":\"" + node.getId() + "\"");
			buf.append(",\"name\":\"" + node.getText() + "\"");
			//buf.append(",\"icon\":\"\"");
			if(node.isLeaf()){
				isParent=false;
			}else{
				isParent=true;
			}
			buf.append(",\"isParent\":"+ isParent +"}");
			//buf.append(",isParent:"+ !node.isLeaf() +",parentId:'" + node.getParentId() + "'}");
		}
		buf.append("]");
		return buf.toString();
	}
	
	//返回会员列表
	public static String getUserTypeJson(int pid){
		StringBuffer buf = new StringBuffer();
		boolean isParent=false;
		buf.append("[");
		if(pid==0){
			buf.append("{\"id\":\"-1\",\"name\":\"新注册会员\",\"isParent\":false},{\"id\":\"0\",\"name\":\"免费会员\",\"isParent\":false}");
		}
		List<TreeNode> lst = register.getUserTypes(pid);
		for(int i=0,count=lst.size();i<count;i++){
			if(pid==0 || i>0){
				buf.append(",");
			}
			TreeNode node = lst.get(i);
			buf.append("{\"id\":\"" + node.getId() + "\"");
			buf.append(",\"name\":\"" + node.getText() + "\"");
			//buf.append(",\"icon\":\"\"");
			if(node.isLeaf()){
				isParent=false;
			}else{
				isParent=true;
			}
			buf.append(",\"isParent\":"+ isParent +"}");
			//buf.append(",isParent:"+ !node.isLeaf() +",parentId:'" + node.getParentId() + "'}");
		}
		buf.append("]");
		//System.out.println(buf.toString());
		return buf.toString();
	}
	
	//返回会员组织
	public static String getUserOrgJson(){
		StringBuffer buf = new StringBuffer();
		boolean isParent=false;
		buf.append("[{\"id\":\"-1\",\"name\":\"无组织\",\"isParent\":false},");
		List<TreeNode> lst = register.getUserOrgs();
		for(int i=0,count=lst.size();i<count;i++){
			if(i != 0){
				buf.append(",");
			}
			TreeNode node = lst.get(i);
			buf.append("{\"id\":\"" + node.getId() + "\"");
			buf.append(",\"name\":\"" + node.getText() + "\"");
			//buf.append(",\"icon\":\"\"");
			if(node.isLeaf()){
				isParent=false;
			}else{
				isParent=true;
			}
			buf.append(",\"isParent\":"+ isParent +"}");
			//buf.append(",isParent:"+ !node.isLeaf() +",parentId:'" + node.getParentId() + "'}");
		}
		buf.append("]");
		//System.out.println(buf.toString());
		return buf.toString();
	}
	
	//取得采集树
	public static String getGrabTreeJson(String userid){
		StringBuffer buf = new StringBuffer();
		boolean isParent=false;
		buf.append("[");
		List<TreeNode> lst = register.getGrabNodes();
		for(int i=0,count=lst.size();i<count;i++){
			if(i != 0){
				buf.append(",");
			}
			TreeNode node = lst.get(i);
			buf.append("{\"id\":\""+node.getSiteId()+"\"");
			buf.append(",\"name\":\"" + node.getText() + "\"");
			buf.append(",\"icon\":\"/include/manage/zTreeStyle/img/folder_Site.gif\"");
			buf.append(",\"isParent\":"+ isParent +"}");
			//buf.append(",isParent:"+ !node.isLeaf() +",parentId:'" + node.getParentId() + "'}");
		}
		buf.append("]");
		//System.out.println(buf.toString());
		return buf.toString();
	}
	
	//我的任务
	public static String getTaskTreeJson(String userid){
		
		return null;
	}
}
