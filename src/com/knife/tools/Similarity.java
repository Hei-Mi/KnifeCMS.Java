package com.knife.tools;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Similarity {
	/**
	 * 求相似度算法
	 * @param x
	 * @param y
	 * @return
	 */
	public static double likeOf(String x,String y){
		double result = 0;
		int count = 0;
		int xLength = x.length();
		int yLength = y.length();
		for(int i = 0 ; i < xLength ; i++){
			for(int j = 0 ; j < yLength ; j++){
				if(i == j && x.charAt(i)==y.charAt(j)){
					count++;
				}
			}
		}
		result = 2*count/(double)(xLength+yLength);
		return result;
	}
	
	/**
	 * 取最大值
	 */
	public static int maxValue(Map map){
		double maxValue = 0;
		Set<Map.Entry<Double, Integer>> mapSet = new HashSet<Map.Entry<Double, Integer>>();
		mapSet = map.entrySet();
		Iterator<Map.Entry<Double, Integer>> item = mapSet.iterator();
//		for(;item.hasNext();){
//			if(item.next().getKey()>maxValue){
//				maxValue = item.next().getKey();
//			}
//		}
		while(item.hasNext()){
			Entry<Double, Integer> obj = item.next();
			if(obj.getKey()>maxValue){
				maxValue = obj.getKey();
			}
		}
		int maxIntValue = (Integer) map.get(maxValue);
		return maxIntValue;
	}
}
