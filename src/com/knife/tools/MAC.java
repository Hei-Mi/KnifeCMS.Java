package com.knife.tools;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import license.key.CMyString;

public class MAC {
	public MAC() {
	}

	public static boolean isValidMac(long _lMac) {
		if (_lMac == Long.parseLong("123456789096")) {
			return true;
		}
		if (getMacAsLong() == _lMac) {
			return true;
		} else {
			return false;
		}
	}

	public static long getMacAsLong() {
		String sMac = "";
		try {
			sMac = getMACAddress();
		} catch (Exception e) {
		}
		return getMyMacAsLong(sMac);
	}

	public static long getMyMacAsLong(String _sMac) {
		if (_sMac == null || _sMac.length() <= 0)
			return 0L;
		String sMac = _sMac;
		sMac = CMyString.replaceStr(sMac, "-", "");
		long nMac = Long.parseLong(sMac, 16);
		sMac = nMac + "";
		if (sMac.length() > 12) {
			for (sMac = sMac.substring(0, 12); sMac.charAt(0) == '0'
					&& sMac.length() > 0; sMac = sMac.substring(1,
					sMac.length()))
				;
			nMac = Long.parseLong(sMac);
		}
		return nMac;
	}

	private static String getMACAddress() throws Exception {
		return getMACAddress(InetAddress.getLocalHost());
	}

	// 获取MAC地址的方法
	private static String getMACAddress(InetAddress ia) throws Exception {
		// 获得网络接口对象（即网卡），并得到mac地址，mac地址存在于一个byte数组中。
		byte[] mac = NetworkInterface.getByInetAddress(ia).getHardwareAddress();
		// 下面代码是把mac地址拼装成String
		StringBuffer sb = new StringBuffer();
		if (mac != null) {
			for (int i = 0; i < mac.length; i++) {
				if (i != 0) {
					sb.append("-");
				}
				// mac[i] & 0xFF 是为了把byte转化为正整数
				String s = Integer.toHexString(mac[i] & 0xFF);
				sb.append(s.length() == 1 ? 0 + s : s);
			}
		} else {

		}

		// 把字符串所有小写字母改为大写成为正规的mac地址并返回
		return sb.toString().toUpperCase();
	}

	public static String hexByte(byte b) {
		String s = "000000" + Integer.toHexString(b);
		return s.substring(s.length() - 2);
	}

	public static String getMAC() {
		Enumeration<NetworkInterface> el;
		String mac_s = "";
		try {
			el = NetworkInterface.getNetworkInterfaces();
			while (el.hasMoreElements()) {
				byte[] mac = el.nextElement().getHardwareAddress();
				if (mac == null){
					continue;
				}			
				if (mac != null && mac.length>=6){ 
                    mac_s = hexByte(mac[0]) + "-" + hexByte(mac[1]) + "-" 
                    + hexByte(mac[2]) + "-" + hexByte(mac[3]) + "-" 
                    + hexByte(mac[4]) + "-" + hexByte(mac[5]); 
                    //System.out.println("MAC地址:"+mac_s);
                    break;
                }

			}
		} catch (SocketException e1) {
			e1.printStackTrace();
		}
		return mac_s.toUpperCase();
	}

	public static void main(String args[]) {
		try {
			System.out.println(getMAC());
			//System.out.println(getMACAddress(InetAddress.getLocalHost()));
			//System.out.println(getMyMacAsLong(0));
		} catch (Exception e) {
			System.out.println("设备获取异常:未发现网络设备");
			e.printStackTrace();
		}
	}
}