package com.knife.tools;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.knife.news.logic.PicService;
import com.knife.news.logic.SiteService;
import com.knife.news.logic.impl.PicServiceImpl;
import com.knife.news.logic.impl.SiteServiceImpl;
import com.knife.news.object.Pic;
import com.knife.news.object.Site;
import com.knife.util.CommUtil;
import com.knife.web.Globals;

public class FlexUpload extends HttpServlet {
	private static final long serialVersionUID = -7825355637448948879L;
	private PicService picDAO=PicServiceImpl.getInstance();
	private SiteService siteDAO = new SiteServiceImpl();

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		String sid = CommUtil.null2String(request.getParameter("sid"));
		Site site = new Site();
		if(sid.length()>0){
			site = siteDAO.getSiteById(sid);
		}else{
			site = siteDAO.getSite();
		}
		String uploadtype = "";
		if(request.getParameter("type")!=null){
			uploadtype = request.getParameter("type");
		}
		String picurl = request.getParameter("path");
		picurl = picurl.substring(picurl.indexOf("/upload"),picurl.length());
		String newurl = picurl.substring(0, picurl.lastIndexOf("."))+".png";
		String filePath = Globals.APP_BASE_DIR + newurl;
		try{
	        BufferedInputStream inputStream = new BufferedInputStream(request.getInputStream());
	        File pngFile = new File(filePath);
	        FileOutputStream outputStream = new FileOutputStream(pngFile);
	        byte [] bytes = new byte[1024];
	        int v;
	        while((v=inputStream.read(bytes))>0){
	        	outputStream.write(bytes,0,v);
	        }
	        outputStream.flush();
	        outputStream.close();
	        inputStream.close();
	        
	        String pubPath = pngFile.getAbsolutePath().replace("html", "wwwroot");
	        if(site.getHtml()==1){
				CopyDirectory.copyFile(pngFile, new File(pubPath));
				if(site.getBig5()==1){
					String big5Path = filePath.replace(site.getPub_dir() + File.separator + "upload", site.getPub_dir() + File.separator + "big5"+File.separator+"upload");
					CopyDirectory.copyFile(pngFile, new File(big5Path));
					String big5PubPath = pubPath.replace(site.getPub_dir() + File.separator + "upload", site.getPub_dir() + File.separator + "big5"+File.separator+"upload");
					CopyDirectory.copyFile(pngFile, new File(big5PubPath));
				}
			}
	        
	        if(uploadtype.length()>0){
	        	List<Pic> pics=picDAO.getPicsByUrl(picurl);
	        	for(Pic apic: pics){
	        		apic.setUrl(newurl);
	        		picDAO.updatePic(apic);
	        	}
	        }
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
