package com.knife.tools.tree.xml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import cn.com.cjf.entity.Char;

import com.knife.member.Organization;
import com.knife.member.OrganizationDAO;
import com.knife.member.Usertype;
import com.knife.member.UsertypeDAO;
import com.knife.news.logic.GrabService;
import com.knife.news.logic.SiteService;
import com.knife.news.logic.TreeService;
import com.knife.news.logic.TypeService;
import com.knife.news.logic.UserService;
import com.knife.news.logic.impl.FlowServiceImpl;
import com.knife.news.logic.impl.GrabServiceImpl;
import com.knife.news.logic.impl.NewsServiceImpl;
import com.knife.news.logic.impl.SiteServiceImpl;
import com.knife.news.logic.impl.TreeServiceImpl;
import com.knife.news.logic.impl.TypeServiceImpl;
import com.knife.news.logic.impl.UserServiceImpl;
import com.knife.news.model.Tree;
import com.knife.news.object.Flow;
import com.knife.news.object.Grab;
import com.knife.news.object.News;
import com.knife.news.object.Site;
import com.knife.news.object.Type;
import com.knife.news.object.User;
import com.knife.util.CommUtil;

public class DataNodeRegister {
	/**
	 * 存放所有的node
	 */
	private List<TreeNode> lst = null;
	private SiteService siteDAO = SiteServiceImpl.getInstance();
	private TypeServiceImpl typeDAO = TypeServiceImpl.getInstance();
	private TreeService treeDAO = TreeServiceImpl.getInstance();
	private UserService userDAO = UserServiceImpl.getInstance();
	private UsertypeDAO utDAO = new UsertypeDAO();
	private OrganizationDAO orgDAO = new OrganizationDAO();
	private GrabService grabDAO = GrabServiceImpl.getInstance();
	private FlowServiceImpl flowDao = FlowServiceImpl.getInstance();
	private NewsServiceImpl newsDao = NewsServiceImpl.getInstance();		
	
	private DataNodeRegister(){}
	
	private static final DataNodeRegister instance = new DataNodeRegister();
	
	public static final DataNodeRegister getInstance(){
		return instance;
	}
	
	@SuppressWarnings("unchecked")
	public List<TreeNode> getUserTypes(int pid){
		List<TreeNode> result = new ArrayList<TreeNode>();
		List<Usertype> types = utDAO.findByParent(pid);
		if(types != null){
			for(Usertype e : types){
				result.add(userType2Node(e));
			}
			Collections.sort(result);
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<TreeNode> getUserOrgs(){
		List<TreeNode> result = new ArrayList<TreeNode>();
		List<Organization> types = orgDAO.findAll();
		if(types != null){
			for(Organization e : types){
				result.add(userOrg2Node(e));
			}
			Collections.sort(result);
		}
		return result;
	}
	
	public List<TreeNode> getTrees(){
		List<TreeNode> result = new ArrayList<TreeNode>();
		List<Tree> trees = treeDAO.getAllTree(1);
		if(trees != null){
			for(Tree e : trees){
				result.add(tree2Node(e));
			}
			Collections.sort(result);
		}
		return result;	
	}
	
	/**返回配置文件的所有节点
	 * @return
	 */
	public List<TreeNode> getNodes(){
		lst = new ArrayList<TreeNode>();
		initAllElements();
		return this.lst;
	}
	
	/**返回有权限的所有站点节点
	 * @return
	 */
	public List<TreeNode> getSiteNodes(){
		List<TreeNode> result = new ArrayList<TreeNode>();
		List<Site> sites=siteDAO.getAllSites();
		if(sites != null){
			for(Site e : sites){
				result.add(site2Node(e));
			}
			Collections.sort(result);
		}
		return result;
	}
	
	/**
	 * 返回有权限的采集节点
	 * @return
	 */
	public List<TreeNode> getGrabNodes(){
		List<TreeNode> result = new ArrayList<TreeNode>();
		List<Grab> grabs=grabDAO.getGrabs();
		if(grabs != null){
			for(Grab e : grabs){
				result.add(grab2Node(e));
			}
			Collections.sort(result);
		}
		return result;
	}
	
	public List<TreeNode> getChildNodes(String parentId){
		return getChildNodes(parentId,"","");
	}
	
	public List<TreeNode> getChildNodes(String parentId,String userid,String tree_id){
		List<TreeNode> result = new ArrayList<TreeNode>();
		List<Type> types=typeDAO.getSubTypesById(parentId,-1,userid,tree_id);
		if(types != null){
			for(Type e : types){
				result.add(data2Node(e));
			}
			Collections.sort(result);
		}
		return result;
	}
	
	/**返回有权限的所有分类节点
	 * @return
	 */
	public List<TreeNode> getChildNodes(String siteId,String parentId,String userid,String tree_id,String uid){
		List<TreeNode> result = new ArrayList<TreeNode>();
		//System.out.println("获取到的父频道为:"+parentId);
		List<Type> types=typeDAO.getSubTypesById(siteId,parentId,-1,userid,tree_id);
		if(types != null){
			for(Type e : types){
				boolean ischild=false;
				if(userid.length()>0){
					for(Type b:types){
						if(!b.getId().equals(e.getId())){
							String ids=typeDAO.getAllSubTypesId(b.getId(), -1);
							
							if(ids.indexOf(e.getId())>=0){
								ischild=true;
								break;
							}
						}
					}
				}
				//System.out.println("bbbb:"+e.getName());
				if(!ischild){
					result.add(data2Node(e,uid));
				}
			}
			Collections.sort(result);
		}
		return result;
	}
	
	//jlm
	public List<TreeNode> getTaskNodes(String siteId,String parentId,String userid,String tree_id,String uid){
		List<TreeNode> result = new ArrayList<TreeNode>();
		//System.out.println("获取到的父频道为:"+parentId);
		List<Type> types = typeDAO.getTaskTypesById(siteId, parentId, -1,userid, tree_id);// 得到有权限的频道
		if (types != null) {
			//List<Type> subll =typeDAO.getAllTypes(siteId);
			for (Type e : types) {
				boolean ischild = false;
				try {
					for (Type b : types) {
						if (!b.getId().equals(e.getId())) {
							String ids = typeDAO.getAllSubTypesId(
									b.getId(), -1);
							if (ids.indexOf(e.getId()) >= 0) {
//								ischild = true;
								break;
							}
						}
					}
				} catch (Exception ee) {
					ee.getStackTrace();
				}
				if (!ischild) {
					result.add(Taskdata2Node(e, uid));
//					result.add(data2Node(e));
				}
			}
			//Collections.sort(result);
		}
		return result;
	}
	
	/**
	 * 读取所有的node
	 */
	private void initAllElements(){
		List<Type> types=typeDAO.getSubTypesById("0");
		if(types != null){
			for(Type e : types){
				this.lst.add(data2Node(e));
			}
			Collections.sort(this.lst);
		}
	}
	
	/**将会员类型转换成 node
	 * @param e
	 * @return
	 */
	private TreeNode userType2Node(Usertype t){
		TreeNode node = new TreeNode();
		node.setId(nvl(t.getId()));
		node.setText(nvl(t.getName()));
		node.setParentId(t.getParent().toString());
		node.setLeaf(t.isLeaf());
		return node;
	}
	
	/**将会员组织转换成 node
	 * @param e
	 * @return
	 */
	private TreeNode userOrg2Node(Organization t){
		TreeNode node = new TreeNode();
		node.setId(nvl(t.getId()));
		node.setText(nvl(t.getType()));
		node.setParentId(null);
		node.setLeaf(true);
		return node;
	}
	
	/**将Tree 转换成 node
	 * @param e
	 * @return
	 */
	private TreeNode tree2Node(Tree t){
		TreeNode node = new TreeNode();
		node.setId(nvl(t.getId()));
		node.setText(nvl(t.getName()));
		node.setParentId(null);
		node.setLeaf(true);
		return node;
	}
	
	/**将Site转换成 node
	 * @param e
	 * @return
	 */
	private TreeNode site2Node(com.knife.news.object.Site s){
		TreeNode node = new TreeNode();
		node.setId("0");
		node.setText(nvl(s.getName()));
		node.setParentId("0");
		node.setLeaf(false);
		try{
			node.setIndex(s.getOrder());
		}catch(Exception ex){
			ex.printStackTrace();
			node.setIndex(0);
		}
		node.setTree(false);
		node.setSiteId(nvl(s.getId()));
		return node;
	}
	
	/**将Grab转换成 node
	 * @param e
	 * @return
	 */
	private TreeNode grab2Node(com.knife.news.object.Grab s){
		TreeNode node = new TreeNode();
		node.setId("0");
		node.setText(nvl(s.getTitle()));
		node.setParentId("0");
		node.setLeaf(false);
		node.setTree(false);
		node.setSiteId(nvl(s.getId()));
		return node;
	}
	
	private TreeNode data2Node(Type t){
		return data2Node(t,"");
	}
	
	/**将element 转换成 node
	 * @param e
	 * @return
	 */
	private TreeNode data2Node(Type t, String uid) {
		TreeNode node = new TreeNode();
		node.setId(nvl(t.getId()));
		node.setText(nvl(t.getName()));
		node.setParentId(nvl(t.getParent()));
		node.setLeaf(t.isLeave());
		node.setCheck(false);
		node.setOpen(false);
		if(uid.length()>0){
			try{
			User suser=userDAO.getUserById(uid);
			if(suser!=null){
				List<Type> utypes=suser.getTypes();
				for(Type atype:utypes){
					if(atype.getId().equals(t.getId())){
						node.setCheck(true);
					}else{
						if(isOpen(t,atype.getId())){
							node.setOpen(true);
						}
					}
				}
			}
			}catch(Exception ex){
				ex.printStackTrace();
				node.setCheck(false);
			}
		}
		try{
			node.setIndex(t.getOrder());
		}catch(Exception ex){
			ex.printStackTrace();
			node.setIndex(0);
		}
		if(CommUtil.null2String(t.getTree()).length()>0){
			node.setTree(true);
		}else{
			node.setTree(false);
		}
		node.setSiteId(nvl(t.getSite()));
		return node;
	}
	
	//jlm
	private TreeNode Taskdata2Node(Type t, String uid) {
		TreeNode node = new TreeNode();
		node.setId(nvl(t.getId()));
		node.setText(nvl(t.getName()));
		node.setParentId(nvl(t.getParent()));
		node.setLeaf(t.isLeave());
		node.setCheck(false);
		node.setOpen(false);
		
		if (1==1 ||uid.length() > 0) {
			try {
				String sql="k_display=0 and k_type="+t.getId();
				List<News> notsum=newsDao.getNews(sql);
				if(notsum.size()>0){
					node.setText(nvl(t.getName())+"<"+notsum.size()+" 篇未审核>");
				}
					
				User suser = userDAO.getUserById(uid);
							node.setCheck(true);
							
						
			} catch (Exception ex) {
				ex.printStackTrace();
				node.setCheck(false);
			}
		}
		try {
	node.setIndex(t.getOrder());
		}catch(Exception ex){
			ex.printStackTrace();
			node.setIndex(0);
		}
		if(CommUtil.null2String(t.getTree()).length()>0){
			node.setTree(true);
		}else{
			node.setTree(false);
		}
		node.setSiteId(nvl(t.getSite()));
		return node;
	}
	
	private boolean isOpen(Type subt,String tid){
		if(subt!=null){
			if(subt.getId().equals(tid)){
				return true;
			}else{
				if(subt.getSubTypes(-1).size()>0){
					for(Type btype:subt.getSubTypes(-1)){
						if(btype.getId().equals(tid)){
							return true;
						}else{
							return isOpen(btype,tid);
						}
					}
				}
			}
		}
		return false;
	}
	
	private String nvl(Object obj){
		if(obj == null){
			return "";
		}else{
			return obj.toString().trim();
		}
	}
}
