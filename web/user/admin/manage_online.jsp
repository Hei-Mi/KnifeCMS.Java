<%@ page language="java" import="java.util.*,com.knife.member.*,com.knife.tools.SessionUtil" pageEncoding="UTF-8"%>
<%@include file="/include/manage/checkUser.jsp" %>
<%
int isfee=0;
if(request.getParameter("isfee")!=null){
	isfee=Integer.parseInt(request.getParameter("isfee"));
}
int page_now=1;
int page_prev=1;
int page_next=1;
int total_page=1;
int page_range_start=0;
if(request.getParameter("page")!=null){
	page_now=Integer.parseInt(request.getParameter("page"));
	page_range_start=(int)(page_now/5);
	page_next=page_now;
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>在线会员</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	border:1px solid gray;
	width:100%;
	font-size:12px;
}
.data_list th{
	height:32px;
	border-bottom:1px solid gray;
}
.data_list td{
	height:18px;
	text-align:center;
	border-bottom:1px solid gray;
}
</style>
	<link rel="stylesheet" href="/user/skin/js/validationEngine.jquery.css" type="text/css"></link>
	<script type="text/javascript" src="/user/skin/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine-cn.js"></script>
	<script language="javascript">
	$(function(){
		$("#regform").validationEngine();
	});
	
	function checkForm(){
		$("#regform")[0].submit();
	}
	
	function resetForm(){
		$("#regform")[0].reset();
	}
	</script>
  </head>
  
<body>
	<table class="data_list" cellspacing="0" cellpadding="0">
		<tr>
					<th>编号</th>
					<th>会 员 名</th>
					<th>邮件地址</th>
					<th>部门</th>
					<th>操作</th>
				</tr>
				<%
				UserinfoDAO userDAO=new UserinfoDAO();
				List users=SessionUtil.getUserList();
				for(Object aobj:users){
					String ausername = (String)aobj;
					Userinfo auser = new Userinfo();
					List finds = new ArrayList();
					finds = userDAO.findByAcount(ausername);
					if(finds.size()>0){
						auser=(Userinfo)finds.get(0);
					}
					if(auser!=null){
				%>
				<tr>
					<td><%=auser.getId()%></td>
					<td><%=auser.getAcount()%></td>
					<td><%=auser.getEmail()%></td>
					<td><%=auser.getDepartment()%></td>
					<td><%
					if(!"admin".equals(auser.getAcount())){
					%><a href="opt/kickuser.jsp?acount=<%=auser.getAcount()%>">踢出</a>
					<%}%>
					</td>
				</tr>
				<%}
				}%>
			</table>
</body>
</html>