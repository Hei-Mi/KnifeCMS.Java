<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%@include file="checkadmin.jsp" %>
<%
int page_now=1;
int page_prev=1;
int page_next=1;
int total_page=1;
int page_range_start=0;
if(request.getParameter("page")!=null){
	page_now=Integer.parseInt(request.getParameter("page"));
	page_range_start=(int)(page_now/5);
	page_next=page_now;
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>会议列表</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
	<meta http-equiv="description" content="This is my page"/>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	border:1px solid gray;
	width:100%;
	font-size:12px;
}
.data_list th{
	height:32px;
	border-bottom:1px solid gray;
}
.data_list td{
	height:18px;
	text-align:center;
	border-bottom:1px solid gray;
}
</style>
	<link rel="stylesheet" href="/user/skin/js/validationEngine.jquery.css" type="text/css"></link>
	<script type="text/javascript" src="/user/skin/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine-cn.js"></script>
  </head>
  
<body>
	<table class="data_list" cellspacing="0" cellpadding="0">
		<tr>
					<th>编号</th>
					<th>会议名称</th>
					<th>主持人</th>
					<th>开始时间</th>
					<th>结束时间</th>
					<th>操作</th>
				</tr>
				<%
				MeetingDAO meetingDAO=new MeetingDAO();
				List meetings=meetingDAO.findPagedAll(page_now,20);
				total_page = meetingDAO.getTotalPage(20);
				if(page_now>1)page_prev=page_now-1;
				if(page_now<total_page)page_next=page_now+1;
				for(Object aobj:meetings){
					Meeting ameeting=(Meeting)aobj;
				%>
				<tr>
					<td><%=ameeting.getId()%></td>
					<td><%=ameeting.getTitle()%></td>
					<td><%=ameeting.getHost()%></td>
					<td><%=ameeting.getStarttime()%></td>
					<td><%=ameeting.getEndtime()%></td>
					<td>
						<a href="meetingedit.jsp?id=<%=ameeting.getId()%>">编辑</a> | <a href="/user/opt/delmeeting.jsp?id=<%=ameeting.getId()%>">删除</a>
					</td>
				</tr>
				<%}%>
				<tr>
				<td colspan="6" style="height:32px" align="center">
					<a href="?page=1"><img src="/user/skin/images/page_first.gif" border="0" /></a>
					<a href="?page=<%=page_prev%>"><img src="/user/skin/images/page_prev.gif" border="0" /></a>
					<%for(int i=1+(page_range_start*5);i<5+(page_range_start*5);i++){
						if(i>total_page) break;
					%>
					<a href="?page=<%=i%>"><%=i%></a>
					<%
					}
					%>
					<a href="?page=<%=page_next%>"><img src="/user/skin/images/page_next.gif" border="0" /></a>
					<a href="?page=<%=total_page%>"><img src="/user/skin/images/page_last.gif" border="0" /></a>
				</td>
			</tr>
			</table>
</body>
</html>