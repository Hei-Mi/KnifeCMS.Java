<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%
request.setCharacterEncoding("UTF-8");
String msg="";
String parameter="";
if(request.getParameter("parameter")!=null){
	parameter=request.getParameter("parameter");
}
int id=0;
if(request.getParameter("id")!=null){
	id = Integer.parseInt(request.getParameter("id"));
}
String name = "";
if(request.getParameter("name")!=null){
	name = request.getParameter("name");
}
int rights = 0;
if(request.getParameter("rights")!=null){
	try{
		rights = Integer.parseInt(request.getParameter("rights"));
	}catch(Exception e){}
}
int parent = 0;
if(request.getParameter("parent")!=null){
	try{
		parent = Integer.parseInt(request.getParameter("parent"));
	}catch(Exception e){}
}
int onscore = 0;
if(request.getParameter("onscore")!=null){
	try{
		onscore = Integer.parseInt(request.getParameter("onscore"));
	}catch(Exception e){}
}
int macnum = 0;
if(request.getParameter("macnum")!=null){
	try{
		macnum = Integer.parseInt(request.getParameter("macnum"));
	}catch(Exception e){}
}
int order = 0;
if(request.getParameter("order")!=null){
	try{
		order = Integer.parseInt(request.getParameter("order"));
	}catch(Exception e){}
}

Usertype adoct = new Usertype();
UsertypeDAO doctDAO = new UsertypeDAO();
if(id>0){
	adoct=doctDAO.findById(id);
}
if(parameter.equals("delete")){
	doctDAO.delete(adoct);
	msg="删除成功！";
}else{
	adoct.setName(name);
	adoct.setRights(rights);
	adoct.setParent(parent);
	adoct.setOnscore(onscore);
	adoct.setMacnum(macnum);
	adoct.setOrder(order);
	try{
		doctDAO.save(adoct);
		msg="编辑成功！";
	}catch(Exception e){
		e.printStackTrace();
		msg="编辑失败！";
	}
}
%>
<script language="javascript">
alert("<%=msg%>");
history.back(-1);
</script>