<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%@include file="checkadmin.jsp" %>
<%
int	id = 0;
if(request.getParameter("id")!=null){
	id				= Integer.parseInt(request.getParameter("id"));
}
	RisktypeDAO riskDAO=new RisktypeDAO();
	Risktype arisk = riskDAO.findById(id);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>编辑风险类别</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
	<meta http-equiv="description" content="This is my page"/>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	border:1px solid gray;
	width:100%;
	font-size:12px;
}
.data_list th{
	height:32px;
	border-bottom:1px solid gray;
}
.data_list td{
	height:24px;
	text-indent:24px;
	border-bottom:1px solid gray;
}
.inputText input{border:1px solid gray;}
</style>
	<link rel="stylesheet" href="skin/js/validationEngine.jquery.css" type="text/css"></link>
	<script type="text/javascript" src="skin/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="skin/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="skin/js/jquery.validationEngine-cn.js"></script>
	<script language="javascript">
	$(function(){
		$("#regform").validationEngine();
	});
	
	function checkForm(){
		$("#regform")[0].submit();
	}
	
	function resetForm(){
		$("#regform")[0].reset();
	}
	</script>
  </head>
  
<body>
<form id="regform" name="regform" action="/user/opt/editrisk.jsp" method="post">
<input type="hidden" name="id" value="<%=arisk.getId()%>" />
	<table class="data_list" cellspacing="0" cellpadding="0">
		<tr>
			<th colspan="2">编辑风险类别</th>
		</tr>
		<tr>
					<td width="280">类别名称：</td>
					<td class="inputText">
						<input id="type" type="text" name="type" class="validate[required,length[0,20]]" value="<%=arisk.getType()%>" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" value="提交" />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="reset" value="重置" />
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
			</table>
	</form>
</body>
</html>