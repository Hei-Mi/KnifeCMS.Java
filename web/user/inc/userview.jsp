<%@ page language="java"
	import="java.util.*,com.knife.member.*,com.knife.member.stat.*,java.text.SimpleDateFormat,com.knife.news.logic.NewsService,com.knife.news.logic.impl.NewsServiceImpl,com.knife.news.object.News"
	pageEncoding="UTF-8"%>
<%@include file="checkuser.jsp"%>
<%
int uid=0;
if(request.getParameter("uid")!=null){
	uid			= Integer.parseInt(request.getParameter("uid"));
}
	Userinfo loginUser=null;
	if(uid>0){
		loginUser=userDAO.findById((long)uid);
	}

	int page_now = 1;
	int page_prev = 1;
	int page_next = 1;
	int total_page = 1;
	int page_range_start = 0;
	int begin = 1;
	if (request.getParameter("page") != null) {
		page_now = Integer.parseInt(request.getParameter("page"));
		page_range_start = (int) page_now / 5;
		page_next = page_now;
		begin = (page_now - 1) * 20 + 1;
	}
	if (page_now < 1) {
		page_now = 1;
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>学习记录</title>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	width:806px;
	font-size:12px;
	border-left:1px solid #BCC3CD;
}
.data_list th{
	height:30px;
	border-bottom:1px solid gray;
	color:white;
	background:url('/user/skin/images/document/title_bg.jpg') bottom repeat-x;
}
.data_list th div.title{
	position:relative;
	height:25px;
	_height:1%;
	line-height:25px;
}
.data_list td{
	height:19px;
	text-align:center;
	border-bottom:1px solid #BCC3CD;
	border-right:1px solid #BCC3CD;
}
.datatype{
	display:none;
	position:absolute;
	top:20px;
	right:0px;
	width:90px;
	line-height:150%!important;
	height:auto!important;
	_height:220px;
	max-height:220px;
	overflow-y:auto;
	border:1px solid gray;
	background:white;
	z-index:100;
}
.datatype ul{list-style-type:none;margin-left:0px;padding:0}
.datatype ul li{list-style-type:none;margin-left:0px;padding:0;text-align:center;}
.datatype ul li a{font-weight:normal}
</style>
<script type="text/javascript" src="/user/skin/js/jquery-1.6.4.min.js"></script>
<script language="javascript">
function showbg(obj){
	obj.style.backgroundImage="url('/user/skin/images/document/select_bg.jpg')";
}

function hidebg(obj){
	obj.style.backgroundImage="";
}
</script>
	</head>
	<body>
		<table class="data_list" cellspacing="0" cellpadding="0">
			<tr>
				<th width="29%">
					资料名称
				</th>
				<!-- th width="14%">
					来源
				</th -->
				<th width="10%">
					作者
				</th>
				<th width="11%">
					风险类别
				</th>
				<th width="11%">
					资料类别
				</th>
				<th width="11%">
					学习日期
				</th>
				<th width="11%">
					起始时间
				</th>
				<th width="11%">
					结束时间
				</th>
				<th width="6%">
					类型
				</th>
			</tr>
			<%
				List<UserView> vlist1 = StatHandle.getViewList(loginUser.getEmail(),0);
				total_page = (int) Math
						.ceil((float) (vlist1.size()) / (float) (20));
				if (page_now > 1)
					page_prev = page_now - 1;
				if (page_now < total_page)
					page_next = page_now + 1;

				NewsService newsDAO = new NewsServiceImpl();
				List<News> docs = new ArrayList<News>();
				List<UserView> vlist = StatHandle
						.getViewList(loginUser.getEmail(),0, page_now);
				for (int i=0;i<vlist.size();i++) {
					UserView uv = (UserView) vlist.get(i);
					try {
						String fileurl = uv.getUrl();
						Collection<Object> paras = new ArrayList<Object>();
						paras.add(fileurl);
						docs = newsDAO.getNewsBySql("id!='' and k_docfile=?",paras, 0, 1);
					} catch (Exception e) {
						e.printStackTrace();
					}
					String id="";
					String title ="<font color=gray>已删除</font>";
						String source = "&nbsp;";
						String author = "&nbsp;";
						String riskName = "&nbsp;";
						String doctName = "&nbsp;";
						String fileType = "&nbsp;";
						String applyDate = "&nbsp;";
						String startTime = "&nbsp;";
						String endTime = "&nbsp;";
						News adoc=null;
					if (docs.size() > 0) {
						adoc = (News) docs.get(0);
						id = adoc.getId();
						title = adoc.getTitle(21);
						SimpleDateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MM-dd");
						try {
							applyDate = dateFormat.format(uv.getAccessdate());
							startTime = uv.getAccesstime().toString();
							endTime = uv.getEndtime().toString();
						} catch (Exception e) {
						}
						if (adoc.getType2() != null) {
							if (adoc.getType2().length() > 0) {
								riskName = adoc.getType2();
							}
						}
						if(riskName.indexOf(",")>0){
							riskName=riskName.substring(0,riskName.indexOf(","));
						}
						if (adoc.getType() != null) {
							try {
								doctName = adoc.getTypeName();
							} catch (Exception e) {
							}
						}
						if (doctName.length() > 7) {
							doctName = doctName.substring(0, 6) + "..";
						}
						if (adoc.getSource() != null) {
							if (adoc.getSource().length() > 0) {
								source = adoc.getSource();
								if (source.length() > 7) {
									source = source.substring(0, 6) + "..";
								}
							}
						}
						if (adoc.getAuthor() != null) {
							if (adoc.getAuthor().length() > 0) {
								author = adoc.getAuthor();
								if (author.length() > 7) {
									author = author.substring(0, 6) + "..";
								}
							}
						}
						if (adoc.getDoctype() != null) {
							if (adoc.getDoctype().length() > 0) {
								fileType = adoc.getDoctype();
							}
						}
					}
						if (fileType.equals(".doc") || fileType.equals(".docx")
								|| fileType.equals(".xls")
								|| fileType.equals(".xlsx")
								|| fileType.equals(".txt")
								|| fileType.equals(".pdf")
								|| fileType.equals(".swf")) {
							fileType = "文本";
						} else if (fileType.equals(".ppt")
								|| fileType.equals(".pptx")) {
							fileType = "PPT";
						} else if (fileType.equals(".mpg")
								|| fileType.equals(".wmv")
								|| fileType.equals(".3gp")
								|| fileType.equals(".mov")
								|| fileType.equals(".mp4")
								|| fileType.equals(".asf")
								|| fileType.equals(".asx")
								|| fileType.equals(".flv")) {
							fileType = "视频";
						} else if (fileType.equals(".mp3")
								|| fileType.equals(".wma")) {
							fileType = "音频";
						} else {
							fileType = "其他";
						}
			%>
			<tr <%if(i%2==0){out.print("bgcolor=\"#F1F4F9\"");}%> onmouseover="showbg(this)" onmouseout="hidebg(this)">
				<td style="text-align: left; text-indent: 12px"><%=title%></td>
				<!-- td>< %=source% ></td -->
				<td><%=author%></td>
				<td><%=riskName%></td>
				<td><%=doctName%></td>
				<td><%=applyDate%></td>
				<td><%=startTime%></td>
				<td><%=endTime%></td>
				<td><%=fileType%></td>
			</tr>
			<%
				}
			%>
			<tr>
				<td colspan="8" style="height: 25px" align="center">
					<a href="?page=1&uid=<%=uid%>"><img src="../skin/images/page_first.gif"
							border="0" /></a>
					<a href="?page=<%=page_prev%>&uid=<%=uid%>"><img
							src="../skin/images/page_prev.gif" border="0" /></a>
					<%
						int start = 0;
						if (start < (page_now - 3)) {
							start = page_now - 3;
						}
						if (start > (total_page - 5)) {
							start = total_page - 5;
						}
						if (start < 0) {
							start = 0;
						}
						for (int i = start; i < (start + 5); i++) {
							if (i >= total_page)
								break;
					%>
					<a href="?page=<%=(i + 1)%>&uid=<%=uid%>"><%
							if (page_now == (i + 1)) {
									out.print("<font color=red>" + (i + 1) + "</font>");
								} else {
									out.print((i + 1));
								}
					%></a>
					<%
						}
				if(start<(total_page-5)){
					out.print("…");
					out.print("<a href=\"?page="+total_page+"&uid=" +uid +"\">"+total_page+"</a>");
				}
				%>
					<a href="?page=<%=page_next%>&uid=<%=uid%>"><img
							src="../skin/images/page_next.gif" border="0" /></a>
					<a href="?page=<%=total_page%>&uid=<%=uid%>"><img
							src="../skin/images/page_last.gif" border="0" /></a>
				</td>
			</tr>
		</table>
	</body>
</html>