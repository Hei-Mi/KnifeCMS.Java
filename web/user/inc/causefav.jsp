<%@ page language="java" import="java.util.*,com.knife.member.*,java.text.SimpleDateFormat,com.knife.news.logic.NewsService,com.knife.news.logic.impl.NewsServiceImpl,com.knife.news.object.News" pageEncoding="UTF-8"%>
<%@include file="checkuser.jsp" %>
<%
int page_now=1;
int page_prev=1;
int page_next=1;
int total_page=1;
int page_range_start=0;
int begin = 1;
if(request.getParameter("page")!=null){
	page_now=Integer.parseInt(request.getParameter("page"));
	page_range_start=(int)page_now/5;
	page_next=page_now;
	begin = (page_now - 1) * 20 + 1;
}
if(page_now<1){page_now=1;}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>我的收藏</title>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	width:806px;
	font-size:12px;
	border-left:1px solid #BCC3CD;
}
.data_list th{
	height:30px;
	border-bottom:1px solid gray;
	color:white;
	background:url('/user/skin/images/document/title_bg.jpg') bottom repeat-x;
}
.data_list th div.title{
	position:relative;
	height:25px;
	_height:1%;
	line-height:25px;
}
.data_list td{
	height:19px;
	text-align:center;
	border-bottom:1px solid #BCC3CD;
	border-right:1px solid #BCC3CD;
}
.datatype{
	display:none;
	position:absolute;
	top:20px;
	right:0px;
	width:90px;
	line-height:150%!important;
	height:auto!important;
	_height:220px;
	max-height:220px;
	overflow-y:auto;
	border:1px solid gray;
	background:white;
	z-index:100;
}
.datatype ul{list-style-type:none;margin-left:0px;padding:0}
.datatype ul li{list-style-type:none;margin-left:0px;padding:0;text-align:center;}
.datatype ul li a{font-weight:normal}
</style>
<script type="text/javascript" src="/user/skin/js/jquery-1.6.4.min.js"></script>
<script language="javascript">
function showbg(obj){
	obj.style.backgroundImage="url('/user/skin/images/document/select_bg.jpg')";
}

function hidebg(obj){
	obj.style.backgroundImage="";
}

function removeFav(id){
	$.get("/user/opt/delfav.jsp?id="+id,function(data){
		alert(data);
		window.location.reload();
	});
}
</script>
</head>
<body>
	<table class="data_list" cellspacing="0" cellpadding="0">
		<tr>			
			<th width="50%">课程名称</th>
			<!-- th width="13%">来源</th -->
			<th width="15%">授课专家</th>
			
			<th width="20%">收录时间</th>
			<th width="15%">操作</th>
		</tr>
		<%
		UserfavDAO uvDAO=new UserfavDAO();
		NewsService newsDAO=new NewsServiceImpl();
		List docs1=uvDAO.findByUseridAndType(auser.getId().intValue(),1);
		total_page = (int) Math.ceil((float) (docs1.size()) / (float) (20));
		if(page_now>1)page_prev=page_now-1;
		if(page_now<total_page)page_next=page_now+1;
		//List docs=uvDAO.findAll(page_now);
		if(docs1.size()>0){
		List docs=uvDAO.findByUseridAndType(auser.getId().intValue(),1,page_now);
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		for(int i=0;i<docs.size();i++){
			Userfav userfav=(Userfav)docs.get(i);
			News adoc=(News)newsDAO.getNewsById(userfav.getDocid());
		String applyDate="&nbsp;";
	
		String doctName="&nbsp;";
		String source="";
		String author="&nbsp;";
		String fileType="&nbsp;";
		if(adoc!=null){
		
			
			if(adoc.getType()!=null){
				try{
					doctName=adoc.getTypeName();
				}catch(Exception e){
				}
			}
			if(doctName.length()>7){
				doctName=doctName.substring(0,6)+"..";
			}
			try{
				applyDate = dateFormat.format(userfav.getFavdate()); 
			}catch(Exception e){
				//System.out.println("convert error....... ");
			}
			if(adoc.getSource()!=null){
				if(adoc.getSource().length()>0){
					source=adoc.getSource();
					if(source.length()>7){
						source=source.substring(0,6)+"..";
					}
				}
			}
			if(adoc.getAuthor()!=null){
				if(adoc.getAuthor().length()>0){
					author=adoc.getAuthor();
					if(author.length()>7){
						author=author.substring(0,6)+"..";
					}
				}
			}
		if(adoc.getDoctype()!=null){
			if(adoc.getDoctype().length()>0){
				fileType=adoc.getDoctype();
			}
		}
		if(fileType.equals(".doc")||fileType.equals(".docx")||fileType.equals(".xls")||fileType.equals(".xlsx")||fileType.equals(".txt")||fileType.equals(".pdf")||fileType.equals(".swf")){
			fileType="文本";
		}else if(fileType.equals(".ppt")||fileType.equals(".pptx")){
			fileType="PPT";
		}else if(fileType.equals(".mpg")||fileType.equals(".wmv")||fileType.equals(".3gp")||fileType.equals(".mov")||fileType.equals(".mp4")||fileType.equals(".asf")||fileType.equals(".asx")||fileType.equals(".flv")){
			fileType="视频";
		}else if(fileType.equals(".mp3")||fileType.equals(".wma")){
			fileType="音频";
		}else{
			fileType="其他";
		}
		%>
		<tr <%if(i%2==0){out.print("bgcolor=\"#F1F4F9\"");}%> onmouseover="showbg(this)" onmouseout="hidebg(this)">
			<td style="text-align:left;text-indent:12px"><%=adoc.getTitle(21)%></td>
			<!-- td>< %=source% ></td -->
			<td><%
				if(source.length()>0){
					out.print("<a href=\""+source+"\" target=\"_blank\">"+author+"</a>");
				}else{
					out.print(author);
				}%></td>
		
			<td><%=applyDate%></td>
			<td><a href="<%
			String urlType=adoc.getDocfile();
			if(urlType!=null){
				urlType=urlType.substring(urlType.lastIndexOf("."),urlType.length());
				if(urlType.equals(".swf")){
					out.print("/user/inc/dataview.jsp?type=1&url="+adoc.getDocfile());
				}else if(urlType.equals(".jsp")){
						out.print(adoc.getDocfile()+"\" target=\"_blank");
				}else{
					out.print("/user/inc/videoview.jsp?type=1&url="+adoc.getDocfile());
				}
			}else{
				out.print("#");
			}
			%>">进入</a> | <a href="javascript:removeFav('<%=userfav.getId()%>')">取消收藏</a></td>
		</tr>
		<%
		}
		}
		}else{
			%><tr>
			<td colspan="6" align="center">
				<div>本项服务为专项收费服务，课程在开发中</div>
			</td>
			</tr><%
		}
		%>
		<tr>
			<td colspan="6" style="height:25px" align="center">
				<a href="?page=1"><img src="../skin/images/page_first.gif" border="0" /></a>
				<a href="?page=<%=page_prev%>"><img src="../skin/images/page_prev.gif" border="0" /></a>
				<%
				int start=0;
				if(start<(page_now-3)){
					start=page_now-3;
				}
				if(start>(total_page-5)){
					start=total_page-5;
				}
				if(start<0){start=0;}
				for(int i=start;i<(start+5);i++){
					if(i>=total_page) break;
				%>
				<a href="?page=<%=(i+1)%>"><%if(page_now==(i+1)){
					out.print("<font color=red>"+(i+1)+"</font>");
				}else{
					out.print((i+1));
				}%></a>
				<%
				}
				if(start<(total_page-5)){
					out.print("…");
					out.print("<a href=\"?page="+total_page+"\">"+total_page+"</a>");
				}
				%>
				<a href="?page=<%=page_next%>"><img src="../skin/images/page_next.gif" border="0" /></a>
				<a href="?page=<%=total_page%>"><img src="../skin/images/page_last.gif" border="0" /></a>
			</td>
		</tr>
	</table>
</body>
</html>