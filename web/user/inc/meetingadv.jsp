<%@page import="java.net.URLEncoder"%>
<%@ page language="java" import="java.util.*,com.knife.member.*,java.text.SimpleDateFormat,com.knife.news.logic.NewsService,com.knife.news.logic.impl.NewsServiceImpl,com.knife.news.object.News" pageEncoding="UTF-8"%>
<%@ page import="com.knife.news.object.Type,com.knife.news.logic.TypeService,com.knife.news.logic.impl.TypeServiceImpl"%>
<%@include file="checkuser.jsp" %>
<%
request.setCharacterEncoding("UTF-8");
String sql="";
String psql="";
String order="";
String porder="";
String listtype="";
String riskname="";
if(request.getParameter("sql")!=null){
	psql=request.getParameter("sql");
}
if(request.getParameter("listtype")!=null){
	listtype=request.getParameter("listtype");
}
if(request.getParameter("riskname")!=null){
	riskname=request.getParameter("riskname");
}
if(request.getParameter("order")!=null){
	porder=request.getParameter("order");
}
int page_now=1;
int page_prev=1;
int page_next=1;
int total_page=1;
int page_range_start=0;
int begin = 1;
if(request.getParameter("page")!=null){
	page_now=Integer.parseInt(request.getParameter("page"));
	page_range_start=(int)page_now/5;
	page_next=page_now;
	begin = (page_now - 1) * 20 + 1;
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>会议预告</title>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	width:806px;
	font-size:12px;
	border-left:1px solid #BCC3CD;
}
.data_list th{
	height:30px;
	border-bottom:1px solid gray;
	color:white;
	background:url('/user/skin/images/document/title_bg.jpg') bottom repeat-x;
}
.data_list th div.title{
	position:relative;
	height:25px;
	_height:1%;
	line-height:25px;
}
.data_list td{
	height:19px;
	text-align:center;
	border-bottom:1px solid #BCC3CD;
	border-right:1px solid #BCC3CD;
}
.datatype{
	display:none;
	position:absolute;
	top:20px;
	_top:15px;
	right:0px;
	width:90px;
	line-height:150%!important;
	height:auto;
	_height:220px;
	max-height:220px;
	overflow-y:auto;
	border:1px solid gray;
	background:white;
	z-index:100;
}
.datatype ul{list-style-type:none;margin-left:0px;padding:0}
.datatype ul li{list-style-type:none;margin-left:0px;padding:0;text-align:center;}
.datatype ul li a{font-weight:normal}
</style>
<script type="text/javascript" src="/user/skin/js/jquery-1.6.4.min.js"></script>
<script language="javascript">
var type2desc=true;
var typedesc=true;
var doctypedesc=true;

function showType(listid){
	var obj=$("#"+listid)[0];
	obj.style.display="block";
}

function hideType(listid){
	var obj=$("#"+listid)[0];
	obj.style.display="none";
}

function choose(field,value){
	var exts = [];
	var sql="";
	if(value.indexOf(",")>0){
		exts=value.split(",");
	}
	if(exts.length>0){
		for(var i=0;i<exts.length;i++){
			if(i==0){
				sql+="-and-("+field+"='"+exts[i]+"'";
			}else if(i==(exts.length-1)){
				sql+="-or-"+field+"='"+exts[i]+"')";
			}else{
				sql+="-or-"+field+"='"+exts[i]+"'";
			}
		}
	}else{
		sql="-and-"+field+"='"+value+"'";
	}
	location.href="?sql="+encodeURIComponent(sql);
}

function chooseType2(value){
	if(value.length>0){
		location.href="?riskname="+encodeURIComponent(value);
	}
}

function chooseList(value){
	if(value.length>0){
		location.href="?listtype="+encodeURIComponent(value);
	}
}

function changeOrder(){
	<%
	if(porder.length()>0){
		if(porder.equals("k_date")){
			out.print("location.href=\"?order=k_date-desc\";");
		}else{
			out.print("location.href=\"?order=k_date\";");
		}
	}else{
		out.print("location.href=\"?order=k_date\";");
	}
	%>
}

function showbg(obj){
	obj.style.backgroundImage="url('/user/skin/images/document/select_bg.jpg')";
}

function hidebg(obj){
	obj.style.backgroundImage="";
}

function addFav(id){
	$.get("/user/opt/addfav.jsp?type=2&docid="+id,function(data){
		alert(data);
	});
}
</script>
</head>
<body>
	<table class="data_list" cellspacing="0" cellpadding="0">
		<tr>
			<th width="37%"><div>会议名称</div></th>
			<th width="10%"><div>演讲嘉宾</div></th>
			<th width="11%"><div class="title" onmouseover="showType('list_type2')" onmouseout="hideType('list_type2')">风险类别<a href="#"><img src="/user/skin/images/get_down.gif" border="0"/></a>
				<div id="list_type2" class="datatype" style="height:266px!important;max-height:266px!important;">
					<ul>
						<li><a href="javascript:chooseType2('市场风险')">市场风险</a></li>
						<li><a href="javascript:chooseType2('信用风险')">信用风险</a></li>
						<li><a href="javascript:chooseType2('操作风险')">操作风险</a></li>
						<li><a href="javascript:chooseType2('流动性风险')">流动性风险</a></li>
						<li><a href="javascript:chooseType2('结算风险')">结算风险</a></li>
						<li><a href="javascript:chooseType2('法律风险')">法律风险</a></li>
						<li><a href="javascript:chooseType2('声誉风险')">声誉风险</a></li>
						<li><a href="javascript:chooseType2('战略风险')">战略风险</a></li>
						<li><a href="javascript:chooseType2('国家风险')">国家风险</a></li>
						<li><a href="javascript:chooseType2('IT风险')">IT风险</a></li>
						<li><a href="javascript:chooseType2('系统性风险')">系统性风险</a></li>
						<li><a href="javascript:chooseType2('全面风险')">全面风险</a></li>
						<li><a href="javascript:chooseType2('其他')">其他</a></li>
					</ul>
				</div>
				</div>
			</th>
			<th width="12%"><div>会议时间</div></th>
			<th width="9%"><div>操作</div></th>
		</tr>
		<%
		TypeService typeDAO=new TypeServiceImpl();
		NewsService newsDAO=new NewsServiceImpl();
		List users=userDAO.findActiveByEmail(login_email);
		Userinfo myuser=null;
		if(users.size()>0){
			myuser=(Userinfo)users.get(0);
		}
		//List docs = newsDAO.findPagedAllisfee(page_now,20,myuser.getIsfee());
		//total_page = dbdocDAO.getTotalPageisFee(20,myuser.getIsfee());
		//total_page = (int) Math.ceil((float) (dbdocDAO.findAll().size()) / (float) (20));
		if(listtype.length()>0){
			try{
				Type atype = typeDAO.getTypeByName(listtype);
				List atypes = typeDAO.getSubTypesById(atype.getId());
				List tmpDocs = new ArrayList();
				sql="and (k_type in('"+atype.getId()+"'";
				for(int i=0;i<atypes.size();i++){
					Type btype=(Type)atypes.get(i);
					sql+=",'"+btype.getId()+"'";
					if(btype.getTree().length()>0){
						tmpDocs.addAll(newsDAO.getNewsBySql("id!='' and k_type='"+btype.getId()+"'",null,0,-1));
					}
				}
				sql+=")";
				if(atype.getTree().length()>0){
					tmpDocs.addAll(newsDAO.getNewsBySql("id!='' and k_type='"+atype.getId()+"'",null,0,-1));
				}
				if(tmpDocs.size()>0){
					sql+=" or k_treenews in(";
					for(int i=0;i<tmpDocs.size();i++){
						News adoc = (News)tmpDocs.get(i);
						if(i>0){sql+=",";}
						sql+="'"+adoc.getId()+"'";
					}
					sql+=")";
				}
				sql+=")";
			}catch(Exception e){
			}
		}
		Collection<Object> paras = new ArrayList<Object>();
		if(myuser!=null){
			paras.add(myuser.getIsfee());
		}
		if(psql.length()>0){
			sql=psql.replace("-and-"," and ");
			sql=sql.replace("-or-"," or ");
			sql=sql.replace("-like-"," like ");
			if(sql.indexOf("like")>0){
				sql=sql.replace("%25","%");
			}
		}
		if(riskname.length()>0){
			sql=" and k_type2 like '%"+riskname+"%'";
		}
		if(porder.length()>0){
			order = porder.replace("-desc"," desc");
			order = "order by "+order;
		}else{
			order="order by length(k_order) desc,k_order desc";
		}
		//List docs = newsDAO.getNewsBySql("id!='' and k_docfile!='' and k_isfee=? " + sql + " " + order, paras, begin - 1, 20);
		List adocs = newsDAO.getNewsBySql("id!='' and k_type='1234239734-99007' and k_docfile!='' " +sql,null,0,-1);
		total_page = (int) Math.ceil((float) (adocs.size()) / (float) (20));
		if(page_now>1)page_prev=page_now-1;
		if(page_now<total_page)page_next=page_now+1;
		//System.out.println("生成查询语句:"+sql);
		List docs = newsDAO.getNewsBySql("id!='' and k_type='1234239734-99007' and k_docfile!='' " + sql + " " + order, null, begin - 1, 20);
		for(int i=0;i<docs.size();i++){
		News adoc=(News)docs.get(i);
		String source="";
		String author="&nbsp;";
		String riskName="&nbsp;";
		String doctName="&nbsp;";
		String fileType="&nbsp;";
		String startTime="&nbsp;";
		if(riskname.length()>0){
			riskName=riskname;
		}else{
			if(adoc.getType2()!=null){
				if(adoc.getType2().length()>0){
					riskName=adoc.getType2();
				}
			}
			if(riskName.indexOf(",")>0){
				riskName=riskName.substring(0,riskName.indexOf(","));
			}
		}
		if(adoc.getType()!=null){
			try{
				doctName=adoc.getTypeName();
			}catch(Exception e){
			}
		}
		if(doctName.length()>7){
			doctName=doctName.substring(0,6)+"..";
		}
		if(adoc.getSource()!=null){
			if(adoc.getSource().length()>0){
				source=adoc.getSource();
				if(source.length()>7){
					source=source.substring(0,6)+"..";
				}
			}
		}
		if(adoc.getAuthor()!=null){
			if(adoc.getAuthor().length()>0){
				author=adoc.getAuthor();
				if(author.length()>6){
					author=author.substring(0,5)+"..";
				}
			}
		}
		if(adoc.get("starttime")!=null){
			if(adoc.get("starttime").length()>0){
				startTime=adoc.get("starttime");
			}
		}
		//System.out.println("获取到的作者是:"+adoc.getId()+":::"+adoc.getAuthor());
		if(adoc.getDoctype()!=null){
			if(adoc.getDoctype().length()>0){
				fileType=adoc.getDoctype();
			}else{
				fileType=adoc.getDocfile().substring(adoc.getDocfile().lastIndexOf("."),adoc.getDocfile().length());
			}
		}
		if(fileType.equals(".doc")||fileType.equals(".docx")||fileType.equals(".xls")||fileType.equals(".xlsx")||fileType.equals(".txt")||fileType.equals(".pdf")||fileType.equals(".swf")){
			fileType="文本";
		}else if(fileType.equals(".ppt")||fileType.equals(".pptx")){
			fileType="PPT";
		}else if(fileType.equals(".mpg")||fileType.equals(".wmv")||fileType.equals(".3gp")||fileType.equals(".mov")||fileType.equals(".mp4")||fileType.equals(".asf")||fileType.equals(".asx")||fileType.equals(".flv")){
			fileType="视频";
		}else if(fileType.equals(".mp3")||fileType.equals(".wma")){
			fileType="音频";
		}else{
			fileType="其他";
		}
		%>
		<tr <%if(i%2==0){out.print("bgcolor=\"#F1F4F9\"");}%> onmouseover="showbg(this)" onmouseout="hidebg(this)">
			<td style="text-align:left;text-indent:12px"><%=adoc.getTitle(21)%></td>
			<!--td>< %=source% ></td-->
			<td><%
				if(source.length()>0){
					out.print("<a href=\""+source+"\" target=\"_blank\">"+author+"</a>");
				}else{
					out.print(author);
				}%></td>
			<td><%=riskName%></td>
			<td><%=startTime%></td>
			<td><a href="<%
			String urlType=adoc.getDocfile();
			if(urlType!=null){
				if(urlType.indexOf(".")>0){
					urlType=urlType.substring(urlType.lastIndexOf("."),urlType.length());
					if(urlType.equals(".swf")){
						out.print("dataview.jsp?url="+adoc.getDocfile());
					}else if(urlType.equals(".jsp")){
						out.print(adoc.getDocfile()+"\" target=\"_blank");
					}else{
						out.print("videoview.jsp?url="+adoc.getDocfile());
					}
				}else{
					out.print("#");
				}
			}else{
				out.print("#");
			}
			%>">简介</a> | <a href="http://www.zd211.com/login.aspx?siteid=tyfy" target="_blank">参会</a></td>
		</tr>
		<%
		}
		psql=URLEncoder.encode(psql,"UTF-8");
		listtype=URLEncoder.encode(listtype,"UTF-8");
		%>
		<tr>
			<td colspan="5" style="height:25px" align="center">
				<a href="?page=1&sql=<%=psql%>&order=<%=porder%>&listtype=<%=listtype%>&riskname=<%=riskname%>"><img src="../skin/images/page_first.gif" border="0" /></a>
				<a href="?page=<%=page_prev%>&sql=<%=psql%>&order=<%=porder%>&listtype=<%=listtype%>&riskname=<%=riskname%>"><img src="../skin/images/page_prev.gif" border="0" /></a>
				<%
				int start=0;
				if(start<(page_now-3)){
					start=page_now-3;
				}
				if(start>(total_page-5)){
					start=total_page-5;
				}
				if(start<0){start=0;}
				for(int i=start;i<(start+5);i++){
					if(i>=total_page) break;
				%>
				<a href="?page=<%=(i+1)%>&sql=<%=psql%>&order=<%=porder%>&listtype=<%=listtype%>&riskname=<%=riskname%>"><%if(page_now==(i+1)){
					out.print("<font color=red>"+(i+1)+"</font>");
				}else{
					out.print((i+1));
				}%></a><%
				}
				if(start<(total_page-5)){
					out.print("…");
					out.print("<a href=\"?page="+total_page+"&sql="+psql+"&order="+porder+"&listtype="+listtype+"&riskname="+riskname+"\">"+total_page+"</a>");
				}
				%>
				<a href="?page=<%=page_next%>&sql=<%=psql%>&order=<%=porder%>&listtype=<%=listtype%>&riskname=<%=riskname%>"><img src="../skin/images/page_next.gif" border="0" /></a>
				<a href="?page=<%=total_page%>&sql=<%=psql%>&order=<%=porder%>&listtype=<%=listtype%>&riskname=<%=riskname%>"><img src="../skin/images/page_last.gif" border="0" /></a>
			</td>
		</tr>
	</table>
</body>
</html>