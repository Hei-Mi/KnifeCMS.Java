/*!
 * Copyright © 2012 ChongQing Rural Commercial Bank
 * Js All
 * Revision 1.1 2012/03/20 yang_m@giantstone.com
 */
//Div Height
	window.onload=function () {
		function setheight() {
			var left=document.getElementById('left');
			//var mid=document.getElementById('mid');
			left.style.height=document.documentElement.clientHeight-252+'px';
			//mid.style.height=document.documentElement.clientHeight-122+'px';
			}
		setheight();
    	onresize=setheight;
}
// 头部导航菜单  
// 左侧导航菜单  
	$("#lNav p").click(function(){
		$(this).addClass("on1");
		$(this).next("ul").slideToggle().siblings("ul").slideUp();
		$(this).siblings("p").removeClass("on1");
		});
	$("#lNav > ul > li ").click(function(){
		$(this).addClass("on2");
		$(this).siblings().removeClass("on2");
		});
		
// 渲染导航菜单
	function navMenu(name1,name2,name3){
		var oneMenu=$("#nav li a");
		var twoMenu=$("#lNav p a");
		var threeMenu=$("#lNav li a");
		for(var i=0;i<oneMenu.length;i++){
			if(oneMenu[i].innerText==name1){
				$(".nav li").eq(i).addClass("on");
			}
		}
		for(var i=0;i<twoMenu.length;i++){
			if(twoMenu[i].innerText==name2){
				$("#lNav p").eq(i).addClass("on1");
				$("#lNav ul").eq(i).show();
			}
		}
		for(var i=0;i<threeMenu.length;i++){
			if(threeMenu[i].innerText==name3){
				$("#lNav li ").eq(i).addClass("on2");
			}
		}
	};