//Creat by mypenn@gmail.com
$(function(){
	$("#oLinks").hover(function(){
		$(">ul",this).fadeIn();
	},function(){
		$(">ul",this).fadeOut();
	});
	
	//
	$("#NavLeft>a,#NavRight>a").hover(function(){
		$(this).animate({"opacity":0.5});
	},function(){
		$(this).animate({"opacity":1});
	});
	
	getWrap();
	slide();
	menu();
	imgshow();
	
	$("#iFocus>h3").click(function(){
		$(this).next("ul").slideToggle();
	});
	
	//
	$("#MemShow li").hover(function(){
		$(">div.ctrl",this).fadeIn();
	},function(){
		$(">div.ctrl",this).fadeOut();
	});
	
	//
	$("#setType>ul").hover(function(){
		$(this).stop().animate({height:"94px"},200);
		$(">li>a",this).click(function(){
			$(this).addClass("active").parent().prependTo($("#setType>ul"));
			$("#setType>ul").stop().css({height:"31px"});
		});
	},function(){
		$(this).stop().animate({height:"31px"},200);
	});
})

//
function addFav(a,b){
	if(document.all){
		window.external.AddFavorite(b,a)
	}else if(window.sidebar){
		window.sidebar.addPanel(a,b,"")
	}else{
		alert("对不起，您的浏览器不支持此操作!\n请您使用菜单栏或Ctrl+D收藏本站。")
	}

}

//
function getTab(o,t){
	$(t).addClass("active").parent().siblings().children("a").removeClass("active");
	$(o).show().siblings().hide();
	$(t).blur();
}

//
function getWrap(){
	//Left
	$("#NavLeft>a").click(function(){
		var $btn = $(this);
		if($("#bgDiv").length == 0){
			$("body").append("<div id='bgDiv' />");
			$("#bgDiv").css("opacity",0.9);
		}
		
		if($("#LeftWrapper").length == 0){
			$("body").append("<div class='left-wrapper' id='LeftWrapper'><h1>秀&nbsp;&nbsp;/&nbsp;&nbsp;SHOW</h1></div>");
		}
		
		$("#LeftWrapper").append("<div class='cont'><div class='col'></div><div class='col'></div></div>");
	
		if($("#LeftWrapper").css("left") != "0px"){
			$btn.addClass("on");
			$("#bgDiv").fadeIn();
			$("#LeftWrapper").animate({left:0},function(){
				$("#LeftWrapper").css("position","absolute");
			});
			
			if($("#RightWrapper").length == 1){
				$("#NavRight>a").removeClass("on");
				$("#RightWrapper").css("position","fixed").animate({right:-660},function(){
					$("#RightWrapper").remove();
				});
				$("#Wrapper").css({"position":"fixed","top":-wst});
			}else{
				wst = document.documentElement.scrollTop || document.body.scrollTop;
				$("#Wrapper").css({"position":"fixed","top":-wst});
			}
		}else{
			$btn.removeClass("on");
			$("#bgDiv").fadeOut();
			$("#LeftWrapper").css("position","fixed").animate({left:-700},function(){
				$("#LeftWrapper").remove();
			});
			$("#Wrapper").css("position","static");
			window.scroll(0,wst);
		}
		
		var page =  1;
		leftData(page);
		
		var pH = document.documentElement.clientHeight || document.body.clientHeight;
		window.onscroll = function(){
			var sT = document.documentElement.scrollTop || document.body.scrollTop;
			if(sT == ldH - pH){
				$("#LeftWrapper").append("<div class='load' id='Loading'></div>");
				page++;
				leftData(page);
			}
		}
	});
	
	$("#NavRight>a").click(function(){
		var $btn = $(this);
		if($("#bgDiv").length == 0){
			$("body").append("<div id='bgDiv' />");
			$("#bgDiv").css("opacity",0.9);
		}
		
		if($("#RightWrapper").length == 0){
			$("body").append("<div class='right-wrapper' id='RightWrapper'><h1>万达明星&nbsp;&nbsp;/&nbsp;&nbsp;STAR</h1><div class='cont' /></div>");
		}
		
		if($("#RightWrapper").css("right") != "0px"){
			$btn.addClass("on");
			$("#bgDiv").fadeIn();
			$("#RightWrapper").animate({right:0},function(){
				$("#RightWrapper").css("position","absolute")
			});
			
			if($("#LeftWrapper").length == 1){
				$("#NavLeft>a").removeClass("on");
				$("#LeftWrapper").css("position","fixed").animate({left:-700},function(){
					$("#LeftWrapper").remove();
				});
				$("#Wrapper").css({"position":"fixed","top":-wst});
			}else{
				wst = document.documentElement.scrollTop || document.body.scrollTop;
				$("#Wrapper").css({"position":"fixed","top":-wst});
			}
		}else{
			$btn.removeClass("on");
			$("#bgDiv").fadeOut();
			$("#RightWrapper").css("position","fixed").animate({right:-660},function(){
				$("#RightWrapper").remove();
				$("#Wrapper").css("position","static");
				window.scroll(0,wst);
			});
			
			
		}
		
		var page =  1;
		rightData(page);
		
		var pH = document.documentElement.clientHeight || document.body.clientHeight;
		window.onscroll = function(){
			var sT = document.documentElement.scrollTop || document.body.scrollTop;
			if(sT == rdH - pH){
				$("#RightWrapper").append("<div class='load' id='Loading'></div>");
				page++;
				rightData(page);
			}
		}
	});
}

var ldH;
function leftData(i){
	$.ajax({
		url:"d1.json",
		data:{'page':i}
	}).done(function(data){
		var data = eval(data);
		$.each(data.list,function(i,item){
			var html = "<div class='cell'><a href='"+item.links+"'><img src='"+item.src+"' alt='' /></a><div class='info'><h5>"+item.title+"</h5><h3>"+item.name+"</h3><a class='more' href='"+item.links+"'>&gt; 更多</a><div class='bg'></div></div></div>";
			
			if( i / 2 - parseInt(i/2) == 0){
				$(html).appendTo($("#LeftWrapper>.cont>.col:first"));
			}else{
				$(html).appendTo($("#LeftWrapper>.cont>.col:last"));
			}
			
			$("#LeftWrapper .bg").css("opacity",0.6);
		})
		
		$("#LeftWrapper div.cell").hover(function(){
			$("div.info",this).stop().animate({bottom:0});
		},function(){
			$("div.info",this).stop().animate({bottom:-130});
		});
		
		setInterval(function(){
			ldH = $("#LeftWrapper").outerHeight();
		},50);
		$("#Loading").remove();
	});
}

var rdH;
function rightData(i){
	$.ajax({
		url:"d2.json",
		data:{'page':i}
	}).done(function(data){
		var data = eval(data);
		$.each(data.list,function(i,item){
			var html = "<div class='cell'><a href='"+item.links+"'><img src='"+item.img+"' alt='' /></a><div class='info'><h3><a href='"+item.links+"'>"+item.name+"</a></h3><span class='num'>"+item.num+"</span></div><p>"+item.act+"</p></div>";
			
			$(html).appendTo($("#RightWrapper>.cont"));
			
			$("#RightWrapper>.cont img").css("opacity",0.6);
		})
		
		setInterval(function(){
			rdH = $("#RightWrapper").outerHeight();
		},50);
		
		$("#Loading").remove();
		
		$("#RightWrapper div.cell").hover(function(){
			$("img",this).stop().animate({"opacity":1});
		},function(){
			$("img",this).stop().animate({"opacity":0.6});
		});
	});
}

//
function slide(){
	var $list = $("#Slide>ul>li"),$link = $("#Slide p>a");
	var maxi = $list.length,ai = -1, sT ,sTimer=5000,ah = 400;
	$link.mouseover(function(){
		ai = $(this).index();
		$(this).addClass("active").siblings().removeClass("active");
		$list.parent().stop().animate({marginTop:-ai*ah});
	});
	
	var _auto = function(){
		ai < maxi - 1 ? ai++ : ai = 0;
		$link.eq(ai).mouseover();
		sT = setTimeout(_auto,sTimer);
	}
	
	_auto();
	
	$list.hover(function(){
		clearTimeout(sT);
	},function(){
		sT = setTimeout(_auto,sTimer);
	});
	
	$link.hover(function(){
		clearTimeout(sT);
	},function(){
		sT = setTimeout(_auto,sTimer);
	});
} 

//
function menu(){
	var i = $("#pMenu>a").length;
	if(i < 6){
		$("#pMenu>a").width((909 - i)/i);
	}
}

//
function imgshow(){
	var mi = 0;
	$("#ImgList a").click(function(){
		mi = $(this).index();
		$(this).addClass("active").siblings().removeClass("active");
		var src = $(this).attr("href");
		if($("#BigImg>img").length == 0){
			$("#BigImg").html("<img src='"+src+"' alt='' />");
		}else{
			$("#BigImg>img").attr("src",src);
		}
		return false;
	});
	
	$("#ImgList a:first").click();
	
	var mlen = $("#ImgList a").length;
	$("#imgNavPrev").click(function(){
		mi > 0 ? mi-- : mi = mlen - 1;
		$("#ImgList a").eq(mi).click();
	});
	
	$("#imgNavNext").click(function(){
		mi < mlen - 1 ? mi++ : mi = 0;
		$("#ImgList a").eq(mi).click();
	});
}