<%@ page language="java" pageEncoding="UTF-8"  import="java.util.*,com.knife.tools.TreeUtils"%>
<%@ include file="checkUser.jsp" %>
<%
try{
	if(user!=null){
		String siteId = request.getParameter("siteId");
		siteId = siteId == null ? "":siteId.trim();
		String nodeId = request.getParameter("id");
		nodeId = nodeId == null ? "0":nodeId.trim();
		String tree_id = request.getParameter("tree_id");
		tree_id = tree_id == null ? "":tree_id.trim();
		String jsonStr = "";
		String uid = request.getParameter("uid");
		uid = uid == null ? "":uid.trim();
		String user_id = popedom==0||popedom==1 ? user.getId():"";
		if(!"".equals(siteId)){
			jsonStr = TreeUtils.getSubTreeNodesJson(siteId,nodeId,user_id,tree_id,uid);
		}else{
			jsonStr = TreeUtils.getSiteTreeJson(uid);
		}
		response.setContentType("text/json;charset=utf-8");
		response.getWriter().write(jsonStr);
	}
}catch(Exception e){
}
%>