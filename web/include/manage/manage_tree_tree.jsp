<%@ page language="java"  pageEncoding="UTF-8"  import="com.knife.news.model.Constants,com.knife.news.object.User,com.knife.news.model.Tree,com.knife.news.logic.TreeService,com.knife.news.logic.impl.TreeServiceImpl"%>
<%@ include file="checkUser.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>分类树管理</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<link rel="stylesheet" href="/include/manage/zTreeStyle/zTreeStyle.css" type="text/css"/>
<script type="text/javascript" src="/include/manage/js/jquery.js"></script>
<script type="text/javascript" src="/include/manage/js/jquery-ztree.js"></script>
<SCRIPT LANGUAGE="JavaScript">
<!--
$.ajaxSetup ({
	cache: false
});

function clone(jsonObj, newName) {
    var buf;
    if (jsonObj instanceof Array) {
        buf = [];
        var i = jsonObj.length;
        while (i--) {
            buf[i] = clone(jsonObj[i], newName);
        }
        return buf;
    }else if (typeof jsonObj == "function"){
        return jsonObj;
    }else if (jsonObj instanceof Object){
        buf = {};
        for (var k in jsonObj) {
	        if (k!="parentNode") {
	            buf[k] = clone(jsonObj[k], newName);
	            if (newName && k=="name") buf[k] += newName;
	        }
        }
        return buf;
    }else{
        return jsonObj;
    }
}
  
	var zTree1;
	var setting;

	setting = {
		editable: true,
		checkable : false,
		async: true,
		asyncUrl: "getTreeList.jsp",
		asyncParam: ["name", "id"],
		callback: {
			rename: zTreeOnRename,
			remove: zTreeOnRemove,
			click: zTreeOnClick,
			drag: zTreeOnDrag,
			drop: zTreeOnDrop,
			rightClick: zTreeOnRightClick
		}
	};

	zNodes =[<%
		TreeService treeDAO = TreeServiceImpl.getInstance();
		List<Tree> trees=treeDAO.getAllTree(1);
		for(int i=0;i<trees.size();i++){
			Tree atree = trees.get(i);
			out.print("{ \"id\":\""+atree.getId()+"\", \"name\":\""+atree.getName()+"\"}");
			if(i<=(trees.size()-1)){
				out.println(",");
			}
		}
		%>];

	$(document).ready(function(){
		refreshTree();

		$("body").bind("mousedown", 
			function(event){
				if (!(event.target.id == "rMenu" || $(event.target).parents("#rMenu").length>0)) {
					$("#rMenu").hide();
				}
			});
	});

	function showRMenu(type, x, y) {
		$("#rMenu ul").show();
		if (type=="root") {
			$("#m_del").hide();
			$("#m_check").hide();
			$("#m_unCheck").hide();
		}
		$("#rMenu").css({"top":y+"px", "left":x+"px", "display":"block"});
	}
	function hideRMenu() {
		$("#rMenu").hide();
	}

	var rightTargetId="0";
	function zTreeOnRightClick(event, treeId, treeNode) {
		if (!treeNode) {
			zTree1.cancelSelectedNode();
			rightTargetId="0";
			showRMenu("root", event.clientX, event.clientY);
		} else if (treeNode && !treeNode.noR) {
			rightTargetId=treeNode.id;
			if (treeNode.newrole && event.target.tagName != "a" && $(event.target).parents("a").length == 0) {
				zTree1.cancelSelectedNode();
				showRMenu("root", event.clientX, event.clientY);
			} else {
				zTree1.selectNode(treeNode);
				showRMenu("node", event.clientX, event.clientY);
			}
		}
	}
	
	function expandAll(expandSign) {
		zTree1.expandAll(expandSign);
	}

	var addCount = 1;
	function addTreeNode() {
		var newtypename="新增分类树" + (addCount++);
		var tempName=window.prompt('请输入分类树名称',newtypename);
		if(tempName==""||tempName==null){
		}else{
			//ajax调用服务器端参数，产生新的ID值
			$.get("/manage_tree_ajax.ad", { parameter: "add",name: tempName }, function(data){
				zTree1.addNodes(zTree1.getSelectedNode(), [{ id:data,name:tempName}]);
			});
		}
		hideRMenu();
	}
	function removeTreeNode() {
		var node = zTree1.getSelectedNode();
		if (node) {
			if (node.nodes && node.nodes.length > 0) {
				var msg = "该分类包含子分类，如果删除将连同子分类一起删掉。\n确认删除吗？";
				if (confirm(msg)==true){
					$.get("/manage_tree_ajax.ad", {parameter:"delete", id: node.id } );
					zTree1.removeNode(node);
				}
			} else {
				var msg = "确认删除吗？";
				if (confirm(msg)==true){
					$.get("/manage_tree_ajax.ad", {parameter:"delete", id: node.id } );
					zTree1.removeNode(node);
				}
			}
		}
		hideRMenu();
	}
	
	function editTreeNode(){
		var node = zTree1.getSelectedNode();
		if (node) {
			
		}
		hideRMenu();
	}

	function refreshTree() {
		hideRMenu();
		zTree1 = $("#treeList").zTree(setting);
	}
	
	function zTreeOnClick(event, treeId, treeNode){
		window.parent.frames["config"].location.href="/manage_tree.ad?parameter=edit&id=" + treeNode.id;
	}
	
	function zTreeOnRename(event, treeId, treeNode) {
		//alert(treeNode.tId + ", " + treeNode.name);
		$.get("/manage_tree_ajax.ad", {parameter:"rename", id:treeNode.id, name:treeNode.name});
	}
	
	function zTreeOnRemove(event, treeId, treeNode) {
		//var msg = "确认删除吗？";
		//if (confirm(msg)==true){
			$.get("/manage_tree_ajax.ad", {parameter:"delete", id:treeNode.id });
		//}else{
		//	return false;
		//}
	}
	
	var pnodeid,dragNodeIndex;
	function zTreeOnDrag(event, treeId, treeNode) {
		if(treeNode.parentNode!=null){
			pnodeid=treeNode.parentNode.id;
		}else{
			pnodeid="0";	
		}
		dragNodeIndex=zTree1.getNodeIndex(treeNode);
	}
	
	function zTreeOnDrop(event, treeId, treeNode, targetNode, moveType) {
		var pid;
		var order=zTree1.getNodeIndex(treeNode)+1;
		if(moveType=="inner"){//移到之内
			//nodes=zTree1.getNodesByParam("level", targetNode.level+1, targetNode);
			pid=targetNode.id;
		}else if(moveType=="before" || moveType=="after"){//移到之前
			if(targetNode.parentNode!=null){
				//nodes = zTree1.getNodesByParam("level", targetNode.level, targetNode.parentNode);
				pid=targetNode.parentNode.id;
			}else{
				//nodes = zTree1.getNodesByParam("level", targetNode.level, null);
				pid="0";
			}
		}
		changeType(treeNode.id,pid,order);
	}
	
	function changeType(nid,pid,order){
		//alert(nid+"被移动到了栏目:"+pid+"下，顺序是:"+order);
		$.get("/manage_tree_ajax.ad", {parameter:"order", id: nid, parent: pid, order: order } );
	}
  //-->
</SCRIPT>
<style type="text/css">
body {
 padding: 0px;
 margin: 0px;
 font-size:12px;
}
#treeTitle{
	font-weight:bold;
	background:url('/include/manage/images/main/tab_05.gif');
	/*width:165px;*/
	height:28px;
	margin:3px;
	margin-bottom:0;
	border-left:1px solid #b5d6e6;
	border-right:1px solid #b5d6e6;
	text-indent:15px;
}
#treeTitle a{line-height:28px;color:black;text-decoration:none}
#treeList {
	margin:3px;
	margin-top:0;
	/*width:165px;*/
	height:482px;
	border:1px solid #b5d6e6;
	border-top:0;
	overflow:hidden;
	overflow-y:auto;
}
.tree{padding:0;}
/* ------------- 右键菜单 -----------------  */

div#rMenu {
	background-color:#F9F9F9;
	text-align: left;
	width:72px;
	border-top:1px solid #F1F1F1;
	border-left:1px solid #F1F1F1;
	border-right:1px solid gray;
	border-bottom:1px solid gray;
}

div#rMenu ul {
	list-style: none outside none;
	margin:0px 1px;
	padding:0;
}
div#rMenu ul li {
	cursor: pointer;
	width:70px;
	height:20px;
	line-height:20px;
	text-indent:8px;
	margin:1px 0px;
	font-size:12px;
	border-bottom:1px solid gray;
}
div#rMenu ul li a{display:block;width:100%;text-decoration:none;color:black}
div#rMenu ul li a:hover{
	color:white;
	background-color:darkblue;
}
div#rMenu ul li a:visited{color:black}
</style>
	</head>
<body>
<div id="treeTitle"><img src="/include/manage/zTreeStyle/img/sim/tree.png" align="absmiddle"/>&nbsp;<a href="/manage_tree.ad?parameter=list" target="config">分 类 树</a></div>
<div id="treeList" class="tree"></div>
<div id="rMenu" style="position:absolute; display:none;">
	<ul id="m_add"><li><a href="javascript:addTreeNode()">新增树</a></li></ul>
	<ul id="m_del"><li><a href="javascript:removeTreeNode()">删除树</a></li></ul>
	<ul id="m_check"><li><a href="javascript:refreshTree()">刷新</a></li></ul>
</div>
	</body>
</html>