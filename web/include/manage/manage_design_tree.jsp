<%@ page language="java" pageEncoding="UTF-8" import="com.knife.news.model.Constants,com.knife.news.object.User"%>
<%@ include file="checkUser.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>模板列表</title>
		<link rel="stylesheet" href="/include/manage/zTreeStyle/zTreeStyle.css" type="text/css">
<script type="text/javascript" src="/include/manage/js/jquery.js"></script>
<script type="text/javascript" src="/include/manage/js/jquery-ztree.js"></script>
<script type="text/javascript">
<!--
$.ajaxSetup ({  
	cache: false
}); 

function clone(jsonObj, newName) {
    var buf;
    if (jsonObj instanceof Array) {
        buf = [];
        var i = jsonObj.length;
        while (i--) {
            buf[i] = clone(jsonObj[i], newName);
        }
        return buf;
    }else if (typeof jsonObj == "function"){
        return jsonObj;
    }else if (jsonObj instanceof Object){
        buf = {};
        for (var k in jsonObj) {
	        if (k!="parentNode") {
	            buf[k] = clone(jsonObj[k], newName);
	            if (newName && k=="name") buf[k] += newName;
	        }
        }
        return buf;
    }else{
        return jsonObj;
    }
}

var zTree1;
var setting;

setting = {
	async: true,
	asyncUrl: "getTemplate.jsp",
	asyncParam: ["name", "id", "siteId"],
	callback: {
			click: zTreeOnClick
		}
};

	var zNodes =[{siteId:""}];

	$(document).ready(function(){
		refreshTree();

		$("body").bind("mousedown", 
			function(event){
				if (!(event.target.id == "rMenu" || $(event.target).parents("#rMenu").length>0)) {
					$("#rMenu").hide();
				}
			});
	});

	function showRMenu(type, x, y) {
		$("#rMenu ul").show();
		if (type=="root") {
			$("#m_add").hide();
		}
		$("#rMenu").css({"top":y+"px", "left":x+"px", "display":"block"});
	}
	function hideRMenu() {
		$("#rMenu").hide();
	}
	
	function expandAll(expandSign) {
		zTree1.expandAll(expandSign);
	}

	function refreshTree() {
		hideRMenu();
		zTree1 = $("#treeList").zTree(setting);
	}
	
	function zTreeOnClick(event, treeId, treeNode){
		if(treeNode.isParent){
			window.parent.frames["config"].location.href="/template.do?parameter=designInfo&sid="+ treeNode.siteId;
		}else{
			window.parent.frames["config"].location.href="/template.do?parameter=designEdit&name=" + treeNode.id+"&sid="+ treeNode.siteId;
		}
	}
  //-->
</script>
<style type="text/css">
body {
 padding: 0px;
 margin: 0px;
 font-size:12px;
}
#treeTitle{
	font-weight:bold;
	background:url('/include/manage/images/main/tab_05.gif');
	/*width:165px;*/
	height:28px;
	margin:3px;
	margin-bottom:0;
	border-left:1px solid #b5d6e6;
	border-right:1px solid #b5d6e6;
	text-indent:15px;
}
#treeTitle a{line-height:28px;color:black;text-decoration:none}
#treeList {
	margin:3px;
	margin-top:0;
	/*width:165px;*/
	height:482px;
	border:1px solid #b5d6e6;
	border-top:0;
	overflow:hidden;
	overflow-y:auto;
}
.tree{padding:0;}
</style>
	</head>
<body>
<div id="treeTitle"><img src="/include/manage/zTreeStyle/img/sim/tree.png" align="absmiddle"/>&nbsp;<a href="/manage_template.ad?parameter=list" target="config">所有模板</a></div>
<div id="treeList" class="tree"></div>
	</body>
</html>