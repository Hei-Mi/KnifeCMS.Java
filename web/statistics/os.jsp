<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import = "java.io.PrintWriter" %>
<%@ page import="com.knife.news.stat.*"%>
<%@ page import="org.jfree.data.category.*"%>
<%@ page import="org.jfree.data.general.DefaultPieDataset"%>
<%
DefaultPieDataset dataset = (DefaultPieDataset)session.getAttribute("dataset");
String fileName = ChartHandle.generatePieChart("访客操作系统统计", session, dataset, new PrintWriter(out));
String graphURL = request.getContextPath() + "/servlet/DisplayChart?filename=" + fileName;
%>
<HTML>
<HEAD>
<TITLE>访客操作系统统计</TITLE>
</HEAD>
<BODY>
<P ALIGN="CENTER">
<img src="<%=graphURL%>" width=500 height=300 border=0">
</P>
</BODY>
</HTML>
