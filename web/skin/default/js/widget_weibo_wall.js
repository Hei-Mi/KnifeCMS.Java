var $Debug = (function() {
    var contentList = [];
    function add_to_content(sText, oOpts, sCMD) {
        var key;
        var text = sText != null ? sText: "";
        var opts = {
            color: null,
            bgcolor: null,
            html: null
        };
        var cmd = sCMD != null ? sCMD: "log";
        oOpts = oOpts != null ? oOpts: {};
        for (key in opts) {
            if (oOpts[key] != null) {
                opts[key] = oOpts[key]
            }
        }
        contentList.push({
            label: text,
            cmd: cmd,
            opts: opts,
            time: new Date()
        })
    }
    function debug_proto(sText, oOpts) {
        add_to_content(sText, oOpts, "log")
    }
    debug_proto.fatal = function(sText, oOpts) {
        add_to_content(sText, oOpts, "fatal")
    };
    debug_proto.error = function(sText, oOpts) {
        add_to_content(sText, oOpts, "error")
    };
    debug_proto.warning = function(sText, oOpts) {
        add_to_content(sText, oOpts, "warning")
    };
    debug_proto.info = function(sText, oOpts) {
        add_to_content(sText, oOpts, "info")
    };
    debug_proto.log = function(sText, oOpts) {
        add_to_content(sText, oOpts, "log")
    };
    debug_proto.clear = function() {
        contentList = []
    };
    debug_proto.contentList = contentList;
    return debug_proto
})();
var Jobs = (function() {
    var E = function(id) {
        if (typeof id === "string") {
            return document.getElementById(id)
        } else {
            return id
        }
    };
    var addEvent = function(sNode, sEventType, oFunc) {
        var oElement = E(sNode);
        if (oElement == null) {
            return
        }
        sEventType = sEventType || "click";
        if ((typeof oFunc).toLowerCase() != "function") {
            return
        }
        if (oElement.attachEvent) {
            oElement.attachEvent("on" + sEventType, oFunc)
        } else {
            if (oElement.addEventListener) {
                oElement.addEventListener(sEventType, oFunc, false)
            } else {
                oElement["on" + sEventType] = oFunc
            }
        }
    };
    var Ready = (function() {
        var funcList = [];
        var inited = false;
        var exec_func_list = function() {
            if (inited == true) {
                return
            }
            inited = true;
            for (var i = 0,
            len = funcList.length; i < len; i++) {
                if ((typeof funcList[i]).toLowerCase() == "function") {
                    funcList[i].call()
                }
            }
            funcList = []
        };
        if (document.attachEvent) {
            document.attachEvent("onreadystatechange",
            function() {
                if ((/loaded|complete/).test(document.readyState)) {
                    exec_func_list()
                }
            });
            if (window == window.top) {
                setTimeout(function() {
                    try {
                        document.documentElement.doScroll("left")
                    } catch(e) {
                        setTimeout(arguments.callee, 0);
                        return
                    }
                    exec_func_list()
                },
                500)
            }
        } else {
            if (document.addEventListener) {
                addEvent(document, "DOMContentLoaded", exec_func_list)
            } else {
                if (/WebKit/i.test(navigator.userAgent)) { (function() {
                        if (/loaded|complete/i.test(document.readyState)) {
                            exec_func_list();
                            return
                        }
                        setTimeout(arguments.callee, 25)
                    })()
                }
            }
        }
        addEvent(window, "load", exec_func_list);
        return function(oFunc) {
            if (inited == false) {
                funcList.push(oFunc)
            } else {
                if ((typeof oFunc).toLowerCase() == "function") {
                    oFunc.call()
                }
            }
        }
    })();
    function jobCls() {
        var regist_jobs_list = {};
        var run_job_list = [];
        function start_event() {
            var run_index = 0;
            var run_list_len = run_job_list.length;
            var jobName, jobFunc, startTime; (function() {
                if (run_list_len > run_index) {
                    jobName = run_job_list[run_index];
                    jobFunc = regist_jobs_list[jobName];
                    startTime = new Date();
                    if (typeof jobFunc == "undefined") {
                        $Debug.fatal("<b>Job [" + jobName + "] 未定义!</b>", {
                            html: true
                        });
                        return
                    }
                    try {
                        jobFunc.call()
                    } catch(e) {
                        $Debug.fatal("<b>Job [" + jobName + "] 执行失败!</b><br/>&nbsp;" + e.name + "<br/>&nbsp;" + (e.message || e.description) + (e.fileName ? "<br/>&nbsp;" + e.fileName: "") + (e.lineNumber ? "<br/>&nbsp;" + e.lineNumber: ""), {
                            html: true
                        })
                    } finally {
                        $Debug.info("<b>Job [" + jobName + "] 执行成功(" + (new Date() - startTime) + "毫秒)</b>", {
                            html: true
                        })
                    }
                    run_index++;
                    setTimeout(arguments.callee, 25)
                }
            })()
        }
        this.regist = function(sJobName, oJobFunc) {
            if (regist_jobs_list[sJobName] == null) {
                regist_jobs_list[sJobName] = oJobFunc
            }
            run_job_list.push(sJobName)
        };
        Ready(start_event)
    }
    return new jobCls()
})();
if (!$CONFIG) {
    $CONFIG = {}
}
var scope = $CONFIG;
Function.prototype.bind2 = function(object) {
    var __method = this;
    return function() {
        return __method.apply(object, arguments)
    }
};
scope.$VERSION = "t3";
scope.$BASEIMG = "http://timg.sjs.sinajs.cn/" + scope.$VERSION + "/";
scope.$BASECSS = "http://timg.sjs.sinajs.cn/" + scope.$VERSION + "/";
scope.$BASEJS = "http://tjs.sjs.sinajs.cn/" + scope.$VERSION + "/";
scope.$BASESTATIC = "http://tjs.sjs.sinajs.cn/" + scope.$VERSION + "/";
scope._ua = navigator.userAgent.toLowerCase();
scope.$IE = /msie/.test(scope._ua);
scope.$OPERA = /opera/.test(scope._ua);
scope.$MOZ = /gecko/.test(scope._ua);
scope.$IE5 = /msie 5 /.test(scope._ua);
scope.$IE55 = /msie 5.5/.test(scope._ua);
scope.$IE6 = /msie 6/.test(scope._ua);
scope.$IE7 = /msie 7/.test(scope._ua);
scope.$SAFARI = /safari/.test(scope._ua);
scope.$winXP = /windows nt 5.1/.test(scope._ua);
scope.$winVista = /windows nt 6.0/.test(scope._ua);
var $IE = scope.$IE,
$MOZ = scope.$MOZ,
$IE6 = scope.$IE6;
function $import(url) {}
var Boot = {};
Boot.addDOMLoadEvent = function(func) {
    if (!window.__load_events) {
        var init = function() {
            if (arguments.callee.done) {
                return
            }
            arguments.callee.done = true;
            if (window.__load_timer) {
                clearInterval(window.__load_timer);
                window.__load_timer = null
            }
            for (var i = 0; i < window.__load_events.length; i++) {
                window.__load_events[i]()
            }
            window.__load_events = null
        };
        if (document.addEventListener) {
            document.addEventListener("DOMContentLoaded", init, false)
        }
        if (/WebKit/i.test(navigator.userAgent)) {
            window.__load_timer = setInterval(function() {
                if (/loaded|complete/.test(document.readyState)) {
                    init()
                }
            },
            10)
        }
        if (window.ActiveXObject) {
            window.__load_timer = setInterval(function() {
                try {
                    document.body.doScroll("left");
                    init()
                } catch(ex) {}
            },
            10)
        }
        window.onload = init;
        window.__load_events = []
    }
    window.__load_events.push(func)
};
Boot.getJsVersion = function() {
    var ver = false;
    if ($CONFIG) {
        ver = $CONFIG.js ? $CONFIG.js: ""
    }
    if (ver) {
        return "?v=" + ver
    } else {
        return ""
    }
};
try {
    Boot.addDOMLoadEvent(main)
} catch(e) {}
if (typeof Sina == "undefined") {
    Sina = {}
}
Sina.pkg = function(ns) {
    if (!ns || !ns.length) {
        return null
    }
    var levels = ns.split(".");
    var nsobj = Sina;
    for (var i = (levels[0] == "Sina") ? 1 : 0; i < levels.length; ++i) {
        nsobj[levels[i]] = nsobj[levels[i]] || {};
        nsobj = nsobj[levels[i]]
    }
    return nsobj
};
function $E(oID) {
    var node = typeof oID == "string" ? document.getElementById(oID) : oID;
    if (node != null) {
        return node
    } else {}
    return null
}
function $C(tagName) {
    return document.createElement(tagName)
}
function $N(name) {
    return document.getElementsByName(name)
}
try {
    document.execCommand("BackgroundImageCache", false, true)
} catch(e) {}
if (typeof App == "undefined") {
    var App = {}
}
Sina.pkg("Core");
if (typeof Core == "undefined") {
    Core = Sina.Core
}
Sina.pkg("Core.Events");
Core.Events.addEvent = function(elm, func, evType, useCapture) {
    var _el = $E(elm);
    if (_el == null) {
        $Debug("addEvent 找不到对象：" + elm);
        return
    }
    if (typeof useCapture == "undefined") {
        useCapture = false
    }
    if (typeof evType == "undefined") {
        evType = "click"
    }
    if (_el.addEventListener) {
        _el.addEventListener(evType, func, useCapture);
        return true
    } else {
        if (_el.attachEvent) {
            var r = _el.attachEvent("on" + evType, func);
            return true
        } else {
            _el["on" + evType] = func
        }
    }
};
Sina.pkg("Core.Base"); (function() {
    var Detect = function() {
        var ua = navigator.userAgent.toLowerCase();
        this.$IE = /msie/.test(ua);
        this.$OPERA = /opera/.test(ua);
        this.$MOZ = /gecko/.test(ua);
        this.$IE5 = /msie 5 /.test(ua);
        this.$IE55 = /msie 5.5/.test(ua);
        this.$IE6 = /msie 6/.test(ua);
        this.$IE7 = /msie 7/.test(ua);
        this.$SAFARI = /safari/.test(ua);
        this.$winXP = /windows nt 5.1/.test(ua);
        this.$winVista = /windows nt 6.0/.test(ua);
        this.$FF2 = /Firefox\/2/i.test(ua);
        this.$IOS = /\((iPhone|iPad|iPod)/i.test(ua)
    };
    Core.Base.detect = new Detect()
})();
Core.Events.getEvent = function() {
    return window.event
};
if (!Core.Base.detect.$IE) {
    Core.Events.getEvent = function() {
        if (window.event) {
            return window.event
        }
        var o = arguments.callee.caller;
        var e;
        var n = 0;
        while (o != null && n < 40) {
            e = o.arguments[0];
            if (e && (e.constructor == Event || e.constructor == MouseEvent)) {
                return e
            }
            n++;
            o = o.caller
        }
        return e
    }
}
Core.Events.stopEvent = function(el) {
    var ev = el ? el: Core.Events.getEvent();
    ev.cancelBubble = true;
    ev.returnValue = false
};
if (!$IE) {
    Core.Events.stopEvent = function(el) {
        var ev = el ? el: Core.Events.getEvent();
        ev.preventDefault();
        ev.stopPropagation()
    }
}
Sina.pkg("Core.Array");
Core.Array.foreach = function(ar, insp) {
    if (ar == null && ar.constructor != Array) {
        return []
    }
    var i = 0,
    len = ar.length,
    r = [];
    while (i < len) {
        var x = insp(ar[i], i);
        if (x !== null) {
            r[r.length] = x
        }
        i++
    }
    return r
}; (function() {
    var isLoaded = false;
    App.addStyle = function(rules) {
        var styleElement = document.createElement("style");
        styleElement.type = "text/css";
        if ($IE) {
            styleElement.styleSheet.cssText = rules
        } else {
            var frag = document.createDocumentFragment();
            frag.appendChild(document.createTextNode(rules));
            styleElement.appendChild(frag)
        }
        function append() {
            document.getElementsByTagName("head")[0].appendChild(styleElement)
        }
        append()
    }
})();
Jobs.regist("init_weib11oWall_widget",
function() {
    var _each = Core.Array.foreach;
    var _addEvent = Core.Events.addEvent;
    var href = location.search;
    var colors = /colors=([A-Fa-f\d,]+)/.test(href) ? RegExp.$1: "";
    if (colors) {
        colors = colors.split(",");
        var cstr = "";
        if (colors[0]) {
            cstr += ".tblog_wrap .tblog_header{border:none;background:#" + colors[0] + ";}\n"
        }
        if (colors[1]) {
            cstr += ".tblog_wrap .tblog_main{ background:#" + colors[1] + " }\n"
        }
        if (colors[2]) {
            cstr += ".tblog_wrap .mbTxtB,\n .tblog_wrap .fans_top ul li span,\n .tblog_wrap .fans_list h4 span{color:#" + colors[2] + " }\n"
        }
        if (colors[3]) {
            cstr += ".tblog_wrap a, .tblog_wrap a:link, .tblog_wrap a:hover, .tblog_wrap a:visited, .tblog_wrap .fans_list h4 em{color:#" + colors[3] + " }"
        }
        App.addStyle(cstr)
    }
    var isborder = /noborder=(\d+)/.test(href) ? RegExp.$1: 1;
    if (isborder == 0) {
        App.addStyle(".tblog_wrap .wrap_top{background:none;}\n.tblog_wrap .wrap_bottom{background:none;}\n.tblog_wrap .tblog_main{border-left:0; border-right:0; padding-left:1px;padding-right:1px;}")
    }
    try {
        var param = {};
        param.fansRow = /fansRow=(\d+)/.test(href) ? RegExp.$1: 2;
        param.isTitle = $E("weibo_title") ? 1 : 0;
        param.isFans = $E("fans_list_con") ? 1 : 0;
        param.isWeibo = $E("weibo_con") ? 1 : 0;
        param.height = parseInt($E("weibo_show_con").style.height);
        param.width = $E("weibo_show_con").offsetWidth
    } catch(e) {}
    var maxH = param.height - 12;
    if (param.isTitle == 1) {
        maxH -= $E("weibo_title").offsetHeight
    }
    if ($E("weibo_head")) {
        maxH -= 74
    }
    if (maxH < 0) {
        return
    }
    if (param.isFans) {
        var list = $E("fans_list_con").getElementsByTagName("li");
        var fash = 0;
        if (list.length == 0) {
            fash = $E("fans_list_con").offsetHeight
        } else {
            var _ul = $E("fans_list_con").getElementsByTagName("ul")[0];
            var w = param.width;
            var n = ((w - 10) / 60) >> 0;
            if (n == 0) {
                n = 1
            }
            _ul.style.padding = "0 " + ((w - 2 - n * 60) / 2 >> 0) + "px";
            var r = Math.ceil(list.length / n);
            param.fansRow = Math.min(param.fansRow, r, (((maxH - 37) / 84) >> 0) || 1);
            fash = param.fansRow * 84;
            if (fash > 0) {
                fash -= 12
            }
            fash += 37;
            if (fash >= maxH) {
                fash = maxH
            }
            $E("fans_list_con").style.height = fash + "px"
        }
        maxH -= fash
    }
    if (param.isWeibo) {
        if (maxH < 32) {
            $E("weibo_con").style.display = "none";
            return
        }
        try {
            var h = $E("weibo_list").offsetHeight;
            $E("weibo_list_con").style.height = (maxH - 20 > h ? h: maxH - 20) + "px"
        } catch(e) {}
    }
});
Sina.pkg("Core.Function");
Core.Function.bind2 = function(fFunc, object) {
    var __method = fFunc;
    return function() {
        return __method.apply(object, arguments)

    }
};
Function.prototype.bind2 = function(object) {
    var __method = this;
    return function() {
        return __method.apply(object, arguments)
    }
}; (function(proxy) {
    proxy.hover = function(el, hoverFun, outerFun) {
        var cls = el.className;
        var css = el.style.cssText;
        Core.Events.addEvent(el,
        function() {
            hoverFun(el)
        },
        "mouseover");
        if (!outerFun || typeof outerFun != "function") {
            Core.Events.addEvent(el,
            function() {
                el.className = cls;
                el.style.cssText = css
            },
            "mouseout")
        } else {
            Core.Events.addEvent(el,
            function() {
                outerFun(el)
            },
            "mouseout")
        }
    }
})(App); (function() {
    var _each = Core.Array.foreach;
    var _addEvent = Core.Events.addEvent;
    var _check = function() {};
    var dom = {},
    list = null,
    listMargin = 0;
    var autoScroll = {
        autoScroll: function(type, speed) {
            var speed = speed || 5;
            var max = $E("weibo_list").offsetHeight - dom.con.clientHeight;
            clearInterval(this._timer);
            var timer = setInterval(function() {
                if (!autoScroll.isOutRange) {
                    return
                }
                if (autoScroll.lock) {
                    return
                }
                var st = dom.con.scrollTop;
                st = type == "down" ? Math.min(st + 2, max) : Math.max(st - 2, 0);
                dom.con.scrollTop = st;
                if (st == 0 || st == max) {
                    autoScroll.stop(timer)
                }
            },
            speed)
        },
        start: function(type, speed) {
            autoScroll.lock = true;
            var speed = speed || 5;
            var max = $E("weibo_list").offsetHeight - dom.con.clientHeight;
            clearInterval(this._timer);
            this._timer = setInterval(function() {
                var st = dom.con.scrollTop;
                st = type == "down" ? Math.min(st + 2, max) : Math.max(st - 2, 0);
                dom.con.scrollTop = st;
                if (st == 0 || st == max) {
                    autoScroll.stop()
                }
            },
            speed)
        },
        lock: false,
        isOutRange: true,
        scroll: function(e) {
            clearTimeout(this.ctimer);
            if (autoScroll.isOutRange) {
                return
            }
            var st = dom.con.scrollTop;
            var max = $E("weibo_list").offsetHeight - dom.con.clientHeight;
            st = (e.wheelDelta <= 0 || e.detail > 0) ? Math.min(st + 20, max) : Math.max(st - 20, 0);
            dom.con.scrollTop = st;
            if (st == 0 || st == max) {
                _check();
                return
            }
            Core.Events.stopEvent(e);
            this.ctimer = setTimeout(function() {
                _check()
            },
            500)
        },
        stop: function(timer) {
            clearInterval(timer || this._timer);
            autoScroll.lock = false;
            _check()
        }
    };
    Jobs.regist("init_weibo_scroll",
    function() {
        if (!$E("weibo_con") || $E("weibo_con").style.display == "none") {
            return
        }
        dom.con = $E("weibo_list_con");
        dom.con.style.position = "relative";
        dom.con.scrollTop = 0;
        listMargin = $E("weibo_list").offsetTop;
        list = dom.con.getElementsByTagName("li"); (function() {
            var con = dom.con;
            if (!$E("weibo_upbtn")) {
                return
            }
            var upImg = $E("weibo_upbtn").getElementsByTagName("img")[0];
            var downImg = $E("weibo_downbtn").getElementsByTagName("img")[0];
            var lcon = $E("weibo_list");
            function ck() {
                var s = con.scrollTop;
                var h = parseInt(con.style.height);
                upImg.style.display = (s == 0 ? "none": "");
                downImg.style.display = (s + h == lcon.offsetHeight ? "none": "")
            }
            var _timer = null;
            _check = function() {
                clearTimeout(_timer);
                setTimeout(ck, 200)
            }
        })();
        _check();
        if (list.length == 0) {
            return
        } (function() {
            var _timer = null;
            _addEvent(dom.con,
            function() {
                clearTimeout(_timer);
                autoScroll.isOutRange = false
            },
            "mouseover");
            _addEvent(dom.con,
            function(e) {
                clearTimeout(_timer);
                _timer = setTimeout(function() {
                    autoScroll.isOutRange = true
                },
                50)
            },
            "mouseout")
        })();
        try {
            window.addEventListener("DOMMouseScroll",
            function(e) {
                autoScroll.scroll(e || event)
            },
            false)
        } catch(err) {
            document.onmousewheel = function(e) {
                autoScroll.scroll(e || event)
            }
        }
        _each(["up", "down"],
        function(v, i) {
            _addEvent($E("weibo_" + v + "btn"),
            function() {
                autoScroll.start(v)
            },
            "mouseover");
            _addEvent($E("weibo_" + v + "btn"),
            function() {
                autoScroll.stop()
            },
            "mouseout")
        });
        _each(list,
        function(v, i) {
            v.style.zoom = 1;
            App.hover(v,
            function(el) {
                el.className = "focus"
            },
            function(el) {
                el.className = ""
            });
            /*_addEvent(v,function() {
                Core.Events.stopEvent();
                window.open(v.getAttribute("href"))
            },"click")*/
        }); (function() {
            var href = location.search;
            var speed = /speed=(\d+)/.test(href) ? RegExp.$1: 0;
            if (speed == 0) {
                return
            }
            var max = $E("weibo_list").offsetHeight - dom.con.clientHeight;
            dom.con.scrollTop = max;
            autoScroll.autoScroll("up", +speed);
            setTimeout(function() {
                _check()
            },
            +speed)
        })()
    })
})();
