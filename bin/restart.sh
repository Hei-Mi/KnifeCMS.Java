#!/bin/sh
echo "backup oracle database"
newdate=`date +%Y%m%d`
olddate=`date -d "3 days ago" +%Y%m%d`
su - oracle -c "exp turbocms/oUcDec18 inctype=complete file=/usr/local/turbocms/db/bak/cms$newdate.dmp"
sleep 1
echo "oracle database backuped"
sleep 1
rm -rf /usr/local/turbocms/db/bak/cms$olddate.dmp
sleep 1
echo "now begin to kill turbocms service..."
ps -ef|grep "TurboCMS"|grep -v "grep"| cut -c 9-15|xargs kill -9
echo "turbocms service killed"
sleep 5
echo "begin start PublishService..."
/usr/local/turbocms/bin/publish.sh
echo "PublishService start successed"
sleep 5
echo "begin start DeployService..."
/usr/local/turbocms/bin/deploy.sh
echo "DeployService start successed"
sleep 5
echo "begin start SpiderService..."
/usr/local/turbocms/bin/spider.sh
echo "SpiderService start successed"
sleep 5
echo "begin start DbDeployService..."
/usr/local/turbocms/bin/dbdeploy.sh
echo "DbDeployService start successed"
echo " "
echo "TurboCMS Service start successed!"
