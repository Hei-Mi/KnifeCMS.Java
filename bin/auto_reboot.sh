#!/bin/sh
#检测tomcat程序是否运行
newdate=`date +"%Y-%m-%d %T"`
tomcatnum=`ps aux | grep 'tomcat6' | wc -l`
#查看有tomcat进程否,并赋值到tomcatnum;
if [ $tomcatnum -le 1 ]
	#判定tomcat是否运行
then
	echo "$newdate CFrisk的Tomcat崩溃，开始自动重启" >> /opt/logs/monitor.log
	#mail -s "严重-CFrisk的Tomcat崩溃！" zergskj@163.com < /opt/logs/monitor.log
	service tomcat6 start
fi